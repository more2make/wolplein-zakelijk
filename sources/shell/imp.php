<?php
	$mageapp = '../app/Mage.php'; // Mage app location
	require_once($mageapp);
	Mage::app('admin');

	$id = 2;

	// Remove unset images, add image to gallery if exists
	$importDir = Mage::getBaseDir('media') . DS . 'import/';
	
	$products = Mage::getModel('catalog/product')->getCollection();
	$products->addAttributeToFilter('sku', array('like' => 'bwbc09561%'));
	foreach($products as $prod) {
		$product = Mage::getModel('catalog/product')->load($prod->getId());
		
		$sku = $product->getSku(); 
		$mainprod = substr($sku, 0, strpos($sku, "_"));
		
		$color = $product->getAttributeText('swatch_color');
		$color2 = sprintf("%02d", $color);
		
		echo $prod->getSku() . " - $color2 - $mainprod ";
		$fileName = "$mainprod/$color2.jpg";
		echo $fileName . "\n";
		
		$filePath = $importDir.$fileName;
		if (file_exists($filePath)) 
		{
			try {
				echo $filePath . "\n";
				$attributes = $product->getTypeInstance ()->getSetAttributes ();
                if (isset ( $attributes ['media_gallery'] )) {
                    $gallery = $attributes ['media_gallery'];
                    //Get the images
                    $galleryData = $product->getMediaGallery ();
                    foreach ( $galleryData ['images'] as $image ) {
                        //If image exists
                        if ($gallery->getBackend ()->getImage ( $product, $image ['file'] )) {
                            $gallery->getBackend ()->removeImage ( $product, $image ['file'] );
                        }
                    }
                    $product->save ();
                }
				//exit();
				
				$product->addImageToMediaGallery($filePath, array('thumbnail', 'small_image', 'image'), false, false);
				$product->save();
			}catch (Exception $e) {
				echo $e->getMessage();
			}
		}else 
		{
			echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
		}
		
	}
	
	exit();
	
	
	
	

	

	//echo $product->getName() . " - " . $product->getSwatchColor() . "\n";
	$color = $product->getAttributeText('swatch_color');
	echo $color ."\n\n";

	$filePath = $importDir.$fileName;
	if (file_exists($filePath)) 
	{
		try {
			echo $filePath . "\n";
			$product->addImageToMediaGallery($filePath, array('thumbnail', 'small_image', 'image'), false, false);
		}catch (Exception $e) {
			echo $e->getMessage();
		}
	}else 
	{
		echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
	}

?>
