<?php

class Mage_Adminhtml_Model_Msp_Config_Source_Order_Email
{

	public function toOptionArray()
	{
		return array(
			array(
				"value" => "after_notification",
				"label" => Mage::helper("msp")->__("After notification")
			),
			array(
				"value" => "after_payment",
				"label" => Mage::helper("msp")->__("After payment complete")
			),
		);
	}

}
