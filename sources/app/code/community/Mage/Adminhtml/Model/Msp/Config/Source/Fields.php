<?php

class Mage_Adminhtml_Model_Msp_Config_Source_Fields
{

	public function toOptionArray()
	{
		return array(
			array(
				"value" => 0,
				"label" => Mage::helper("msp")->__('Disabled')
			),
			array(
				"value" => 1,
				"label" => Mage::helper("msp")->__('Mandatory')
			),
			 array(
			        "value" => 2,
			        "label" => Mage::helper("msp")->__('Optional')
			)
		);
	}

}
