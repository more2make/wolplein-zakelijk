<?php

class Mage_Adminhtml_Model_Msp_Config_Source_Languages
{

	public function toOptionArray()
	{
		return array(
			array(
				"value" => "nl",
				"label" => Mage::helper("msp")->__('Dutch')
			),
			array(
				"value" => "en",
				"label" => Mage::helper("msp")->__('English')
			),
      array(
        "value" => "de",
        "label" => Mage::helper("msp")->__('German')
      ),
      array(
        "value" => "fr",
        "label" => Mage::helper("msp")->__('French')
      ),
      array(
        "value" => "es",
        "label" => Mage::helper("msp")->__('Spanish')
      )
		);
	}

}
