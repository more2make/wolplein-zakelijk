<?php

class Mage_Msp_Model_Gateway_Banktransfer extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_banktransfer";
	protected $_model   = "banktransfer";
	protected $_gateway = "BANKTRANS";
}
