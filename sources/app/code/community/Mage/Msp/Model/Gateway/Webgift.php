<?php

class Mage_Msp_Model_Gateway_Webgift extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_webgift";
	protected $_model   = "webgift";
	protected $_gateway = "WEBSHOPGIFTCARD";
}
