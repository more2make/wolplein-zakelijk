<?php

class Mage_Msp_Model_Gateway_MultiSafepay extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_multisafepay";
	protected $_model   = "multiSafepay";
	protected $_gateway = "WALLET";
}
