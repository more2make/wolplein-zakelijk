<?php

class Mage_Msp_Model_Gateway_GiroPay extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_giropay";
	protected $_model   = "giroPay";
	protected $_gateway = "GIROPAY";
}
