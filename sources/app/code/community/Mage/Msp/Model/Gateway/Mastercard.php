<?php

class Mage_Msp_Model_Gateway_Mastercard extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_mastercard";
	protected $_model   = "mastercard";
	protected $_gateway = "MASTERCARD";
}
