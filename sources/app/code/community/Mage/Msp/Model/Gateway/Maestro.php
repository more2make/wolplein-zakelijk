<?php

class Mage_Msp_Model_Gateway_Maestro extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_maestro";
	protected $_model   = "maestro";
	protected $_gateway = "MAESTRO";
}
