<?php

class Mage_Msp_Model_Gateway_Visa extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_visa";
	protected $_model   = "visa";
	protected $_gateway = "VISA";
}
