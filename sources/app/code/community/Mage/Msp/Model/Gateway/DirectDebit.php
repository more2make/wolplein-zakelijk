<?php

class Mage_Msp_Model_Gateway_DirectDebit extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_directdebit";
	protected $_model   = "directDebit";
	protected $_gateway = "DIRDEB";
}
