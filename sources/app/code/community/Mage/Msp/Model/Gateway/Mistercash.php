<?php

class Mage_Msp_Model_Gateway_Mistercash extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_mistercash";
	protected $_model   = "mistercash";
	protected $_gateway = "MISTERCASH";
}
