<?php

class Mage_Msp_Model_Gateway_DirectEbanking extends Mage_Msp_Model_Gateway_Abstract
{
	protected $_code    = "msp_directebanking";
	protected $_model   = "directEbanking";
	protected $_gateway = "DIRECTBANK";
}
