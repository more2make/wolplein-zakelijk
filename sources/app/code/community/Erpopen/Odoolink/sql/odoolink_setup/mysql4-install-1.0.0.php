<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_product_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_product_mapping')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `erp_pro_id` int(11) unsigned NOT NULL,
  `mage_pro_id` int(11) unsigned NOT NULL,
  `need_sync` varchar(11) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_category_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_category_mapping')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `erp_cat_id` int(11) unsigned NOT NULL,
  `mage_cat_id` int(11) unsigned NOT NULL,
  `need_sync` varchar(11) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_customer_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_customer_mapping')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `erp_cus_id` int(11) unsigned NOT NULL,
  `mage_cus_id` varchar(255) DEFAULT NULL,
  `mage_address_id` varchar(255) DEFAULT NULL,
  `need_sync` varchar(11) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_order_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_order_mapping')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `erp_order_id` int(11) unsigned NOT NULL,
  `mage_order_id` int(11) unsigned NOT NULL,
  `erp_order_line_id` varchar(255) DEFAULT NULL,
  `erp_cus_id` int(11) NOT NULL,
  `erp_order_name` varchar(255) NOT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_tax_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_tax_mapping')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `erp_tax_id` int(11) unsigned NOT NULL,
  `mage_tax_id` int(11) NOT NULL,
  `mage_tax_code` varchar(500) NOT NULL,
  `need_sync` varchar(11) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_currency_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_currency_mapping')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `erp_price_list_id` int(11) unsigned NOT NULL,
  `mage_currency_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('erp_mage_payment_mapping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('erp_mage_payment_mapping')} (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `mage_payment_id` varchar(255) NOT NULL,
  `erp_payment_id` int(11) NOT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

");

$installer->endSetup(); 