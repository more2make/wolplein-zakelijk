<?php

class Erpopen_Odoolink_Model_Tax extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/tax');
    }
		
	public function mappingerp($data){
		$productmap=Mage::getModel('odoolink/tax');
		$productmap->seterp_pro_id($data['erp_tax_id']);
		$productmap->setmage_pro_id($data['mage_tax_id']);
		$productmap->setneed_sync('no');
		$productmap->save();
	}
	static public function getMageTaxArray() {
        $Tax = array();
		$prefix=Mage::getConfig()->getTablePrefix();
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$result = $write->query("SELECT * FROM ".$prefix."tax_calculation_rate");
		foreach($result->fetchAll() as $one_row)	{					
			$mage_tax_id = $one_row['tax_calculation_rate_id'];
			$mage_tax_code = $one_row["code"];
			$mage_tax_rate = $one_row["rate"];
			$t = $mage_tax_code.'('.$mage_tax_rate.'%)';
			array_push($Tax,
			array(
					'value' => $mage_tax_id,
					'label'=>Mage::helper('adminhtml')->__($t),					
					)
			);
		}
        array_unshift($Tax, array('label' => '--Select Magento Tax--', 'value' => ''));
        return $Tax;
    }
	static public function getErpTaxArray() {
		$Tax = array();
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			array_push($Tax, array('label' => 'Not Available(Connection Error)', 'value' => ''));
			return $Tax;
		}else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$key     = array();
			$msg_ser = new odooxmlrpcmsg('execute');
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg_ser->addParam(new odooxmlrpcval("account.tax", "string"));
			$msg_ser->addParam(new odooxmlrpcval("search", "string"));
			$msg_ser->addParam(new odooxmlrpcval($key, "array"));
			$resp0    = $client->send($msg_ser);
			if ($resp0->faultCode()) {
				array_push($Tax, array('label' => 'Not Available(Error in Fetching)', 'value' => ''));
				return $Tax;
			}
			else 
			{
				$val    = $resp0->value()->me['array'];
				$key1 = array(new odooxmlrpcval('id','int') , new odooxmlrpcval('name', 'string'),new odooxmlrpcval('amount', 'string'));
				$msg_ser1 = new odooxmlrpcmsg('execute');
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval($userId, "int"));
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval("account.tax", "string"));
				$msg_ser1->addParam(new odooxmlrpcval("read", "string"));
				$msg_ser1->addParam(new odooxmlrpcval($val, "array"));
				$msg_ser1->addParam(new odooxmlrpcval($key1, "array"));
				$resp1   = $client->send($msg_ser1);
				if ($resp1->faultCode()) {
					$msg = 'Not Available- Error: '.$resp1->faultString();
					array_push($Tax, array('label' => $msg, 'value' => ''));
					return $Tax;
				} 
				else 
				{
					$value_array=$resp1->value()->scalarval();
					$count = count($value_array);			
				
					for($x=0;$x<$count;$x++)
					{
					   $id = $value_array[$x]->me['struct']['id']->me['int'];
					   $name = $value_array[$x]->me['struct']['name']->me['string'].' ('.($value_array[$x]->me['struct']['amount']->me['double']*100).'%)';
					   array_push($Tax,
						array(
								'value' => $id,
								'label'=>Mage::helper('adminhtml')->__($name))
						);
					}
				}	
			}
			array_unshift($Tax, array('label' => '--Select ODOO Tax--', 'value' => ''));
			return $Tax;
		}		
	}
}
?>