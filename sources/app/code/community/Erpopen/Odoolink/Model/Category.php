<?php

class Erpopen_Odoolink_Model_Category extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/category');
    }
	
	public function mappingerp($data){	
		$categorymap=Mage::getModel('odoolink/category');
		$categorymap->seterp_cat_id($data['erp_category_id']);
		$categorymap->setmage_cat_id($data['mage_category_id']);
		$categorymap->setneed_sync('no');
		$categorymap->save();
	}
	
	static public function getMageCategoryArray() {
        $Category = array();
		$category_ids = Mage::getModel('catalog/category')->getCollection()->getAllIds();
		
		foreach ($category_ids as $one_id){
			$cat = Mage::getModel('catalog/category')->load($one_id);			
			if ($cat->getLevel() > 0){
				array_push($Category,
				array(
						'value' => $cat->getId(),
						'label'=>Mage::helper('adminhtml')->__($cat->getName()),
						)
				);
			}
		}
        array_unshift($Category, array('label' => '--Select Magento Category--', 'value' => ''));
        return $Category;
    }
	
	static public function getErpCategoryArray() {
		$Category = array();
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			array_push($Category, array('label' => 'Not Available(Connection Error)', 'value' => ''));
			return $Category;
		}else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$key     = array();
			$msg_ser = new odooxmlrpcmsg('execute');
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg_ser->addParam(new odooxmlrpcval("product.category", "string"));
			$msg_ser->addParam(new odooxmlrpcval("search", "string"));
			$msg_ser->addParam(new odooxmlrpcval($key, "array"));
			$resp0    = $client->send($msg_ser);
			if ($resp0->faultCode()) {
				array_push($Category, array('label' => 'Not Available(Error in Fetching)', 'value' => ''));
				return $Category;
			}
			else 
			{
				$val    = $resp0->value()->me['array'];
				$key1 = array(new odooxmlrpcval('id','int') , new odooxmlrpcval('name', 'string'));
				$msg_ser1 = new odooxmlrpcmsg('execute');
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval($userId, "int"));
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval("product.category", "string"));
				$msg_ser1->addParam(new odooxmlrpcval("read", "string"));
				$msg_ser1->addParam(new odooxmlrpcval($val, "array"));
				$msg_ser1->addParam(new odooxmlrpcval($key1, "array"));
				$resp1   = $client->send($msg_ser1);
				if ($resp1->faultCode()) {
					$msg = 'Not Available- Error: '.$resp1->faultString();
					array_push($Category, array('label' => $msg, 'value' => ''));
					return $Category;
				} 
				else 
				{
					$value_array=$resp1->value()->scalarval();
					$count = count($value_array);

					for($x=0;$x<$count;$x++)
					{
					   $id = $value_array[$x]->me['struct']['id']->me['int'];
					   $name = $value_array[$x]->me['struct']['name']->me['string'];
					   array_push($Category,
						array(
								'value' => $id,
								'label'=>Mage::helper('adminhtml')->__($name))
						);
					}
				}	
			}
			array_unshift($Category, array('label' => '--Select ODOO category--', 'value' => ''));
			return $Category;
		}		
	}
}