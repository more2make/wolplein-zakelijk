<?php
include (Mage::GetBaseDir("skin")."/adminhtml/default/default/odoolink/xmlrpc.inc");
error_reporting(E_ALL);
ini_set('allow_url_fopen', 1);

class Erpopen_Odoolink_Model_Commonsocket extends Mage_Core_Model_Abstract	{
	public function getSocketConnect()	{	
		//Mage::log("Model\Commonsocket.php: getSocketConnect()", null, "odoolink.log", null, "odoolink.log"); 
		$sock = new odooxmlrpc_client(Mage::getStoreConfig('odoolink/odoolink/url').":".Mage::getStoreConfig('odoolink/odoolink/port')."/xmlrpc/common");
		$msg = new odooxmlrpcmsg('login');
		$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/uname'), "string"));
		$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$resp =  $sock->send($msg);
		if ($resp->errno != 0){
			return 'error';
		}
		else{
			$val = $resp->value();
			$id = $val->scalarval();
			if($id > 0) 
				return $id;
			else
				return 'error';
		}
	}
	
	public function getClientConnect(){
		//Mage::log("Model\Commonsocket.php: getClientConnect()", null, "odoolink.log", null, "odoolink.log"); 
		// Mage::log(Mage::getStoreConfig('odoolink/odoolink/url').":".Mage::getStoreConfig('odoolink/odoolink/port')."/xmlrpc/object", null, "odoolink.log");
		return new odooxmlrpc_client(Mage::getStoreConfig('odoolink/odoolink/url').":".Mage::getStoreConfig('odoolink/odoolink/port')."/xmlrpc/object");
	}
	
	public function getAllSyncedCategoryIds($need_sync=true){
		Mage::log("Model\Commonsocket.php: getAllSyncedCategoryIds()", null, "odoolink.log", null, "odoolink.log"); 
		$mage_ids = array();
		$model = Mage::getModel('odoolink/category');
		$mappingcollection =  $model->getCollection();
		foreach($mappingcollection as $map){
			if($need_sync){
				if($map->getNeedSync() == 'no'){
					array_push($mage_ids, $map->getMageCatId());
				}
			}else{
				array_push($mage_ids, $map->getMageCatId());
			}
		}		
		return $mage_ids;
	}
	
	public function CreateCategory(){
		Mage::log("Model\Commonsocket.php: CreateCategory()", null, "odoolink.log", null, "odoolink.log"); 
		$synced_ids = $this->getAllSyncedCategoryIds();
		if(count($synced_ids) > 0){
			$category_collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToFilter('entity_id', array('nin'=>$synced_ids));
		}else{
			$category_collection = Mage::getModel('catalog/category')->getCollection();
		}
		$category_ids = $category_collection->getAllIds();		
		$userId = -1;$signal = 0;$parent_id = 1;
		if($userId<=0)
			$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$signal = 0;
		}
		else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$max_level=$write->query("SELECT MAX(level) FROM ".$prefix."catalog_category_entity");
			$max_level=$max_level->fetch();
			for($i=1;$i<=$max_level["MAX(level)"];$i++)	{
				foreach($category_ids as $one_id)	{
					$cat = Mage::getModel('catalog/category')->load($one_id);
					if(!$cat->getName())
						continue;
					if($cat->getLevel()==$i){
						$result_check=$write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$cat->getId()."'");
						$check_one=$result_check->fetch();
						if(!$check_one){
							$check_parent1=$write->query("SELECT parent_id FROM ".$prefix."catalog_category_entity WHERE entity_id='".$cat->getId()."'");
							$check_fetch1=$check_parent1->fetch();
							$check_parent2=$write->query("SELECT erp_cat_id FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$check_fetch1["parent_id"]."'");
							$check_fetch2=$check_parent2->fetch();
							$cat_name = urlencode($cat->getName());
							$catg_array = array(
										'name'=>new odooxmlrpcval($cat_name, "string"),
										'type'=>new odooxmlrpcval("normal", "string"),
										'mage_id'=>new odooxmlrpcval($cat->getId(), "int"),
										'method'=>new odooxmlrpcval('create', "string")
								);
							if($check_fetch2)
								$catg_array['parent_id'] = new odooxmlrpcval($check_fetch2["erp_cat_id"], "string");
							$erp_catg_id = $this ->Erp_category_create($catg_array, $userId, $client);
							if($erp_catg_id>0){
								$write->query("INSERT INTO ".$prefix."erp_mage_category_mapping VALUES('','$erp_catg_id','".$cat->getId()."','no')");
							}
						}elseif($check_one["need_sync"] == 'yes'){
							$erp_catg_id = $check_one["erp_cat_id"];
							// fetching parent id from catalog_category_entity Table
							$check_parent1=$write->query("SELECT parent_id FROM ".$prefix."catalog_category_entity WHERE entity_id='".$cat->getId()."'");
							$check_fetch1=$check_parent1->fetch();
							// fetching ODOO parent category id
							$check_parent2=$write->query("SELECT erp_cat_id FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$check_fetch1["parent_id"]."'");
							$check_fetch2=$check_parent2->fetch();
							$cat_name = urlencode($cat->getName());							
							$catg_array = array(
										'name'=>new odooxmlrpcval($cat_name, "string"),
										'type'=>new odooxmlrpcval("normal", "string"),
										'category_id'=>new odooxmlrpcval($erp_catg_id, "int"),
										'method'=>new odooxmlrpcval('write', "string")
								);
							if($check_fetch2)
								$catg_array['parent_id'] = new odooxmlrpcval($check_fetch2["erp_cat_id"], "string");			
							$this ->Erp_category_update($catg_array, $userId, $client);
							$write->query("UPDATE ".$prefix."erp_mage_category_mapping SET need_sync='no' WHERE  	erp_cat_id='$erp_catg_id'");
						}
					}
				}
			}
		$signal = 1;
		}
		return $signal;
	}
	
	public function Erp_category_create($catg_array, $userId, $client )	{
		Mage::log("Model\Commonsocket.php: Erp_Category_Create()", null, "odoolink.log", null, "odoolink.log"); 
		$msg1=new odooxmlrpcmsg('execute');
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new odooxmlrpcval($userId,"int"));
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new odooxmlrpcval("magento.category","string"));
		$msg1->addParam(new odooxmlrpcval("create_category","string"));
		$msg1->addParam(new odooxmlrpcval($catg_array, "struct"));
		$resp = $client->send($msg1);		
		if (!$resp->faultCode()){	
			$erp_catg_id = $resp->value()->me["int"];
			return $erp_catg_id;
		}
	}
	
	public function Erp_category_update($catg_array, $userId, $client )	{
		Mage::log("Model\Commonsocket.php: Erp_category_update()", null, "odoolink.log", null, "odoolink.log"); 
		$msg1=new odooxmlrpcmsg('execute');
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new odooxmlrpcval($userId,"int"));
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new odooxmlrpcval("magento.category","string"));
		$msg1->addParam(new odooxmlrpcval("create_category","string"));
		$msg1->addParam(new odooxmlrpcval($catg_array, "struct"));
		$resp = $client->send($msg1);
	}
	
	public function getAllSyncedProductIds($need_sync=true){
		Mage::log("Model\Commonsocket.php: getAllsyncedProductIds()", null, "odoolink.log", null, "odoolink.log"); 
		$mage_ids = array();
		$model = Mage::getModel('odoolink/products');
		$mappingcollection =  $model->getCollection();
		foreach($mappingcollection as $map){
			if($need_sync){
				if($map->getNeedSync() == 'no'){
					array_push($mage_ids, $map->getMageProId());
				}
			}else{
				array_push($mage_ids, $map->getMageProId());
			}
		}		
		return $mage_ids;	
	}
	
	public function CreateProduct()
	{
		Mage::log("Model\Commonsocket.php: CreateProduct()", null, "odoolink.log", null, "odoolink.log"); 
		$signal = 0;
		$pro_image = false;
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$signal = 0;
		}
		else{
			Mage::log("Geen Conn error", null, "odoolink.log"); 
			$type ='';
			$this->CreateCategory();
			$synced_ids = $this->getAllSyncedProductIds();
			if(count($synced_ids) > 0){
				//$product_collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('entity_id', array('nin'=>$synced_ids))->addAttributeToFilter('attribute_set_id', array('neq'=>10))->addAttributeToSort('updated_at','DESC');
				$product_collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('entity_id', array('nin'=>$synced_ids))->addAttributeToSort('updated_at','DESC');
			}else{
				$product_collection = Mage::getModel('catalog/product')->getCollection();
			}
			//$product_collection->setOrder('mage_pro_id', 'DESC');
			$product_collection->setPageSize(50);
			Mage::log((string)$product_collection->getSelect(), null, "odoolink.log");
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$inventory = Mage::getStoreConfig('odoolink/odoolink2/inventory');
			$prefix = Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			foreach($product_collection as $product){
				$pro_id = 0;
				$pro_image = false;
				$erp_categ_ids = array();
				$onepro = Mage::getModel('catalog/product')->load($product->getId());
				if(!$onepro->getName())
					continue;
				$categories = $onepro->getCategoryIds();
				if(count($categories) > 0){
					foreach($categories as $cat_id){
						$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$cat_id."'");
						$catg_one=$catg_check->fetch();
						if($catg_one["erp_cat_id"] > 0 ){
							$categ_id = $catg_one["erp_cat_id"];
							array_push($erp_categ_ids, new odooxmlrpcval($categ_id, 'int'));
						}
					}
				}else{
					$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='2'");
					$catg = $catg_check->fetch();
					array_push($erp_categ_ids, new odooxmlrpcval($catg["erp_cat_id"], 'int'));
				}
				$result_check = $write->query("SELECT * FROM ".$prefix."erp_mage_product_mapping WHERE mage_pro_id='".$onepro->getId()."'");
				$check_one = $result_check->fetch();
				
				
				
				//$newimgurl = $onepro->getImageUrl();
				$newimgurl = str_replace("https://www.wolplein.nl/", "", $onepro->getImageUrl());
				//Mage::log("prod image: " . $newimgurl, null, "odoolink.log"); 
				try{
					$image = file_get_contents($newimgurl);
					//Mage::log($image, null, "odoolink.log");
				}catch (Exception $e){
					//Mage::log("IMG ERR: " . $e, null, "odoolink.log");
					$image = false;
				}
				if($image) {
					$pro_image = base64_encode($image);
					//Mage::log("Image found", null, "odoolink.log");
				}else {
					//Mage::log("No Image found", null, "odoolink.log");
				}
				
				$ean = $onepro->getBarcode();
				$type = urlencode($onepro->getTypeID());
				$name = urlencode($onepro->getName());
				$sku = urlencode($onepro->getSku());
				if(isset($check_one["erp_pro_id"]))
					$pro_id = $check_one["erp_pro_id"];
				
				$website_ids = $onepro->getWebsiteIds();
				$store_ids = $onepro->getStoreIds();
				
				$websiteids = $this->PHPIntArrayToXMLRPCArray($website_ids);
				$storeids = $this->PHPIntArrayToXMLRPCArray($store_ids);
				
				$upsell_product_collection = $onepro->getUpSellProductCollection();
				$xmlupsell = array();
				foreach($upsell_product_collection as $pdt)
				{
					$vall = array(
						'product_id'=>new odooxmlrpcval($pdt->getId(), "int"),
						'product_pos'=>new odooxmlrpcval((int)$pdt->getPos(), "int")
					);
					$xmlupsell[] = new odooxmlrpcval($vall, "struct");
				}
				
				$crosssell_product_collection = $onepro->getCrossSellProductCollection();
				$xmlcrosssell = array();
				foreach($crosssell_product_collection as $pdt)
				{
					$vall = array(
						'product_id'=>new odooxmlrpcval($pdt->getId(), "int"),
						'product_pos'=>new odooxmlrpcval((int)$pdt->getPos(), "int")
					);
					$xmlcrosssell[] = new odooxmlrpcval($vall, "struct");
				}
				
				$related_product_collection = $onepro->getRelatedProductCollection();
				$xmlrelated = array();
				foreach($related_product_collection as $pdt)
				{
					$vall = array(
						'product_id'=>new odooxmlrpcval($pdt->getId(), "int"),
						'product_pos'=>new odooxmlrpcval((int)$pdt->getPos(), "int")
					);
					$xmlrelated[] = new odooxmlrpcval($vall, "struct");
				}
				
				
				
				//print_r($xmlupsell);
				//exit();	
				
				$ignore_attributes = array('options_container');
				$storeview_data = array();
				$storeview_data_content = array();
				//Storeview specific data
				foreach($store_ids as $store_id)
				{
					$product_store = Mage::getModel('catalog/product')->setStoreId($store_id)->load($product->getId());
					$storedata = $product_store->getData();
					foreach($storedata as $storedatakey=>$storedataval)
					{
						$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $storedatakey);
						$attributetype = $attribute->getFrontendInput();
						$scopetype = $attribute->getIsGlobal();
						if($scopetype != 1 && !in_array($storedatakey, $ignore_attributes))
						{
							switch($attributetype)
							{
								case "text":
									$storeview_data_content[$storedatakey] = new odooxmlrpcval($storedataval, "string");
									break;
								case "textarea":
									$storeview_data_content[$storedatakey] = new odooxmlrpcval(strip_tags($storedataval), "string");
									break;
								case "multiselect":
									$pro_array[$globaldatakey] =  new odooxmlrpcval($this->MultiselectToXMLRPC($globaldatakey, $globaldataval), "struct");
									//$storeview_data_content[$storedatakey] = new odooxmlrpcval($this->PHPIntArrayToXMLRPCArray(explode(",", $storedataval))	, "array");
									break;
								case "select":
									$vall = array(
										'value'=>new odooxmlrpcval($storedataval, "int"),
										'value_text'=>new odooxmlrpcval($onepro->getAttributeText($storedatakey), "string")
										);
									if(isset($storedataval) && $onepro->getAttributeText($storedatakey))
										$storeview_data_content[$storedatakey] = new odooxmlrpcval($vall, "struct");
									
									break;
								case "price":
									$storeview_data_content[$storedatakey] = new odooxmlrpcval($storedataval, "string");	//String???
									break;
								case "date":
									$storeview_data_content[$storedatakey] = new odooxmlrpcval($storedataval, "string");	//String???
									break;
								default:
									//print_r($storedatakey . " - SCOPE: " . $scopetype . " - " . $attribute->getFrontendInput() . " - " . $storedataval . "\n");
									break;
							}
						}
					}
					$storeview_data[$store_id] = new odooxmlrpcval($storeview_data_content ,"struct");
				}
	
				$desc = urlencode(strip_tags($onepro->getDescription()));
				$srt_desc = urlencode(strip_tags($onepro->getShortDescription()));
				
				$pro_array = array(
					'name'=>new odooxmlrpcval($name, "string"),
					'description'=>new odooxmlrpcval($desc, "string"),
					'description_sale'=>new odooxmlrpcval($srt_desc, "string"),
					'list_price'=>new odooxmlrpcval($onepro->getPrice(), "string"),
					'type'=>new odooxmlrpcval($type, "string"),
					'categ_ids'=>new odooxmlrpcval($erp_categ_ids, "array"),
					'default_code'=>new odooxmlrpcval($sku, "string"),				
					'weight_net'=>new odooxmlrpcval($onepro->getWeight(), "double"),
					'mage_id'=>new odooxmlrpcval($onepro->getId(), "int"),		//Not used in "UPDATE", will it work?
					'website_ids'=>new odooxmlrpcval($websiteids, "array"),
					'storeview_ids'=>new odooxmlrpcval($storeids, "array"),							
					'attribute_set_id'=>new odooxmlrpcval($onepro->getAttributeSetId(), "int"),
					'storeview_data'=>new odooxmlrpcval($storeview_data, "struct"),
					'upsell_data'=>new odooxmlrpcval($xmlupsell, "struct"),
					'crosssell_data'=>new odooxmlrpcval($xmlcrosssell, "struct"),
					'related_data'=>new odooxmlrpcval($xmlrelated, "struct")
				);
				
				$globaldata = $onepro->getData();
				foreach($globaldata as $globaldatakey=>$globaldataval)
					{
						$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $globaldatakey);
						$attributetype = $attribute->getFrontendInput();
						$scopetype = $attribute->getIsGlobal();
						if($scopetype == 1 && !in_array($storedatakey, $ignore_attributes))
						{
							//print_r($globaldatakey . " - SCOPE: " . $scopetype . " - " . $attribute->getFrontendInput() . " - " . $globaldataval . "\n");
							switch($attributetype)
							{
								case "text":
									$pro_array[$globaldatakey] = new odooxmlrpcval($globaldataval, "string");
									break;
								case "textarea":
									$pro_array[$globaldatakey] = new odooxmlrpcval(strip_tags($globaldataval), "string");
									break;
								case "multiselect":
									$pro_array[$globaldatakey] =  new odooxmlrpcval($this->MultiselectToXMLRPC($globaldatakey, $globaldataval), "struct");
									//$pro_array[$globaldatakey] = new odooxmlrpcval($this->PHPIntArrayToXMLRPCArray(explode(",", $globaldataval)), "array");
									break;
								case "select":
									$vall = array(
										'value'=>new odooxmlrpcval($globaldataval, "int"),
										'value_text'=>new odooxmlrpcval($onepro->getAttributeText($globaldatakey), "string")
										);
									
									if(isset($globaldataval) && $onepro->getAttributeText($globaldatakey))
									{
										$pro_array[$globaldatakey] = new odooxmlrpcval($vall, "struct");
									}
									
									break;
								case "price":
									$pro_array[$globaldatakey] = new odooxmlrpcval($globaldataval, "string");	//String???
									break;
								case "date":
									$pro_array[$globaldatakey] = new odooxmlrpcval($globaldataval, "string");	//String???
									break;
								default:
									//print_r($globaldatakey . " - SCOPE: " . $scopetype . " - " . $attribute->getFrontendInput() . " - " . $globaldataval . "\n");
									break;
							}
						}
					}
				
				Mage::log($sku . " - " . $name . " - " . $onepro->getId() . " - " . $onepro->getUpdatedAt(), null, "odoolink.log");
				
				if($pro_image)
					$pro_array['image'] = new odooxmlrpcval($pro_image, "string");
				
				if($ean){
					$check_ean = $this->validate_ean13($ean);
					if($check_ean)
						$pro_array['ean13'] = new odooxmlrpcval($ean, "string");
				}
				
				if(!$check_one) {
					$pro_array['method'] = new odooxmlrpcval('create', "string");	//Create Product
					$pro_id = $this ->Erp_product_create($pro_array, $userId, $client);
					if($pro_id > 0)
					{
						$write->query("INSERT INTO ".$prefix."erp_mage_product_mapping VALUES('','".$pro_id."','".$onepro->getId()."','no')");
						if($inventory == 1)
							$this->ErpCreateInventory($onepro, $pro_id, 0, $userId, $client);
					}
				}
				elseif($check_one["need_sync"] == 'yes') {
					$pro_array['method'] = new odooxmlrpcval('write', "string");	//Update Product
					$this->Erp_product_update($pro_array, $userId, $client);
					$write->query("UPDATE ".$prefix."erp_mage_product_mapping SET need_sync='no' WHERE erp_pro_id='$pro_id'");
				}
				
			} 
			$signal = 1;
		}
		return $signal;
	}
	
	private function PHPIntArrayToXMLRPCArray($php_array)
	{
		$xmlrpc_array = array();
		foreach($php_array as $php_array_item)
		{
			$xmlrpc_array[] = new odooxmlrpcval($php_array_item, "int");
		}
		return $xmlrpc_array;
	}
	
	private function MultiselectToXMLRPC($attributename, $attributevalue)
	{
		$attributeId = Mage::getModel('eav/config')->getAttribute('catalog_product', $attributename)->getId();
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		$attributeOptions = $attribute ->getSource()->getAllOptions();
		$attributeValues = explode(",", $attributevalue);
		$xmlrpctest = array();
		
		foreach($attributeOptions as $attributeOption)
		{
			foreach($attributeValues as $attributeValue)
			{
				if($attributeValue == $attributeOption['value'])
				{
					$vall = array(
						'value'=>new odooxmlrpcval($attributeValue, "int"),
						'value_text'=>new odooxmlrpcval($attributeOption['label'], "string")
					);
					$xmlrpctest[] = new odooxmlrpcval($vall, "struct");
				}
			}
		}
		return $xmlrpctest;
	}
	
	public function ErpCreateInventory($onepro, $erp_pro_id, $ordered_qty=0, $userId, $client){
		Mage::log("Model\Commonsocket.php: ErpCreateInventory()", null, "odoolink.log", null, "odoolink.log"); 
		$mageproductqty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($onepro)->getQty();
		$stock_setting = Mage::getStoreConfig('cataloginventory/options/can_subtract');
		if($ordered_qty>0 && $stock_setting==1){
			$mageproductqty = $mageproductqty + $ordered_qty;
		}
		$move_arr =  array(
				'new_quantity'=>new odooxmlrpcval($mageproductqty,"int"),
				'product_id'=>new odooxmlrpcval($erp_pro_id,"int")
		);
		$inv = new odooxmlrpcmsg('execute');
		$inv->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$inv->addParam(new odooxmlrpcval($userId, "int"));
		$inv->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$inv->addParam(new odooxmlrpcval("magento.orders", "string"));
		$inv->addParam(new odooxmlrpcval("update_quantity", "string"));
		$inv->addParam(new odooxmlrpcval($move_arr, "struct"));
		$cli_resp = $client->send($inv);
	}
	
	public function Erp_product_create($pro_array, $userId, $client )	{
		Mage::log("Model\Commonsocket.php: Erp_product_create()", null, "odoolink.log"); 
		$msg2 = new odooxmlrpcmsg('execute');
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new odooxmlrpcval($userId, "int"));
		Mage::log("Model\Commonsocket.php userId: " . $userId, null, "odoolink.log"); 
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new odooxmlrpcval("magento.product", "string"));
		$msg2->addParam(new odooxmlrpcval("create_product", "string"));
		$msg2->addParam(new odooxmlrpcval($pro_array, "struct"));
		//Mage::log("Model\Commonsocket.php array: " . $pro_array, null, "odoolink.log"); 
		//Mage::log($pro_array, null, "odoolink.log");  
		Mage::log("Model\Commonsocket.php: pass param", null, "odoolink.log"); 
		$resp = $client->send($msg2);
		Mage::log("Model\Commonsocket.php: pass send", null, "odoolink.log"); 
		if (!$resp->faultCode()){	
			$erp_pro_id = $resp->value()->me["int"];
			return $erp_pro_id;
		}else{
			Mage::log("Model\Commonsocket.php: fault " . $resp->faultCode(), null, "odoolink.log"); 
		}
	}
	
	public function Erp_product_update($pro_array, $userId, $client )	{
		Mage::log("Model\Commonsocket.php: Erp_product_update()", null, "odoolink.log"); 
		$msg2 = new odooxmlrpcmsg('execute');
		Mage::log(" - Execute", null, "odoolink.log"); 
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		
		$msg2->addParam(new odooxmlrpcval($userId, "int"));
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		Mage::log(" - login ", null, "odoolink.log"); 
		$msg2->addParam(new odooxmlrpcval("magento.product", "string"));
		Mage::log(" - magento product ", null, "odoolink.log"); 
		$msg2->addParam(new odooxmlrpcval("create_product", "string"));
		Mage::log(" create  ", null, "odoolink.log"); 
		$msg2->addParam(new odooxmlrpcval($pro_array, "struct"));
		Mage::log(" pro! ", null, "odoolink.log"); 
	//	mage::log($msg2, null, "odoolink.log");
		$resp = $client->send($msg2);
		
		Mage::log(" Zending voltooid ", null, "odoolink.log"); 
	}



	public function Erp_customer_create($customer_arr, $mage_customer_id, $mage_address_id, $userId, $client )	{
		
		Mage::log("Model\Commonsocket.php: Erp_customer_create()", null, "odoolink.log", null, "odoolink.log"); 
		$msg1 = new odooxmlrpcmsg('execute');
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new odooxmlrpcval($userId,"int"));
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new odooxmlrpcval("magento.orders","string"));
		$msg1->addParam(new odooxmlrpcval("create_customer","string"));
		$msg1->addParam(new odooxmlrpcval($customer_arr,"struct"));
		$resp = $client->send($msg1);		
		$erp_customer_id = $resp->value()->me["int"];
	// mapping Entry
		if($erp_customer_id > 0 && $mage_address_id){
			$model = Mage::getModel('odoolink/odoolink');
			$model->setErpCusId($erp_customer_id);
			$model->setMageCusId($mage_customer_id);
			$model->setMageAddressId($mage_address_id);
			$model->setNeedSync('no');
			$model->save();			
			$prod_map_arr = array(  
							'cus_name'=>new odooxmlrpcval($erp_customer_id,"int"),
							'oe_customer_id'=>new odooxmlrpcval($erp_customer_id,"int"),
							'mag_customer_id'=>new odooxmlrpcval($mage_customer_id,"string"),
							'mag_address_id'=>new odooxmlrpcval($mage_address_id,"string")
						);
			$prod_map = new odooxmlrpcmsg('execute');
			$prod_map->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$prod_map->addParam(new odooxmlrpcval($userId, "int"));
			$prod_map->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$prod_map->addParam(new odooxmlrpcval("magento.customers", "string"));
			$prod_map->addParam(new odooxmlrpcval("create", "string"));
			$prod_map->addParam(new odooxmlrpcval($prod_map_arr, "struct"));
			$prod_map_resp = $client->send($prod_map);
		}
		return $erp_customer_id;
	}
	
	public function Erp_customer_update($customer_arr, $entityId, $userId, $client){
		Mage::log("Model\Commonsocket.php: Erp_customer_updatee()", null, "odoolink.log", null, "odoolink.log"); 
		$msg1 = new odooxmlrpcmsg('execute');
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new odooxmlrpcval($userId,"int"));
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new odooxmlrpcval("magento.orders","string"));
		$msg1->addParam(new odooxmlrpcval("create_customer","string"));
		$msg1->addParam(new odooxmlrpcval($customer_arr,"struct"));
		$resp = $client->send($msg1);		
		$customermap = Mage::getModel('odoolink/odoolink')->load($entityId);
		$customermap->setNeedSync('no');
		$customermap->save();
	}
	
	public function Erp_Tax_create($tax_array, $userId, $client)	{
			Mage::log("Model\Commonsocket.php: Erp_tax_create()", null, "odoolink.log", null, "odoolink.log"); 
			$msg2 = new odooxmlrpcmsg('execute');
			$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg2->addParam(new odooxmlrpcval($userId, "int"));
			$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg2->addParam(new odooxmlrpcval("account.tax", "string"));
			$msg2->addParam(new odooxmlrpcval("create", "string"));
			$msg2->addParam(new odooxmlrpcval($tax_array, "struct"));
			$resp = $client->send($msg2);
			if ($resp->errno != 0){
				return 0;
			}
			else{
				$erp_tax_id = $resp->value()->me['int'];
				return $erp_tax_id;
			}
	}
	
	public function Erp_Tax_update($tax_array, $erp_tax_id, $userId, $client)	{
		Mage::log("Model\Commonsocket.php: Erp_tax_update()", null, "odoolink.log", null, "odoolink.log"); 
		$erp_tax_key = array(new odooxmlrpcval($erp_tax_id, 'int'));
		$msg1 = new odooxmlrpcmsg('execute');
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new odooxmlrpcval($userId,"int"));
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new odooxmlrpcval("account.tax","string"));
		$msg1->addParam(new odooxmlrpcval("write","string"));
		$msg1->addParam(new odooxmlrpcval($erp_tax_key,"array"));
		$msg1->addParam(new odooxmlrpcval($tax_array,"struct"));
		$resp = $client->send($msg1);
	}
	
	public function Erp_Journal_create($arrayVal, $userId, $client)	{
		$msg1=new odooxmlrpcmsg('execute');
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new odooxmlrpcval($userId,"int"));
		$msg1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new odooxmlrpcval("magento.orders","string"));
		$msg1->addParam(new odooxmlrpcval("create_payment_method","string"));
		$msg1->addParam(new odooxmlrpcval($arrayVal, "struct"));
		$resp = $client->send($msg1);
		if ($resp->errno != 0){
			return 0;
		}
		else{
			$erp_Journal_id = $resp->value()->me['int'];
			return $erp_Journal_id;
		}
	}
	
	public function Get_Erp_Journal($payment_method, $userId, $client)	{		
		Mage::log("Model\Commonsocket.php: Get_erp_Journal()", null, "odoolink.log", null, "odoolink.log"); 
		$prefix=Mage::getConfig()->getTablePrefix();
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$result_check = $write->query("SELECT erp_payment_id FROM ".$prefix."erp_mage_payment_mapping WHERE mage_payment_id = '$payment_method'");
		$checkList = $result_check->fetch();
		if (!$checkList){
			$arrayVal = array(
					'name'=>new odooxmlrpcval($payment_method, "string"),
					'type'=>new odooxmlrpcval('cash', "string")
				);
			$erp_payment_id = $this->Erp_Journal_create($arrayVal, $userId, $client);
			$write->query("INSERT INTO ".$prefix."erp_mage_payment_mapping VALUES('','".$payment_method."','$erp_payment_id')");
			return $erp_payment_id;
		}
		else
			return $checkList['erp_payment_id'];
	}
	
	// custom code for ODOO invoice...
	public function doErpOrderInvoice($This_order, $erpOrderId, $erpCustomerId, $userId, $client){		
		$inv_no = false;		
		$invoice_date = $This_order->getUpdatedAt();
		$Invoice = $This_order->getInvoiceCollection()->getData();
		foreach($Invoice as $I){
			$invoice_date = $I['created_at'];
			$inv_no = $I['increment_id'];
			break;
		}
		$Order_invoice_arr = array(
					'order_id'=>new odooxmlrpcval($erpOrderId,"int"),
					'date'=>new odooxmlrpcval($invoice_date,"string"),
					'mage_inv_number'=>new odooxmlrpcval($inv_no,"string")
				);		
		$msg2 = new odooxmlrpcmsg('execute');
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new odooxmlrpcval($userId, "int"));
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new odooxmlrpcval("magento.orders", "string"));
		$msg2->addParam(new odooxmlrpcval("create_order_invoice", "string"));
		$msg2->addParam(new odooxmlrpcval($Order_invoice_arr,"struct"));
		$msg2_resp = $client->send($msg2);
		$val_ser = $msg2_resp->value();
		$invoice_id = $val_ser->me["int"];
		// custom code for ODOO payment...
		$payment_method = $This_order->getPayment()->getMethodInstance()->getTitle();
		$journal_id = $this->Get_Erp_Journal($payment_method, $userId, $client);
		Mage::log("Model\Commonsocket.php: Payment amount = " . $This_order->getGrandTotal() , null, "odoolink.log");
		$Order_payment_arr = array(
					'partner_id'=>new odooxmlrpcval($erpCustomerId,"int"),
					'reference'=>new odooxmlrpcval($This_order->getIncrementId(),"string"),
					'amount'=>new odooxmlrpcval($This_order->getBaseSubtotal(),"string"),
					'invoice_id'=>new odooxmlrpcval($invoice_id,"int"),
					'journal_id'=>new odooxmlrpcval($journal_id,"int")
					);
		$order_payment = new odooxmlrpcmsg('execute');
		$order_payment->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$order_payment->addParam(new odooxmlrpcval($userId, "int"));
		$order_payment->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$order_payment->addParam(new odooxmlrpcval("magento.orders", "string"));
		$order_payment->addParam(new odooxmlrpcval("sales_order_payment", "string"));
		$order_payment->addParam(new odooxmlrpcval($Order_payment_arr, "struct"));
		$order_pay_resp = $client->send($order_payment);	
	}
	
	public function doErpOrderShipment($This_order, $erp_order_id, $userId, $client ){
		$shipment_no = false;
		$Shipment = $This_order->getShipmentsCollection()->getData();
		foreach($Shipment as $S){
			$shipment_no = $S['increment_id'];
			break;
		}
		$Order_shipment_array = array(
					'order_id'=>new odooxmlrpcval($erp_order_id,"int"),					
					'mage_ship_number'=>new odooxmlrpcval($shipment_no,"string")
				);
		$msg2 = new odooxmlrpcmsg('execute');
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new odooxmlrpcval($userId, "int"));
		$msg2->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new odooxmlrpcval("magento.orders", "string"));
		$msg2->addParam(new odooxmlrpcval("order_shipped", "string"));
		$msg2->addParam(new odooxmlrpcval($Order_shipment_array,"struct"));
		$msg2_resp = $client->send($msg2);
	}
	
	public function validate_ean13($barcode)
	{
		// check to see if barcode is 13 digits long
		if (!preg_match("/^[0-9]{13}$/", $barcode)) {
			return false;
		}

		$digits = $barcode;

		// 1. Add the values of the digits in the 
		// even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits[1] + $digits[3] + $digits[5] +
					$digits[7] + $digits[9] + $digits[11];

		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;

		// 3. Add the values of the digits in the 
		// odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits[0] + $digits[2] + $digits[4] +
				   $digits[6] + $digits[8] + $digits[10];

		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;

		// 5. The check character is the smallest number which,
		// when added to the result in step 4, produces a multiple of 10.
		$next_ten = (ceil($total_sum / 10)) * 10;
		$check_digit = $next_ten - $total_sum;

		// if the check digit and the last digit of the 
		// barcode are OK return true;
		if ($check_digit == $digits[12]) {
			return true;
		}

		return false;
	}
}