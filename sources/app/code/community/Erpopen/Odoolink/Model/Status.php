<?php

class Erpopen_Odoolink_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 'yes';
    const STATUS_DISABLED	= 'no';

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('odoolink')->__('Yes'),
            self::STATUS_DISABLED   => Mage::helper('odoolink')->__('No')
        );
    }
}