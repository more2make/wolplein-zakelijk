<?php

class Erpopen_Odoolink_Model_Products extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/products');
    }
	public function mappingerp($data){
		$productmap=Mage::getModel('odoolink/products');
		$productmap->seterp_pro_id($data['erp_product_id']);
		$productmap->setmage_pro_id($data['mage_product_id']);
		$productmap->setneed_sync('no');
		$productmap->save();
	}
	
	static public function getMageProductArray() {
        $Product = array();
		$product_ids = Mage::getModel('catalog/product')->getCollection()->getAllIds();

		foreach ($product_ids as $one_id){
			$pro = Mage::getModel('catalog/product')->load($one_id);
			if ($pro->getName()){
				array_push($Product,
				array(
						'value' => $pro->getId(),
						'label'=>Mage::helper('adminhtml')->__($pro->getName()),
						)
				);
			}
		}
        array_unshift($Product, array('label' => '--Select Magento Product--', 'value' => ''));
        return $Product;
    }
	
	static public function getErpProductArray() {
		$Product = array();
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			array_push($Product, array('label' => 'Not Available(Connection Error)', 'value' => ''));
			return $Product;
		}else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$key     = array();
			$msg_ser = new odooxmlrpcmsg('execute');
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg_ser->addParam(new odooxmlrpcval("product.product", "string"));
			$msg_ser->addParam(new odooxmlrpcval("search", "string"));
			$msg_ser->addParam(new odooxmlrpcval($key, "array"));
			$resp0    = $client->send($msg_ser);
			if ($resp0->faultCode()) {
				array_push($Product, array('label' => 'Not Available(Error in Fetching)', 'value' => ''));
				return $Product;
			}
			else 
			{
				$val    = $resp0->value()->me['array'];
				$key1 = array(new odooxmlrpcval('id','int') , new odooxmlrpcval('name', 'string'));
				$msg_ser1 = new odooxmlrpcmsg('execute');
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval($userId, "int"));
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval("product.product", "string"));
				$msg_ser1->addParam(new odooxmlrpcval("read", "string"));
				$msg_ser1->addParam(new odooxmlrpcval($val, "array"));
				$msg_ser1->addParam(new odooxmlrpcval($key1, "array"));
				$resp1   = $client->send($msg_ser1);
				if ($resp1->faultCode()) {
					$msg = 'Not Available- Error: '.$resp1->faultString();
					array_push($Product, array('label' => $msg, 'value' => ''));
					return $Product;
				} 
				else 
				{
					$value_array=$resp1->value()->scalarval();
					$count = count($value_array);

					for($x=0;$x<$count;$x++)
					{
					   $id = $value_array[$x]->me['struct']['id']->me['int'];
					   $name = $value_array[$x]->me['struct']['name']->me['string'];
					   array_push($Product,
						array(
								'value' => $id,
								'label'=>Mage::helper('adminhtml')->__($name))
						);
					}
				}	
			}
			array_unshift($Product, array('label' => '--Select ODOO Product--', 'value' => ''));
			return $Product;
		}		
	}
}
?>