<?php
class Erpopen_Odoolink_Model_Payment extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/payment');
    }
	
	static public function getMagePaymentArray() {        
		$payments = Mage::getSingleton('payment/config')->getActiveMethods();		
		$methods = array();
		$payment_array = array();
		foreach ($payments as $paymentCode=>$paymentModel) {
			$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
			$methods[$paymentCode] = array(
				'label'   => $paymentTitle,
				'value' => $paymentCode,
			);
			array_push($payment_array,
						array(
							'value' => $paymentTitle,
							'label'=>Mage::helper('adminhtml')->__($paymentTitle),					
							));
		}	
        array_unshift($payment_array, array('label' => '--Select Magento Payment Method--', 'value' => ''));
        return $payment_array;
    }
	
	static public function getErpPaymentArray() {
		$Payment = array();
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			array_push($Payment, array('label' => 'Not Available(Connection Error)', 'value' => ''));
			return $Payment;
		}else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$key1 = array(	new odooxmlrpcval('bank', 'string'),
							new odooxmlrpcval('cash', 'string')	);
			$key = array(new odooxmlrpcval(
						array(	new odooxmlrpcval('type' , "string"), 
								new odooxmlrpcval('in',"string"),
								new odooxmlrpcval($key1,"array")),"array"),
				);
			$msg_ser = new odooxmlrpcmsg('execute');
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg_ser->addParam(new odooxmlrpcval("account.journal", "string"));
			$msg_ser->addParam(new odooxmlrpcval("search", "string"));
			$msg_ser->addParam(new odooxmlrpcval($key, "array"));
			$resp0    = $client->send($msg_ser);
			if ($resp0->faultCode()) {
				array_push($Payment, array('label' => 'Not Available(Error in Fetching)', 'value' => ''));
				return $Payment;
			}
			else 
			{
				$val    = $resp0->value()->me['array'];
				$key1 = array(new odooxmlrpcval('id','int') , new odooxmlrpcval('name', 'string'));
				$msg_ser1 = new odooxmlrpcmsg('execute');
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval($userId, "int"));
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval("account.journal", "string"));
				$msg_ser1->addParam(new odooxmlrpcval("read", "string"));
				$msg_ser1->addParam(new odooxmlrpcval($val, "array"));
				$msg_ser1->addParam(new odooxmlrpcval($key1, "array"));
				$resp1   = $client->send($msg_ser1);
				if ($resp1->faultCode()) {
					$msg = 'Not Available- Error: '.$resp1->faultString();
					array_push($Payment, array('label' => $msg, 'value' => ''));
					return $Payment;
				}
				else 
				{
					$value_array=$resp1->value()->scalarval();
					$count = count($value_array);			
				
					for($x=0;$x<$count;$x++)
					{
					   $id = $value_array[$x]->me['struct']['id']->me['int'];
					   $name = $value_array[$x]->me['struct']['name']->me['string'];
					   array_push($Payment,
						array(
								'value' => $id,
								'label'=>Mage::helper('adminhtml')->__($name))
						);
					}
				}
			}
			array_unshift($Payment, array('label' => '--Select ODOO Payment Method--', 'value' => ''));
			return $Payment;
		}
	}
}
?>