<?php

class Erpopen_Odoolink_Model_Currency extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/currency');
    }
	
	static public function getMageCurrencyArray() {
        $Currency = array();		
		$currencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
		foreach($currencies as $currency){
			array_push($Currency,
			array(
					'value' => $currency,
					'label'=>Mage::helper('adminhtml')->__($currency),					
					)
			);	
		}		
        array_unshift($Currency, array('label' => '--Select Magento Currency--', 'value' => ''));
        return $Currency;
    }
	
	static public function getErpCurrencyArray() {
		$Currency = array();
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			array_push($Currency, array('label' => 'Not Available(Connection Error)', 'value' => ''));
			return $Currency;
		}else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$key = array(new odooxmlrpcval(
						array(	new odooxmlrpcval('type' , "string"), 
								new odooxmlrpcval('=',"string"),
								new odooxmlrpcval('sale',"string")),"array"),
				);
			$msg_ser = new odooxmlrpcmsg('execute');
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
			$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg_ser->addParam(new odooxmlrpcval("product.pricelist", "string"));
			$msg_ser->addParam(new odooxmlrpcval("search", "string"));
			$msg_ser->addParam(new odooxmlrpcval($key, "array"));
			$resp0    = $client->send($msg_ser);
			if ($resp0->faultCode()) {
				array_push($Currency, array('label' => 'Not Available(Error in Fetching)', 'value' => ''));
				return $Currency;
			}
			else 
			{
				$val    = $resp0->value()->me['array'];
				$key1 = array(new odooxmlrpcval('id','int') , new odooxmlrpcval('name', 'string'),new odooxmlrpcval('currency_id', 'string'));
				$msg_ser1 = new odooxmlrpcmsg('execute');
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval($userId, "int"));
				$msg_ser1->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$msg_ser1->addParam(new odooxmlrpcval("product.pricelist", "string"));
				$msg_ser1->addParam(new odooxmlrpcval("read", "string"));
				$msg_ser1->addParam(new odooxmlrpcval($val, "array"));
				$msg_ser1->addParam(new odooxmlrpcval($key1, "array"));
				$resp1   = $client->send($msg_ser1);
				if ($resp1->faultCode()) {
					$msg = 'Not Available- Error: '.$resp1->faultString();
					array_push($Currency, array('label' => $msg, 'value' => ''));
					return $Currency;
				}
				else
				{
					$value_array=$resp1->value()->scalarval();					
					$count = count($value_array);					
					for($x=0;$x<$count;$x++)
					{						
					   $id = $value_array[$x]->me['struct']['id']->me['int'];
					   $name = $value_array[$x]->me['struct']['name']->me['string'];
					   $code = $value_array[$x]->me['struct']['currency_id']->me['array'][1]->me['string'];
					   array_push($Currency,
						array(
								'value' => $id,
								'label'=>Mage::helper('adminhtml')->__($name.' - '.$code))
						);
					}
				}	
			}
			array_unshift($Currency, array('label' => '--Select ODOO Pricelist--', 'value' => ''));
			return $Currency;
		}
	}
}
?>