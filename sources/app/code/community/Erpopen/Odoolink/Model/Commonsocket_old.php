<?php
include (Mage::GetBaseDir("skin")."/adminhtml/default/default/MageErpSync/xmlrpc.inc");

class Erpopen_Odoolink_Model_Commonsocket extends Mage_Core_Model_Abstract	{
	public function getSocketConnect()	{			
		$sock = new xmlrpc_client(Mage::getStoreConfig('odoolink/odoolink/url').":".Mage::getStoreConfig('odoolink/odoolink/port')."/xmlrpc/common");
		$msg = new xmlrpcmsg('login');
		$msg->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/uname'), "string"));
		$msg->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$resp =  $sock->send($msg);
		if ($resp->errno != 0){
			return 'error';
		}
		else{
			$val = $resp->value();
			$id = $val->scalarval();
			if($id > 0) 
				return $id;
			else
				return 'error';
		}
	}
	
	public function getClientConnect(){
		return new xmlrpc_client(Mage::getStoreConfig('odoolink/odoolink/url').":".Mage::getStoreConfig('odoolink/odoolink/port')."/xmlrpc/object");
	}
	
	public function getAllSyncedCategoryIds($need_sync=true){
		$mage_ids = array();
		$model = Mage::getModel('odoolink/category');
		$mappingcollection =  $model->getCollection();
		foreach($mappingcollection as $map){
			if($need_sync){
				if($map->getNeedSync() == 'no'){
					array_push($mage_ids, $map->getMageCatId());
				}
			}else{
				array_push($mage_ids, $map->getMageCatId());
			}
		}		
		return $mage_ids;
	}
	
	public function CreateCategory(){
		$synced_ids = $this->getAllSyncedCategoryIds();
		if(count($synced_ids) > 0){
			$category_collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToFilter('entity_id', array('nin'=>$synced_ids));
		}else{
			$category_collection = Mage::getModel('catalog/category')->getCollection();
		}
		$category_ids = $category_collection->getAllIds();		
		$userId = -1;$signal = 0;$parent_id = 1;
		if($userId<=0)
			$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$signal = 0;
		}
		else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$max_level=$write->query("SELECT MAX(level) FROM ".$prefix."catalog_category_entity");
			$max_level=$max_level->fetch();
			for($i=1;$i<=$max_level["MAX(level)"];$i++)	{
				foreach($category_ids as $one_id)	{
					$cat = Mage::getModel('catalog/category')->load($one_id);
					if(!$cat->getName())
						continue;
					if($cat->getLevel()==$i){
						$result_check=$write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$cat->getId()."'");
						$check_one=$result_check->fetch();
						if(!$check_one){
							$check_parent1=$write->query("SELECT parent_id FROM ".$prefix."catalog_category_entity WHERE entity_id='".$cat->getId()."'");
							$check_fetch1=$check_parent1->fetch();
							$check_parent2=$write->query("SELECT erp_cat_id FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$check_fetch1["parent_id"]."'");
							$check_fetch2=$check_parent2->fetch();
							$cat_name = urlencode($cat->getName());
							$catg_array = array(
										'name'=>new xmlrpcval($cat_name, "string"),
										'type'=>new xmlrpcval("normal", "string"),
										'mage_id'=>new xmlrpcval($cat->getId(), "int"),
										'method'=>new xmlrpcval('create', "string")
								);
							if($check_fetch2)
								$catg_array['parent_id'] = new xmlrpcval($check_fetch2["erp_cat_id"], "string");
							$erp_catg_id = $this ->Erp_category_create($catg_array, $userId, $client);
							if($erp_catg_id>0){
								$write->query("INSERT INTO ".$prefix."erp_mage_category_mapping VALUES('','$erp_catg_id','".$cat->getId()."','no')");
							}
						}elseif($check_one["need_sync"] == 'yes'){
							$erp_catg_id = $check_one["erp_cat_id"];
							// fetching parent id from catalog_category_entity Table
							$check_parent1=$write->query("SELECT parent_id FROM ".$prefix."catalog_category_entity WHERE entity_id='".$cat->getId()."'");
							$check_fetch1=$check_parent1->fetch();
							// fetching ODOO parent category id
							$check_parent2=$write->query("SELECT erp_cat_id FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$check_fetch1["parent_id"]."'");
							$check_fetch2=$check_parent2->fetch();
							$cat_name = urlencode($cat->getName());							
							$catg_array = array(
										'name'=>new xmlrpcval($cat_name, "string"),
										'type'=>new xmlrpcval("normal", "string"),
										'category_id'=>new xmlrpcval($erp_catg_id, "int"),
										'method'=>new xmlrpcval('write', "string")
								);
							if($check_fetch2)
								$catg_array['parent_id'] = new xmlrpcval($check_fetch2["erp_cat_id"], "string");			
							$this ->Erp_category_update($catg_array, $userId, $client);
							$write->query("UPDATE ".$prefix."erp_mage_category_mapping SET need_sync='no' WHERE  	erp_cat_id='$erp_catg_id'");
						}
					}
				}
			}
		$signal = 1;
		}
		return $signal;
	}
	
	public function Erp_category_create($catg_array, $userId, $client )	{
		$msg1=new xmlrpcmsg('execute');
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new xmlrpcval($userId,"int"));
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new xmlrpcval("magento.category","string"));
		$msg1->addParam(new xmlrpcval("create_category","string"));
		$msg1->addParam(new xmlrpcval($catg_array, "struct"));
		$resp = $client->send($msg1);		
		if (!$resp->faultCode()){	
			$erp_catg_id = $resp->value()->me["int"];
			return $erp_catg_id;
		}
	}
	
	public function Erp_category_update($catg_array, $userId, $client )	{
		$msg1=new xmlrpcmsg('execute');
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new xmlrpcval($userId,"int"));
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new xmlrpcval("magento.category","string"));
		$msg1->addParam(new xmlrpcval("create_category","string"));
		$msg1->addParam(new xmlrpcval($catg_array, "struct"));
		$resp = $client->send($msg1);
	}
	
	public function getAllSyncedProductIds($need_sync=true){
		$mage_ids = array();
		$model = Mage::getModel('odoolink/products');
		$mappingcollection =  $model->getCollection();
		foreach($mappingcollection as $map){
			if($need_sync){
				if($map->getNeedSync() == 'no'){
					array_push($mage_ids, $map->getMageProId());
				}
			}else{
				array_push($mage_ids, $map->getMageProId());
			}
		}		
		return $mage_ids;	
	}
	
	public function CreateProduct()	{
		$signal = 0;
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$signal = 0;
		}
		else{
			$type ='';
			$this->CreateCategory();
			$synced_ids = $this->getAllSyncedProductIds();
			if(count($synced_ids) > 0){
				$product_collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('entity_id', array('nin'=>$synced_ids));
			}else{
				$product_collection = Mage::getModel('catalog/product')->getCollection();
			}
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$inventory = Mage::getStoreConfig('odoolink/Odoolink2/inventory');
			$prefix = Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			foreach($product_collection as $product){
				$pro_id = 0; $pro_image = false;
				$erp_categ_ids = array();
				$onepro = Mage::getModel('catalog/product')->load($product->getId());
				if(!$onepro->getName())
					continue;
				$categories = $onepro->getCategoryIds();
				if(count($categories) > 0){
					foreach($categories as $cat_id){
						$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$cat_id."'");
						$catg_one=$catg_check->fetch();
						if($catg_one["erp_cat_id"] > 0 ){
							$categ_id = $catg_one["erp_cat_id"];
							array_push($erp_categ_ids, new xmlrpcval($categ_id, 'int'));
						}
					}
				}else{
					$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='2'");
					$catg = $catg_check->fetch();
					array_push($erp_categ_ids, new xmlrpcval($catg["erp_cat_id"], 'int'));
				}
				$result_check = $write->query("SELECT * FROM ".$prefix."erp_mage_product_mapping WHERE mage_pro_id='".$onepro->getId()."'");
				$check_one = $result_check->fetch();
				try{
					$image = file_get_contents($onepro->getImageUrl());
				}catch (Exception $e){
					$image = false;
				}
				if($image) {
					$pro_image = base64_encode($image);
				}				
				$ean = $onepro->getEan();
				if(!$check_one)	{
					$type = urlencode($onepro->getTypeID());
					$name = urlencode($onepro->getName());
					$sku = urlencode($onepro->getSku());
					$desc = urlencode(strip_tags($onepro->getDescription()));
					$srt_desc = urlencode(strip_tags($onepro->getShortDescription()));
					$pro_array = array(
							'name'=>new xmlrpcval($name, "string"),
							'description'=>new xmlrpcval($desc, "string"),
							'description_sale'=>new xmlrpcval($srt_desc, "string"),
							'list_price'=>new xmlrpcval($onepro->getPrice(), "string"),
							'type'=>new xmlrpcval($type, "string"),
							'categ_ids'=>new xmlrpcval($erp_categ_ids, "array"),
							'default_code'=>new xmlrpcval($sku, "string"),				
							'weight_net'=>new xmlrpcval($onepro->getWeight(), "double"),
							'mage_id'=>new xmlrpcval($onepro->getId(), "int"),
							'method'=>new xmlrpcval('create', "string")
						);
					if($pro_image){
						$pro_array['image'] = new xmlrpcval($pro_image, "string");
					}
					if($ean){
						$check_ean = $this->validate_ean13($ean);
						if($check_ean){
							$pro_array['ean13'] = new xmlrpcval($ean, "string");
						}
					}
					$pro_id = $this ->Erp_product_create($pro_array, $userId, $client);
					if($pro_id>0){
						$write->query("INSERT INTO ".$prefix."erp_mage_product_mapping VALUES('','".$pro_id."','".$onepro->getId()."','no')");
					}
				}
				elseif($check_one["need_sync"] == 'yes'){
					$pro_id = $check_one["erp_pro_id"];
					$type = urlencode($onepro->getTypeID());
					$name = urlencode($onepro->getName());
					$sku = urlencode($onepro->getSku());
					$desc = urlencode(strip_tags($onepro->getDescription()));
					$srt_desc = urlencode(strip_tags($onepro->getShortDescription()));
					$pro_array = array(
							'name'=>new xmlrpcval($name, "string"),
							'description'=>new xmlrpcval($desc, "string"),
							'description_sale'=>new xmlrpcval($srt_desc, "string"),
							'list_price'=>new xmlrpcval($onepro->getPrice(), "string"),
							'type'=>new xmlrpcval($type, "string"),
							'categ_ids'=>new xmlrpcval($erp_categ_ids, "array"),
							'default_code'=>new xmlrpcval($sku, "string"),
							'weight_net'=>new xmlrpcval($onepro->getWeight(), "double"),
							'product_id'=>new xmlrpcval($pro_id, "int"),
							'method'=>new xmlrpcval('write', "string")
						);
					if($pro_image){
						$pro_array['image'] = new xmlrpcval($pro_image, "string");
					}
					if($ean){
						$check_ean = $this->validate_ean13($ean);
						if($check_ean){
							$pro_array['ean13'] = new xmlrpcval($ean, "string");
						}
					}
					$this->Erp_product_update($pro_array, $userId, $client);
					$write->query("UPDATE ".$prefix."erp_mage_product_mapping SET need_sync='no' WHERE erp_pro_id='$pro_id'");
				}
				if ($pro_id > 0  && $inventory == 1){
					$this->ErpCreateInventory($onepro, $pro_id, 0, $userId, $client);
				}
			}
			$signal = 1;
		}
		return $signal;
	}
	
	public function ErpCreateInventory($onepro, $erp_pro_id, $ordered_qty=0, $userId, $client){
		$mageproductqty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($onepro)->getQty();
		$stock_setting = Mage::getStoreConfig('cataloginventory/options/can_subtract');
		if($ordered_qty>0 && $stock_setting==1){
			$mageproductqty = $mageproductqty + $ordered_qty;
		}
		$move_arr =  array(
				'new_quantity'=>new xmlrpcval($mageproductqty,"int"),
				'product_id'=>new xmlrpcval($erp_pro_id,"int")
		);
		$inv = new xmlrpcmsg('execute');
		$inv->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$inv->addParam(new xmlrpcval($userId, "int"));
		$inv->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$inv->addParam(new xmlrpcval("magento.orders", "string"));
		$inv->addParam(new xmlrpcval("update_quantity", "string"));
		$inv->addParam(new xmlrpcval($move_arr, "struct"));
		$cli_resp = $client->send($inv);
	}
	
	public function Erp_product_create($pro_array, $userId, $client )	{
		$msg2 = new xmlrpcmsg('execute');
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new xmlrpcval($userId, "int"));
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new xmlrpcval("magento.product", "string"));
		$msg2->addParam(new xmlrpcval("create_product", "string"));
		$msg2->addParam(new xmlrpcval($pro_array, "struct"));
		$resp = $client->send($msg2);
		if (!$resp->faultCode()){	
			$erp_pro_id = $resp->value()->me["int"];
			return $erp_pro_id;
		}
	}
	
	public function Erp_product_update($pro_array, $userId, $client )	{
		$msg2 = new xmlrpcmsg('execute');
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new xmlrpcval($userId, "int"));
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new xmlrpcval("magento.product", "string"));
		$msg2->addParam(new xmlrpcval("create_product", "string"));
		$msg2->addParam(new xmlrpcval($pro_array, "struct"));
		$resp = $client->send($msg2);
	}



	public function Erp_customer_create($customer_arr, $mage_customer_id, $mage_address_id, $userId, $client )	{
		$msg1 = new xmlrpcmsg('execute');
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new xmlrpcval($userId,"int"));
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new xmlrpcval("magento.orders","string"));
		$msg1->addParam(new xmlrpcval("create_customer","string"));
		$msg1->addParam(new xmlrpcval($customer_arr,"struct"));
		$resp = $client->send($msg1);		
		$erp_customer_id = $resp->value()->me["int"];
	// mapping Entry
		if($erp_customer_id > 0 && $mage_address_id){
			$model = Mage::getModel('odoolink/odoolink');
			$model->setErpCusId($erp_customer_id);
			$model->setMageCusId($mage_customer_id);
			$model->setMageAddressId($mage_address_id);
			$model->setNeedSync('no');
			$model->save();			
			$prod_map_arr = array(  
							'cus_name'=>new xmlrpcval($erp_customer_id,"int"),
							'oe_customer_id'=>new xmlrpcval($erp_customer_id,"int"),
							'mag_customer_id'=>new xmlrpcval($mage_customer_id,"string"),
							'mag_address_id'=>new xmlrpcval($mage_address_id,"string")
						);
			$prod_map = new xmlrpcmsg('execute');
			$prod_map->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$prod_map->addParam(new xmlrpcval($userId, "int"));
			$prod_map->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$prod_map->addParam(new xmlrpcval("magento.customers", "string"));
			$prod_map->addParam(new xmlrpcval("create", "string"));
			$prod_map->addParam(new xmlrpcval($prod_map_arr, "struct"));
			$prod_map_resp = $client->send($prod_map);
		}
		return $erp_customer_id;
	}
	
	public function Erp_customer_update($customer_arr, $entityId, $userId, $client){
		$msg1 = new xmlrpcmsg('execute');
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new xmlrpcval($userId,"int"));
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new xmlrpcval("magento.orders","string"));
		$msg1->addParam(new xmlrpcval("create_customer","string"));
		$msg1->addParam(new xmlrpcval($customer_arr,"struct"));
		$resp = $client->send($msg1);		
		$customermap = Mage::getModel('odoolink/odoolink')->load($entityId);
		$customermap->setNeedSync('no');
		$customermap->save();
	}
	
	public function Erp_Tax_create($tax_array, $userId, $client)	{
			$msg2 = new xmlrpcmsg('execute');
			$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg2->addParam(new xmlrpcval($userId, "int"));
			$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg2->addParam(new xmlrpcval("account.tax", "string"));
			$msg2->addParam(new xmlrpcval("create", "string"));
			$msg2->addParam(new xmlrpcval($tax_array, "struct"));
			$resp = $client->send($msg2);
			if ($resp->errno != 0){
				return 0;
			}
			else{
				$erp_tax_id = $resp->value()->me['int'];
				return $erp_tax_id;
			}
	}
	
	public function Erp_Tax_update($tax_array, $erp_tax_id, $userId, $client)	{
		$erp_tax_key = array(new xmlrpcval($erp_tax_id, 'int'));
		$msg1 = new xmlrpcmsg('execute');
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new xmlrpcval($userId,"int"));
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new xmlrpcval("account.tax","string"));
		$msg1->addParam(new xmlrpcval("write","string"));
		$msg1->addParam(new xmlrpcval($erp_tax_key,"array"));
		$msg1->addParam(new xmlrpcval($tax_array,"struct"));
		$resp = $client->send($msg1);
	}
	
	public function Erp_Journal_create($arrayVal, $userId, $client)	{
		$msg1=new xmlrpcmsg('execute');
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'),"string"));
		$msg1->addParam(new xmlrpcval($userId,"int"));
		$msg1->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'),"string"));
		$msg1->addParam(new xmlrpcval("magento.orders","string"));
		$msg1->addParam(new xmlrpcval("create_payment_method","string"));
		$msg1->addParam(new xmlrpcval($arrayVal, "struct"));
		$resp = $client->send($msg1);
		if ($resp->errno != 0){
			return 0;
		}
		else{
			$erp_Journal_id = $resp->value()->me['int'];
			return $erp_Journal_id;
		}
	}
	
	public function Get_Erp_Journal($payment_method, $userId, $client)	{		
		$prefix=Mage::getConfig()->getTablePrefix();
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$result_check = $write->query("SELECT erp_payment_id FROM ".$prefix."erp_mage_payment_mapping WHERE mage_payment_id = '$payment_method'");
		$checkList = $result_check->fetch();
		if (!$checkList){
			$arrayVal = array(
					'name'=>new xmlrpcval($payment_method, "string"),
					'type'=>new xmlrpcval('cash', "string")
				);
			$erp_payment_id = $this->Erp_Journal_create($arrayVal, $userId, $client);
			$write->query("INSERT INTO ".$prefix."erp_mage_payment_mapping VALUES('','".$payment_method."','$erp_payment_id')");
			return $erp_payment_id;
		}
		else
			return $checkList['erp_payment_id'];
	}
	
	// custom code for ODOO invoice...
	public function doErpOrderInvoice($This_order, $erpOrderId, $erpCustomerId, $userId, $client){		
		$inv_no = false;		
		$invoice_date = $This_order->getUpdatedAt();
		$Invoice = $This_order->getInvoiceCollection()->getData();
		foreach($Invoice as $I){
			$invoice_date = $I['created_at'];
			$inv_no = $I['increment_id'];
			break;
		}
		$Order_invoice_arr = array(
					'order_id'=>new xmlrpcval($erpOrderId,"int"),
					'date'=>new xmlrpcval($invoice_date,"string"),
					'mage_inv_number'=>new xmlrpcval($inv_no,"string")
				);		
		$msg2 = new xmlrpcmsg('execute');
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new xmlrpcval($userId, "int"));
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new xmlrpcval("magento.orders", "string"));
		$msg2->addParam(new xmlrpcval("create_order_invoice", "string"));
		$msg2->addParam(new xmlrpcval($Order_invoice_arr,"struct"));
		$msg2_resp = $client->send($msg2);
		$val_ser = $msg2_resp->value();
		$invoice_id = $val_ser->me["int"];
		// custom code for ODOO payment...
		$payment_method = $This_order->getPayment()->getMethodInstance()->getTitle();
		$journal_id = $this->Get_Erp_Journal($payment_method, $userId, $client);

		$Order_payment_arr = array(
					'partner_id'=>new xmlrpcval($erpCustomerId,"int"),
					'reference'=>new xmlrpcval($This_order->getIncrementId(),"string"),
					'amount'=>new xmlrpcval($This_order->getBaseSubtotal(),"string"),
					'invoice_id'=>new xmlrpcval($invoice_id,"int"),
					'journal_id'=>new xmlrpcval($journal_id,"int")
					);
		$order_payment = new xmlrpcmsg('execute');
		$order_payment->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$order_payment->addParam(new xmlrpcval($userId, "int"));
		$order_payment->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$order_payment->addParam(new xmlrpcval("magento.orders", "string"));
		$order_payment->addParam(new xmlrpcval("sales_order_payment", "string"));
		$order_payment->addParam(new xmlrpcval($Order_payment_arr, "struct"));
		$order_pay_resp = $client->send($order_payment);	
	}
	
	public function doErpOrderShipment($This_order, $erp_order_id, $userId, $client ){
		$shipment_no = false;
		$Shipment = $This_order->getShipmentsCollection()->getData();
		foreach($Shipment as $S){
			$shipment_no = $S['increment_id'];
			break;
		}
		$Order_shipment_array = array(
					'order_id'=>new xmlrpcval($erp_order_id,"int"),					
					'mage_ship_number'=>new xmlrpcval($shipment_no,"string")
				);
		$msg2 = new xmlrpcmsg('execute');
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg2->addParam(new xmlrpcval($userId, "int"));
		$msg2->addParam(new xmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg2->addParam(new xmlrpcval("magento.orders", "string"));
		$msg2->addParam(new xmlrpcval("order_shipped", "string"));
		$msg2->addParam(new xmlrpcval($Order_shipment_array,"struct"));
		$msg2_resp = $client->send($msg2);
	}
	
	public function validate_ean13($barcode)
	{
		// check to see if barcode is 13 digits long
		if (!preg_match("/^[0-9]{13}$/", $barcode)) {
			return false;
		}

		$digits = $barcode;

		// 1. Add the values of the digits in the 
		// even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits[1] + $digits[3] + $digits[5] +
					$digits[7] + $digits[9] + $digits[11];

		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;

		// 3. Add the values of the digits in the 
		// odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits[0] + $digits[2] + $digits[4] +
				   $digits[6] + $digits[8] + $digits[10];

		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;

		// 5. The check character is the smallest number which,
		// when added to the result in step 4, produces a multiple of 10.
		$next_ten = (ceil($total_sum / 10)) * 10;
		$check_digit = $next_ten - $total_sum;

		// if the check digit and the last digit of the 
		// barcode are OK return true;
		if ($check_digit == $digits[12]) {
			return true;
		}

		return false;
	}
}
?>