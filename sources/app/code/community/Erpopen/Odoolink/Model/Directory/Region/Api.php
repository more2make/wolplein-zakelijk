<?php
class Erpopen_Odoolink_Model_Directory_Region_Api extends Mage_Directory_Model_Region_Api
{
    
    public function items($country)
    {
        try {
            $country = Mage::getModel('directory/country')->loadByCode($country);
        } catch (Mage_Core_Exception $e) {
            $this->_fault('country_not_exists', $e->getMessage());
        }

        if (!$country->getId()) {
            $this->_fault('country_not_exists');
        }

        $result = array();
        foreach ($country->getRegions() as $region) {
            $region->setName($region->getName());
            $result[] = $region->toArray(array('region_id', 'code', 'name'));
        }

        return $result;
    }
}