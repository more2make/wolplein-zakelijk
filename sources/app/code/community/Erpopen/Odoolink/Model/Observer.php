<?php
Class Erpopen_Odoolink_Model_Observer
{
	public function addMassaction($observer) 
    {
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'sales_order') 
        {
            $block->addItem('sync_order', array(
                'label' => 'Synchronise To ODOO',
                'url' => Mage::app()->getStore()->getUrl('odoolink/adminhtml_order/syncorder'),
            ));
        }
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'catalog_product') 
        {
            $block->addItem('sync_product', array(
                'label' => 'Synchronise To ODOO',
                'url' => Mage::app()->getStore()->getUrl('odoolink/adminhtml_products/syncMassProduct'),
            ));
        }
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'customer') 
        {
            $block->addItem('sync_customer', array(
                'label' => 'Synchronise To ODOO',
                'url' => Mage::app()->getStore()->getUrl('odoolink/adminhtml_odoolink/syncMassCustomer'),
            ));
        }
    }

    public function checkErpOrderStatus($erpOrderId, $userId, $client){
		$state = false;
		$id = array(new odooxmlrpcval($erpOrderId, 'int'));
		$key1 = array(new odooxmlrpcval('state','string'));
		$msg_ser = new odooxmlrpcmsg('execute');
		$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
		$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg_ser->addParam(new odooxmlrpcval("sale.order", "string"));
		$msg_ser->addParam(new odooxmlrpcval("read", "string"));
		$msg_ser->addParam(new odooxmlrpcval($id, "array"));
		$msg_ser->addParam(new odooxmlrpcval($key1, "array"));
		$resp    = $client->send($msg_ser);
		if ($resp->faultCode()) {
			$msg = $resp->faultString();
			$state = $msg;
		}
		else{
			$value_array = $resp->value()->scalarval();
			$state = $value_array[0]->me['struct']['state']->me['string'];
		}
		return $state;
	}

	public function DeleteOrder($observer) {
		$order = $observer->getOrder()->getId();
		$controller = Mage::app()->getRequest()->getControllerName();
		if($order){
			$prefix = Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');		
			$readresult=$write->query("select erp_order_id from ".$prefix."erp_mage_order_mapping where mage_order_id='".$order."'");
			$row = $readresult->fetch();
			$erporderid=$row['erp_order_id'];
			if($erporderid){
				$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
				$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
				$state = $this->checkErpOrderStatus($erporderid, $userId, $client);
				if($state == 'cancel'){
					Mage::getSingleton('adminhtml/session')->addSuccess("ODOO Order is Already Canceled");
					return true;
				}else{
					$order_cancel = new odooxmlrpcmsg('execute');
					$order_cancel->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
					$order_cancel->addParam(new odooxmlrpcval($userId, "int"));
					$order_cancel->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
					$order_cancel->addParam(new odooxmlrpcval("magento.orders", "string"));
					$order_cancel->addParam(new odooxmlrpcval("order_cancel", "string"));
					$order_cancel->addParam(new odooxmlrpcval($erporderid, "int"));
					$resp = $client->send($order_cancel);
					if($resp->faultcode()){
						$error_message = 'Failed to Cancel order on ODOO, Error: , '.$resp->faultString();
						Mage::getSingleton('adminhtml/session')->addError($error_message);
					}else{
						Mage::getSingleton('adminhtml/session')->addSuccess("Order Canceled successfully from ODOO...");
					}
				}
			}
			return True ;
		}
	}
	
	public function invoiceRegister($observer){
		$event = $observer->getEvent()->getInvoice();		
		$orderId = $event->getOrderId();
		$erpOrderDetails = Mage::getModel("odoolink/order")->_syncOrderInvoice($orderId);
	}
	
	public function checkErpDoStatus($erpOrderId, $userId, $client){
		$ship = false;
		$id = array(new odooxmlrpcval($erpOrderId, 'int'));		
		$key1 = array(new odooxmlrpcval('shipped','string'));
		$msg_ser = new odooxmlrpcmsg('execute');
		$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg_ser->addParam(new odooxmlrpcval($userId, "int"));
		$msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$msg_ser->addParam(new odooxmlrpcval("sale.order", "string"));
		$msg_ser->addParam(new odooxmlrpcval("read", "string"));
		$msg_ser->addParam(new odooxmlrpcval($id, "array"));
		$msg_ser->addParam(new odooxmlrpcval($key1, "array"));
		$resp    = $client->send($msg_ser);
		if ($resp->faultCode()) {
			$msg = $resp->faultString();
			$ship = $msg;
		}
		else{
			$value_array = $resp->value()->scalarval();
			$ship = $value_array[0]->me['struct']['shipped']->me['boolean'];
		}
		return $ship;		
	}
	
	public function shipRegister($observer){
		$orderId = $observer->getEvent()->getShipment()->getOrderId();
		$draft_state = Mage::getStoreConfig('odoolink/Odoolink2/draftstate');
		$auto_shipment = Mage::getStoreConfig('odoolink/Odoolink2/auto_shipment');
		if($orderId && $draft_state!=1 && $auto_shipment==1){
			$thisorder = Mage::getModel("sales/order")->load($orderId);
			$prefix = Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$erp_ord_que = $write->query("SELECT * FROM ".$prefix."erp_mage_order_mapping WHERE mage_order_id='".$orderId."'");
			$erp_ord_fetch = $erp_ord_que->fetch();
			if($erp_ord_fetch["erp_order_id"]>0){
				$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
				if ($userId != 'error'){
					$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
					$erpOrderId = $erp_ord_fetch["erp_order_id"];
					$status = $this->checkErpDoStatus($erpOrderId, $userId, $client);
					if(!$status){
						Mage::getModel("odoolink/commonsocket")->doErpOrderShipment($thisorder, $erpOrderId, $userId, $client);
						Mage::getSingleton('adminhtml/session')->addSuccess("Shipment Successfully updated in ODOO !!");
					}elseif($status == true){
						Mage::getSingleton('adminhtml/session')->addNotice("Shipment of this order is already created in ODOO !!");
					}else{
						Mage::getSingleton('adminhtml/session')->addError("Shipment Could not be synced on ODOO, due to Following error ".$status);
					}
				}
				else{
					Mage::getSingleton('adminhtml/session')->addError("Shipment is not updated on ODOO due to error in ODOO Configuration!!! ");
				}
			}
			else{
				Mage::getSingleton('adminhtml/session')->addNotice("Shipment Can not be Synced in ODOO Because this Order is not yet Synced on ODOO!!");
			}
		}
	}
	
	public function onAddressSave($observer){
		$controller = Mage::app()->getRequest()->getControllerName();	
		if(in_array($controller,array('customer','account','address'))){
			$customerid = $observer->getCustomerAddress()->getCustomerId();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$write->query("UPDATE ".$prefix."erp_mage_customer_mapping SET need_sync = 'yes' WHERE mage_cus_id = '$customerid'");
			$write->query("UPDATE ".$prefix."erp_mage_customer_mapping SET need_sync = 'yes' WHERE mage_cus_id = 'customer'");
		}
	}
	
	public function onCategorySave($observer){
		$controller = Mage::app()->getRequest()->getControllerName();
		if($controller == "catalog_category"){
			$cat_id = $observer->getCategory()->getId();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");		
			$write->query("UPDATE ".$prefix."erp_mage_category_mapping SET need_sync = 'yes' WHERE mage_cat_id = '$cat_id'");
		}
	}
	public function onProductSave($observer){
		$controller = Mage::app()->getRequest()->getControllerName();
		if($controller == "catalog_product" || $controller == "system_convert_gui"){
			$pro_id = $observer->getProduct()->getId();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$write->query("UPDATE ".$prefix."erp_mage_product_mapping SET need_sync = 'yes' WHERE mage_pro_id = '$pro_id'");
		}
	}
	
	public function onTaxSave($observer){
		$controller = Mage::app()->getRequest()->getControllerName();	
		if($controller == "tax_rate"){
			$tax_id = Mage::getSingleton('core/app')->getRequest()->getParam('tax_calculation_rate_id');		
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$write->query("UPDATE ".$prefix."erp_mage_tax_mapping SET need_sync = 'yes' WHERE mage_tax_id = '$tax_id'");
		}
	}
	
	public function getErpTaxIdByTaxClass($TaxClassId, $userId, $client){
		$tax_id = 0;
		$tax_rates = array();
		//$store = $This_order->getStore();
		$prefix = Mage::getConfig()->getTablePrefix();
		$taxCalculationModel = Mage::getSingleton('tax/calculation');
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$request = $taxCalculationModel->getRateOriginRequest();
		$tax_rates = $taxCalculationModel->getAppliedRates($request->setProductClassId($TaxClassId));
		if(count($tax_rates)){
			$tax_code = $tax_rates[0]['id'];
			$tax_apply1 = $write->query("SELECT erp_tax_id FROM ".$prefix."erp_mage_tax_mapping WHERE mage_tax_code='".$tax_code."'");
			$tax_apply_result1 = $tax_apply1->fetch();
			if(!$tax_apply_result1){
				$result = $write->query("SELECT rate FROM ".$prefix."tax_calculation_rate WHERE code='$tax_code'");
				$tax_rate_result = $result->fetch();
				$rate = $tax_rate_result["rate"]/100;
				$tax_array =  array('name'=>new odooxmlrpcval($tax_code,"string"),
									'description'=>new odooxmlrpcval($tax_code,"string"),
									'type'=>new odooxmlrpcval('percent',"string"),
									'amount'=>new odooxmlrpcval($rate,"string")
									);
				$tax_id = Mage::getModel("odoolink/commonsocket")->Erp_Tax_create($tax_array, $userId, $client);
			}else{
				$tax_id = $tax_apply_result1['erp_tax_id'];
			}
		}
		return $tax_id;
	}
	
	public function sync_Product($mage_pro_id, $ordered_qty, $userId, $client){
		Mage::log("Sync Product");
		$image = false; $erp_categ_ids = array();
		$prefix=Mage::getConfig()->getTablePrefix();
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$inventory = Mage::getStoreConfig('odoolink/Odoolink2/inventory');
		$obj = Mage::getModel('catalog/product');
		$_product = $obj->load($mage_pro_id);
		$type = urlencode($_product->getTypeID());
		$categories = $_product->getCategoryIds();
		if(count($categories) > 0){
			foreach($categories as $cat_id){
				$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$cat_id."'");
				$catg_one=$catg_check->fetch();
				if($catg_one["erp_cat_id"] > 0 ){
					$categ_id = $catg_one["erp_cat_id"];
					array_push($erp_categ_ids, new odooxmlrpcval($categ_id, 'int'));
				}
			}
		}else{
			$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='2'");
			$catg = $catg_check->fetch();
			array_push($erp_categ_ids, new odooxmlrpcval($catg["erp_cat_id"], 'int'));
		}
		try{
			$image = file_get_contents($_product->getImageUrl());
		}catch (Exception $e){
			$image = false;
		}
		if($image) {
			$pro_image = base64_encode($image);
		}
		$name = urlencode($_product->getName());
		$sku = urlencode($_product->getSku());
		$desc = urlencode(strip_tags($_product->getDescription()));
		$srt_desc = urlencode(strip_tags($_product->getShortDescription()));
		$pro_array = array(
					'name'=>new odooxmlrpcval($name, "string"),
					'description'=>new odooxmlrpcval($desc, "string"),
					'description_sale'=>new odooxmlrpcval($srt_desc, "string"),
					'list_price'=>new odooxmlrpcval($_product->getPrice(), "string"),
					'type'=>new odooxmlrpcval($type, "string"),
					'categ_ids'=>new odooxmlrpcval($erp_categ_ids, "array"),
					'default_code'=>new odooxmlrpcval($sku, "string"),
					'weight_net'=>new odooxmlrpcval($_product->getWeight(), "double"),
					'mage_id'=>new odooxmlrpcval($mage_pro_id, "int"),
					'method'=>new odooxmlrpcval('create', "string")
			);
		
		if($pro_image){
			$pro_array['image'] = new odooxmlrpcval($pro_image, "string");
		}
		$pro_id = Mage::getModel("odoolink/commonsocket")->Erp_product_create($pro_array, $userId, $client);
		if($pro_id>0){
			$write->query("INSERT INTO ".$prefix."erp_mage_product_mapping VALUES('','".$pro_id."','".$mage_pro_id."','no')");
			if($inventory == 1){
				Mage::getModel("odoolink/commonsocket")->ErpCreateInventory($_product, $pro_id, $ordered_qty, $userId, $client);
			}
		}
		return $pro_id;
	}
	
	public function createErpAddress($flat_address, $parent_id, $mage_customer_id, $mage_address_id, $userId, $client ){
		Mage::log ('CreateErpAddress: Parent_ID: '.$parent_id);
		Mage::log ('CreateErpAddress: AccountID: '.$mage_customer_id);
		Mage::log ('CreateErpAddress: AddressID: '.$mage_address_id);
		$flag = false; $bool = false; $type = '';
		$erp_cus_id = 0;$mapId = 0;
		if($flat_address['address_type'] == 'billing'){
			$type = 'invoice';
		}
		if($flat_address['address_type'] == 'shipping')
			$type = 'delivery';
		$model = Mage::getModel('odoolink/odoolink');
		$streets = $flat_address->getStreet();
		if(count($streets)>1){
			$street = urlencode($streets[0]);
			$street2 = urlencode($streets[1]);
		}else{
			$street = urlencode($streets[0]);
			$street2 = urlencode('');
		}
		Mage::log ('Housenumber additions');
		$housenumber = urlencode($flat_address->getHousenumber());
		Mage::log ('Housenumber: '.$housenumber);
		$housenumberadd = urlencode($flat_address->getHousenumberadd());
		Mage::log ('HousenumberAdd: '.$housenumberadd);
		$name = urlencode($flat_address->getName());
		$email = urlencode($flat_address->getEmail());
		$city = urlencode($flat_address->getCity());
		$region = urlencode($flat_address->getRegion());
		$company = urlencode($flat_address->getCompany());
		if (!($flat_address->getVatId())) {
			$vatid = urlencode($flat_address->getVatId());
		}else{
			$vatid = urlencode('');
		}
		if($mage_address_id > 0){
			$addresscollection =  $model->getCollection()
										->addFieldToFilter('mage_cus_id',array('eq'=>$mage_customer_id))
										->addFieldToFilter('mage_address_id',array('eq'=>$mage_address_id));
			if(count($addresscollection)>0){
				foreach($addresscollection as $add){
					$mapId = $add->getEntityId();
					$erp_cus_id = $add->getErpCusId();
					if($add->getNeedSync() == 'yes'){						
						$customer_arr =  array(
							'name'=>new odooxmlrpcval($name,"string"),
							'street'=>new odooxmlrpcval($street,"string"),
							'street2'=>new odooxmlrpcval($street2,"string"),
							'city'=>new odooxmlrpcval($city,"string"),
							'email'=>new odooxmlrpcval($email,"string"),
							'zip'=>new odooxmlrpcval($flat_address->getPostcode(),"string"),
							'phone'=>new odooxmlrpcval($flat_address->getTelephone(),"string"),
							'fax'=>new odooxmlrpcval($flat_address->getFax(),"string"),
							'country_code'=>new odooxmlrpcval($flat_address->getCountryId(),"string"),
							'region'=>new odooxmlrpcval($region,"string"),
							'company'=>new odooxmlrpcval($company,"string"),
							'type'=>new odooxmlrpcval($type,"string"),
							'method'=>new odooxmlrpcval('write',"string"),
							'partner_id'=>new odooxmlrpcval($erp_cus_id,"int"),
							'vat_id'=>new odooxmlrpcval($vatid,"string"),
							'housenumber'=>new odooxmlrpcval($housenumber,"string"),
							'housenumberadd'=>new odooxmlrpcval($housenumberadd,"string"),
						);
						Mage::getModel("odoolink/commonsocket")->Erp_customer_update($customer_arr, $mapId, $userId, $client);
					}
				}
			}else{
				$flag = true;
			}
		}else{ 
			$flag = true;
		}
		if($flag == true){			
			$customer_arr =  array(
							'magento_user_id'=>new odooxmlrpcval($parent_id,"int"),
							'name'=>new odooxmlrpcval($name,"string"),
							'street'=>new odooxmlrpcval($street,"string"),
							'street2'=>new odooxmlrpcval($street2,"string"),
							'city'=>new odooxmlrpcval($city,"string"),
							'email'=>new odooxmlrpcval($email,"string"),
							'zip'=>new odooxmlrpcval($flat_address->getPostcode(),"string"),
							'phone'=>new odooxmlrpcval($flat_address->getTelephone(),"string"),
							'fax'=>new odooxmlrpcval($flat_address->getFax(),"string"),
							'country_code'=>new odooxmlrpcval($flat_address->getCountryId(),"string"),
							'region'=>new odooxmlrpcval($region,"string"),
							'company'=>new odooxmlrpcval($company,"string"),
							'type'=>new odooxmlrpcval($type,"string"),
							'customer'=>new odooxmlrpcval(false,"boolean"),
							'use_parent_address'=>new odooxmlrpcval($bool,"boolean"),
							'method'=>new odooxmlrpcval('create',"string"),
							'vat_id'=>new odooxmlrpcval($vatid,"string"),
							'housenumber'=>new odooxmlrpcval($housenumber,"string"),
							'housenumberadd'=>new odooxmlrpcval($housenumberadd,"string"),
						);
			$erp_cus_id = Mage::getModel("odoolink/commonsocket")->Erp_customer_create($customer_arr, $mage_customer_id, $mage_address_id, $userId, $client);
		}
		return $erp_cus_id;
	}
	
	public function checkAddresses($This_order){
		$flag = false;		
		if($This_order->getShippingAddressId() && $This_order->getBillingAddressId()){
			$s = $This_order->getShippingAddress();
			$b = $This_order->getBillingAddress();
			
			if($s['street'] != $b['street'])
				$flag = true;
			if($s['postcode'] != $b['postcode'])
				$flag = true;
			if($s['city'] != $b['city'])
				$flag = true;
			if($s['region'] != $b['region'])
				$flag = true;
			if($s['country_id'] != $b['country_id'])
				$flag = true;
			if($s['firstname'] != $b['firstname'])
				$flag = true;
		}		
		return $flag;
	}
	
	public function getErpOrderAddresses($This_order, $userId, $client){
		$partner_id = 0;
		$partner_shipping_id = 0;
		$partner_invoice_id = 0;
		$s_id = 0;
		$b_id = 0;
		$model = Mage::getModel('odoolink/odoolink');
		$CommonSocket = Mage::getModel("odoolink/commonsocket");
		$s = $This_order->getShippingAddress();
		$b = $This_order->getBillingAddress();

		if($s){
			$s->setEmail($This_order->getCustomerEmail());
			$s_id = $s->getCustomerAddressId();
			if (!$s_id){
				$s_id = 0;
			}
		}
		if($b){
			$b->setEmail($This_order->getCustomerEmail());
			$b_id = $b->getCustomerAddressId();
			if (!$b_id){
				$b_id = 0;
			};
		}
		Mage::log ('My Shipping ID: '.$s_id);
		Mage::log ('My Billing ID: '.$b_id);
		if($This_order->getCustomerIsGuest() == 1){			
			$customer_arr =  array(
							'is_user'=>new odooxmlrpcval(true,"boolean"),
							'name'=>new odooxmlrpcval(urlencode($This_order->getCustomerName()),"string"),
							'email'=>new odooxmlrpcval(urlencode($This_order->getCustomerEmail()),"string"),
							'method'=>new odooxmlrpcval('create',"string"),
							'store_id'=>new odooxmlrpcval($This_order->getStoreId(),"string")
					);
			$partner_id = $CommonSocket->Erp_customer_create($customer_arr, 0, 0, $userId, $client);
			$isDifferent = $this->checkAddresses($This_order);
			if($isDifferent == true){
				$partner_shipping_id = $this->createErpAddress($s, $partner_id, 0, 0,$userId, $client);
				$partner_invoice_id = $this->createErpAddress($b, $partner_id, 0, 0,$userId, $client);
			}else{
				$partner_invoice_id = $this->createErpAddress($b, $partner_id, 0, 0,$userId, $client);
				$partner_shipping_id = $partner_invoice_id;
			}
		}
		if($This_order->getCustomerId() > 0){
			$mappingcollection = $model->getCollection()
										->addFieldToFilter('mage_cus_id',array('eq'=>$This_order->getCustomerId()))
										->addFieldToFilter('mage_address_id',array('eq'=>"customer"));
			if(count($mappingcollection)>0){
				foreach($mappingcollection as $map){					
					$partner_id = $map->getErpCusId();					
					$entityId = $map->getEntityId();
					$mapNeedSync = $map->getNeedSync();
				
					if($mapNeedSync == 'yes'){
						$customer_arr =  array(
								'is_user'=>new odooxmlrpcval(true,"boolean"),
								'name'=>new odooxmlrpcval(urlencode($This_order->getCustomerName()),"string"),
								'email'=>new odooxmlrpcval(urlencode($This_order->getCustomerEmail()),"string"),
								'method'=>new odooxmlrpcval('write',"string"),
								'partner_id'=>new odooxmlrpcval($partner_id,"int"),
								'store_id'=>new odooxmlrpcval($This_order->getStoreId(),"string")
							);
						$CommonSocket->Erp_customer_update($customer_arr, $entityId, $userId, $client);
					}
				}
				$isDifferent = $this->checkAddresses($This_order);
				if($isDifferent == true){
					if($s_id == $b_id) {
						$s_id = 0;
					}
					$partner_shipping_id = $this->createErpAddress($s, $partner_id, $This_order->getCustomerId(), $s_id,$userId, $client);
					$partner_invoice_id = $this->createErpAddress($b, $partner_id, $This_order->getCustomerId(), $b_id,$userId, $client);
				}else{
					$partner_invoice_id = $this->createErpAddress($b, $partner_id, $This_order->getCustomerId(), $b_id,$userId, $client);
					$partner_shipping_id = $partner_invoice_id;
				}
			}else{
				$customer_arr =  array(
                    'is_user'=>new odooxmlrpcval(true,"boolean"),
					'name'=>new odooxmlrpcval(urlencode($This_order->getCustomerName()),"string"),
					'email'=>new odooxmlrpcval(urlencode($This_order->getCustomerEmail()),"string"),
					'method'=>new odooxmlrpcval('create',"string"),
                    'store_id'=>new odooxmlrpcval($This_order->getStoreId(),"string")
                );
				$partner_id = $CommonSocket->Erp_customer_create($customer_arr, $This_order->getCustomerId(), 'customer', $userId, $client);
				$isDifferent = $this->checkAddresses($This_order);
				if($isDifferent == true){
					if($s_id == $b_id) {
						$s_id = 0;
					}
					$partner_shipping_id = $this->createErpAddress($s, $partner_id, $This_order->getCustomerId(), $s_id,$userId, $client);
					
					$partner_invoice_id = $this->createErpAddress($b, $partner_id, $This_order->getCustomerId(), $b_id,$userId, $client);
				}else{
					$partner_invoice_id = $this->createErpAddress($b, $partner_id, $This_order->getCustomerId(), $b_id,$userId, $client);
					$partner_shipping_id = $partner_invoice_id;
				}
			}
		}
		return array($partner_invoice_id, $partner_invoice_id, $partner_shipping_id);
	}
	
	public function afterAdminOrderPlace($observer){
		$lastOrderId = $observer->getOrder()->getId();		
		if (!$lastOrderId)
			return;
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
		if ($userId != 'error'){
			$This_order = Mage::getModel('sales/order')->load($lastOrderId);
			/***************** Order Synchronization  *********************/
			$erpOrderDetails = Mage::getModel("odoolink/order")->_syncSpecificOrder($lastOrderId, $userId, $client);
			$erpOrderId = $erpOrderDetails[0];
			$partner_id = $erpOrderDetails[1];
			$draft_state = Mage::getStoreConfig('odoolink/Odoolink2/draftstate');
			$auto_invoice = Mage::getStoreConfig('odoolink/Odoolink2/auto_invoice');
			//$order_state = $This_order['status'];
			// if($draft_state!=1  && $erpOrderId >0 && $partner_id >0 && $order_state !='pending'){
			//	/***************** Erp Order confirm *********************/
			//	$method = new odooxmlrpcmsg('exec_workflow');
			//	$method->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			//	$method->addParam(new odooxmlrpcval($userId, "int"));
			//	$method->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			//	$method->addParam(new odooxmlrpcval("sale.order", "string"));
			//	$method->addParam(new odooxmlrpcval("order_confirm", "string"));
			//	$method->addParam(new odooxmlrpcval($erpOrderId, "int"));
			//	$method_resp = $client->send($method);
			//	// ODOO invoice done
			//	if($This_order->hasInvoices() && $auto_invoice==1){
			//		Mage::getModel("odoolink/commonsocket")->doErpOrderInvoice($This_order, $erpOrderId, $partner_id, $userId, $client);
			//	}
			//}
		}
	}
	
	public function afterPlaceOrder($observer){
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
		$OrderIds = $observer->getOrderIds();
		if ($userId != 'error'){
			foreach($OrderIds as $lastOrderId){
				$This_order = Mage::getModel('sales/order')->load($lastOrderId);
				/***************** Order Synchronization  *********************/
				$erpOrderDetails = Mage::getModel("odoolink/order")->_syncSpecificOrder($lastOrderId, $userId, $client);
				$erpOrderId = $erpOrderDetails[0];
				$partner_id = $erpOrderDetails[1];
				$draft_state = Mage::getStoreConfig('odoolink/Odoolink2/draftstate');
				$auto_invoice = Mage::getStoreConfig('odoolink/Odoolink2/auto_invoice');
				//$order_state = $This_order['status'];
				//if($draft_state !=1 && $erpOrderId >0 && $partner_id >0 && $order_state != 'new'){
				//	/***************** Erp Order confirm *********************/
				//	$method = new odooxmlrpcmsg('exec_workflow');
				//	$method->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				//	$method->addParam(new odooxmlrpcval($userId, "int"));
				//	$method->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				//	$method->addParam(new odooxmlrpcval("sale.order", "string"));
				//	$method->addParam(new odooxmlrpcval("order_confirm", "string"));
				//	$method->addParam(new odooxmlrpcval($erpOrderId, "int"));
				//	$method_resp = $client->send($method);
				//	// ODOO invoice done
				//	if($This_order->hasInvoices() && $auto_invoice==1){
				//		Mage::getModel("odoolink/commonsocket")->doErpOrderInvoice($This_order, $erpOrderId, $partner_id, $userId, $client);
				//	}
				//}
			}
		}
	}
}
