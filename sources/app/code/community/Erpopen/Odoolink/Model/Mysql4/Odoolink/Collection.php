<?php

class Erpopen_Odoolink_Model_Mysql4_Odoolink_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('odoolink/odoolink');
    }
}