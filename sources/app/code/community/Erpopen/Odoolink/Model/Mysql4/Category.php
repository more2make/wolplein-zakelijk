<?php
class Erpopen_Odoolink_Model_Mysql4_Category extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the Odoolink_id refers to the key field in your database table.
        $this->_init('odoolink/category', 'entity_id');
    }
}