<?php
class Erpopen_Odoolink_Model_Api extends Mage_Api_Model_Resource_Abstract
{
       
    public function customermap($data){
        Mage::getModel('odoolink/odoolink')->mappingerp($data);
        return true;
    }
     
    public function categorymap($data){
        Mage::getModel('odoolink/category')->mappingerp($data);
        return true;
    }
     
    public function productmap($data){
        Mage::getModel('odoolink/products')->mappingerp($data);
        return true;
    }
     
    public function taxmap($data){
        // Mage::getModel('odoolink/tax')->mappingerp($data['erp_tax_id'],$data['mage_tax_id']);
        return true;
    }
		
    // method for retriving all mapping data...
    public function getcustomers(){
        $prefix = Mage::getConfig()->getTablePrefix();
        $write = Mage::getSingleton("core/resource")->getConnection("core_write");
        $result_check=$write->query("SELECT * FROM ".$prefix."erp_mage_customer_mapping");
        $data = $result_check->fetchAll();
        return $data;
    }
     
    public function getcategories(){
        $prefix = Mage::getConfig()->getTablePrefix();
        $write = Mage::getSingleton("core/resource")->getConnection("core_write");
        $result_check=$write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping");
        $data = $result_check->fetchAll();
        return $data;
    }
     
    public function getproducts(){
        $prefix = Mage::getConfig()->getTablePrefix();
        $write = Mage::getSingleton("core/resource")->getConnection("core_write");
        $result_check=$write->query("SELECT * FROM ".$prefix."erp_mage_product_mapping");
        $data = $result_check->fetchAll();
        return $data;
    }

    public function syncNext() {
        $prefix=Mage::getConfig()->getTablePrefix();
        $sql = Mage::getSingleton("core/resource")->getConnection("core_write");
        $unsynced_order_ids = $sql->fetchCol("SELECT entity_id FROM ".$prefix."sales_flat_order WHERE entity_id not in (select mage_order_id from ".$prefix."erp_mage_order_mapping)");

        if (count($unsynced_order_ids)) {
            $mage_order_id = reset($unsynced_order_ids);
            return $this->syncOrder($mage_order_id);
        } else {
            return "All orders are synced.";
        }
    }

    public function syncByDate($mage_start_date, $mage_end_date) {
        Mage::log ('SyncByDate');
        Mage::log ('SyncByDate:Startdate: '.$mage_start_date);
        Mage::log ('SyncByDate:Enddate: '.$mage_end_date);
        $prefix=Mage::getConfig()->getTablePrefix();
        if (!$mage_start_date || !$mage_end_date) {
            return array("Invalid request! Please specify 'start_date' and 'end_date'.");
        }
        $sql = Mage::getSingleton("core/resource")->getConnection("core_write");
        $unsynced_order_ids = $sql->fetchCol("SELECT entity_id FROM ".$prefix."sales_flat_order WHERE entity_id not in (select mage_order_id from ".$prefix."erp_mage_order_mapping) and created_at > :startdate and created_at < :enddate", array('startdate' => $mage_start_date, 'enddate' => $mage_end_date));
        Mage::log ('SyncByDate:OrderIDS: '.$unsynced_order_ids);
        if (count($unsynced_order_ids)) {
            foreach ($unsynced_order_ids as $mage_order_id) {
                $this->syncOrder($mage_order_id);
            }
            Mage::log ('All new orders in this date range are synced.');
            return 'All new orders in this date range are synced.';
        } else {
            Mage::log ('All orders in this date range are synced.');
            return 'All orders in this date range are synced.';
        }
    }

    public function syncOrderById($mage_order_id) {
        if (!$mage_order_id) {
            return 'Invalid request, please specify orderid parameter.';
        }
        return $this->syncOrder($mage_order_id);
    }
	
	public function syncStockData($stock_data, $s2)
	{
		Mage::log ('Stock data:' . $stock_data . " - " . $s2, null, "syncstock.log");
	}
	
	public function syncGroupPrices($group_price_data)
	{
		$product=Mage::getModel('catalog/product')->load($group_price_data['product_id']);
		if($product)
		{
			$product->setData('group_price',array());
			$product->save();
			$new_price = array();
			foreach($group_price_data['prices'] as $group_price)
			{
				$pricegroupdata = array ('website_id'=>0, 'cust_group'=>$group_price['group_id'], 'price'=>$group_price['price']);
				$new_price[] = $pricegroupdata;
			}
			$product->setData('group_price',$new_price);
			$product->save();
		}
	}

    private function syncOrder($mage_order_id) {
        $order_model = Mage::getModel('odoolink/order');
        $ordercollection = $order_model->getCollection()->addFieldToFilter('mage_order_id',array('eq'=>$mage_order_id));
        if (count($ordercollection)==0){
            $userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
            $client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
            $erpOrderDetails = Mage::getModel("odoolink/order")->_syncSpecificOrder($mage_order_id, $userId, $client);
			Mage::log ('Order synced');
			$order = Mage::getModel('sales/order')->load($mage_order_id);
			if($order->hasInvoices()){
                                Mage::log ('Order has invoices');
				Mage::getModel("odoolink/order")->_syncOrderInvoice($mage_order_id);
			}
			
            return $mage_order_id." synced to erp.";
        } else {
            return $mage_order_id." was already synced!";
        }
    }
	
	
}
