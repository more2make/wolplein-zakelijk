<?php
class Erpopen_Odoolink_Model_Order extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/order');
    }
    
    public function checkErpInvoiceStatus($erpOrderId, $userId, $client){
                $invoice = false;
                $id = array(new odooxmlrpcval($erpOrderId, 'int'));
                $key1 = array(new odooxmlrpcval('invoice_exists','string'));
                $msg_ser = new odooxmlrpcmsg('execute');
                $msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
                $msg_ser->addParam(new odooxmlrpcval($userId, "int"));
                $msg_ser->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
                $msg_ser->addParam(new odooxmlrpcval("sale.order", "string"));
                $msg_ser->addParam(new odooxmlrpcval("read", "string"));
                $msg_ser->addParam(new odooxmlrpcval($id, "array"));
                $msg_ser->addParam(new odooxmlrpcval($key1, "array"));
                $resp    = $client->send($msg_ser);
                if ($resp->faultCode()) {
                        $msg = $resp->faultString();
                        $invoice = $msg;
                }
                else{
                        $value_array = $resp->value()->scalarval();
                        $invoice = $value_array[0]->me['struct']['invoice_exists']->me['boolean'];
                }
                return $invoice;
        }
    
	
	public function _syncOrderInvoice ($orderId) {
		$draft_state = Mage::getStoreConfig('odoolink/odoolink2/draftstate');
		$auto_invoice = Mage::getStoreConfig('odoolink/odoolink2/auto_invoice');
		if($orderId && $draft_state!=1 && $auto_invoice==1){
			$This_order = Mage::getModel("sales/order")->load($orderId);
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$erp_ord_que = $write->query("SELECT * FROM ".$prefix."erp_mage_order_mapping WHERE mage_order_id='".$orderId."'");
			$erp_ord_fetch = $erp_ord_que->fetch();	
			if($erp_ord_fetch["erp_order_id"]>0){
				$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
				if ($userId != 'error'){
					$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
					$erpOrderId = $erp_ord_fetch["erp_order_id"];
					$erpCustomerId = $erp_ord_fetch["erp_cus_id"];
					$erpOrderName = $erp_ord_fetch["erp_order_name"];
					$status = $this->checkErpInvoiceStatus($erpOrderId, $userId, $client);
					if(!$status){
						Mage::getModel("odoolink/commonsocket")->doErpOrderInvoice($This_order, $erpOrderId, $erpCustomerId, $userId, $client);
						Mage::getSingleton('adminhtml/session')->addSuccess("Invoice has been Successfully Synced in ODOO !!");
					}elseif($status == true){
						Mage::getSingleton('adminhtml/session')->addNotice("Invoice of this order is already created in ODOO !!");
					}else{
						Mage::getSingleton('adminhtml/session')->addError("Invoice Could not be synced on ODOO, due to Following error ".$status);
					}
				}
				else{
					Mage::getSingleton('adminhtml/session')->addError("Invoice is not updated on ODOO due to error in ODOO Configuration!!! ");
				}
			}
			else{
				Mage::getSingleton('adminhtml/session')->addNotice("Invoice of this Order is not Synced in ODOO Because this Order is not Already Synced !!");
			}
		}
	}
	
	public function _syncSpecificOrder($mageOrderId, $userId, $client){
		$lineids="";
		$erp_shop_mod = false;
		$prefix=Mage::getConfig()->getTablePrefix();
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$result_currency_check = $write->query("SELECT erp_price_list_id FROM ".$prefix."erp_mage_currency_mapping WHERE mage_currency_id='".Mage::app()->getStore()->getBaseCurrencyCode()."'");
		$result_currency_fetch = $result_currency_check->fetch();
		if(!$result_currency_fetch)	{
			$pricelist_arr =  array(
				'code'=>new odooxmlrpcval(Mage::app()->getStore()->getBaseCurrencyCode(),"string")
			);
			$pricelist_create = new odooxmlrpcmsg('execute');
			$pricelist_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$pricelist_create->addParam(new odooxmlrpcval($userId, "int"));
			$pricelist_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$pricelist_create->addParam(new odooxmlrpcval("magento.orders", "string"));
			$pricelist_create->addParam(new odooxmlrpcval("create_pricelist", "string"));
			$pricelist_create->addParam(new odooxmlrpcval($pricelist_arr, "struct"));
			$pricelist_resp = $client->send($pricelist_create);
			$pricelist_id = $pricelist_resp->value()->me["int"];
			$write->query("INSERT INTO ".$prefix."erp_mage_currency_mapping VALUES('','".$pricelist_id."','".Mage::app()->getStore()->getBaseCurrencyCode()."')");
		}
		else	{
			$pricelist_id = $result_currency_fetch["erp_price_list_id"];
		}
		if(!$pricelist_id){
			return array(0,0,"ODOO pricelist id not found");
		}
		$This_order = Mage::getModel('sales/order')->load($mageOrderId);
		
		//it will return an array of (partner_id, partner_invoice_id, partner_shipping_id)
		$erpAddressArray = Mage::getModel("odoolink/observer")->getErpOrderAddresses($This_order, $userId, $client);
		if(count(array_filter($erpAddressArray)) == 3){
			$partner_id = $erpAddressArray[0];
			$partner_invoice_id = $erpAddressArray[1];
			$partner_shipping_id = $erpAddressArray[2];
			$magento_payment = $This_order->getPayment()->getMethodInstance()->getCode(); 
			$magento_shipping_description = urlencode($This_order->getShippingDescription());
			$magento_shipping_method = urlencode($This_order->getShippingMethod());
			
			$quoteOrderId = $This_order->getQuoteId();
			$quoteOrder = Mage::getModel('sales/quote')->load($quoteOrderId);
			$magento_shipping_method_data = urlencode($quoteOrder->getMyparcelData());
			
			$store_id = $This_order->getStoreId();
			$order_arr =  array(
						'partner_id'=>new odooxmlrpcval($partner_id,"int"),
						'partner_invoice_id'=>new odooxmlrpcval($partner_invoice_id,"int"),
						'partner_shipping_id'=>new odooxmlrpcval($partner_shipping_id,"int"),
						'pricelist_id'=>new odooxmlrpcval($pricelist_id,"int"),
						'date_order'=>new odooxmlrpcval($This_order->getCreatedAt(),"string"),
						'magento_order'=>new odooxmlrpcval($This_order->getIncrementId(),"string"),
						'magento_status'=>new odooxmlrpcval("export","string"),
						'client_order_ref'=>new odooxmlrpcval($This_order['onestepcheckout_customercomment'],"string"),
						'magento_payment'=>new odooxmlrpcval($magento_payment,"string"),
						'magento_shipping'=>new odooxmlrpcval($magento_shipping_method,"string"),
						'magento_shipping_description'=>new odooxmlrpcval($magento_shipping_description,"string"),
						'mag_store_id'=>new odooxmlrpcval($store_id,"int"),
						'shipping_method'=>new odooxmlrpcval($magento_shipping_method,"string"),
						'shipping_method_data'=>new odooxmlrpcval($magento_shipping_method_data,"string"),
					);			
					
			$order_create = new odooxmlrpcmsg('execute');
			$order_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$order_create->addParam(new odooxmlrpcval($userId, "int"));
			$order_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$order_create->addParam(new odooxmlrpcval("sale.order", "string"));
			$order_create->addParam(new odooxmlrpcval("create_magento", "string"));
			$order_create->addParam(new odooxmlrpcval($order_arr, "struct"));

			$cli_resp = $client->send($order_create);
			if($cli_resp->faultcode()){
				$error_message = 'Failed to Create order on ODOO, Error: , '.$cli_resp->faultString();
				return array(0,0, $error_message);
			}
			$order_id = $cli_resp->value()->me["int"];
			$price_includes_tax = Mage::getStoreConfig('tax/calculation/price_includes_tax');
			$shipping_includes_tax = Mage::getStoreConfig('tax/calculation/shipping_includes_tax');
			$items = $This_order->getAllItems();
			foreach($items as $itm)	{
				$Order_line_arr="";
				$item_desc = $itm->getName();
				if ($price_includes_tax) {
					$ItemBasePrice = $itm->getBasePriceInclTax();
				}else{
					$ItemBasePrice = $itm->getBasePrice();	
				}
				$ItemTaxPercent = $itm->getTaxPercent();
				$item_type = $itm->getProductType();
				if($item_type === 'configurable'){
					continue;
				}
				if($item_type === 'bundle'){
					$pro_id = $itm->getProductId();
					$p = Mage::getModel('catalog/product')->load($pro_id);
					$price_type = $p->getPriceType();
					if(!$price_type)
						$ItemBasePrice = 0;
				}
				if($itm->getParentItemId() != Null){
					$item_id = $itm->getParentItemId();
					$a = Mage::getModel('sales/order_item')->load($item_id);
					if($a->getProductType() === 'configurable'){
						if ($price_includes_tax) {
							$ItemBasePrice = $a->getBasePriceInclTax();
						}else{
							$ItemBasePrice = $a->getBasePrice();	
						}
						$ItemTaxPercent = $a->getTaxPercent();
					}
				}
			//getting erp product id
				$Erp_pro_id_qur = $write->query("SELECT erp_pro_id FROM ".$prefix."erp_mage_product_mapping WHERE mage_pro_id='".$itm->getProductId()."'");
				$Erp_pro_id_fetch = $Erp_pro_id_qur->fetch();
				if(!$Erp_pro_id_fetch){					
					$erp_product_id = Mage::getModel("odoolink/observer")->sync_Product($itm->getProductId(), $itm->getQtyOrdered(), $userId, $client);
				}
				else{
					$erp_product_id = $Erp_pro_id_fetch['erp_pro_id'];
				}
				$Order_line_arr =  array(
							'order_id'=>new odooxmlrpcval($order_id,"int"),
							'product_id'=>new odooxmlrpcval($erp_product_id,"int"),
							'price_unit'=>new odooxmlrpcval($ItemBasePrice,"string"),
							'product_uom_qty'=>new odooxmlrpcval($itm->getQtyOrdered(),"string"),
							'name'=>new odooxmlrpcval(urlencode($item_desc),"string")
						);
			/**************** checking tax applicable & getting mage tax id per item ************/
				if($ItemTaxPercent > 0){
					$tax_code = $write->query("SELECT * FROM ".$prefix."sales_order_tax WHERE order_id='".$mageOrderId."' AND percent= '".$ItemTaxPercent."'");
					$tax_code_result = $tax_code->fetch();
			/******************** getting erp tax id ******************/
					$tax_apply1 = $write->query("SELECT erp_tax_id FROM ".$prefix."erp_mage_tax_mapping WHERE mage_tax_code='".$tax_code_result["code"]."'");
					$tax_apply_result1 = $tax_apply1->fetch();
			/******************** getting mage rate_id	******************/
					$tax_apply_query = $write->query("SELECT tax_calculation_rate_id FROM ".$prefix."tax_calculation_rate WHERE code='".$tax_code_result["code"]."'");
					$tax_fetch = $tax_apply_query->fetch();
					$mage_tax_id = $tax_fetch["tax_calculation_rate_id"];
					
					if(!$tax_apply_result1)	{
						$tax_arr =  array(  
								'name'=>new odooxmlrpcval($tax_code_result["code"],"string"),
								'description'=>new odooxmlrpcval($tax_code_result["code"],"string"),
								'type'=>new odooxmlrpcval('percent',"string"),
								'amount'=>new odooxmlrpcval(($tax_code_result["percent"]/100),"string")
							);
						$erp_tax_id = Mage::getModel("odoolink/commonsocket")->Erp_Tax_create($tax_arr, $userId, $client);
						
						$taxmap=Mage::getModel('odoolink/tax');
						$taxmap->seterp_tax_id($erp_tax_id);
						$taxmap->setmage_tax_id($mage_tax_id);
						$taxmap->setmage_tax_code($tax_code_result["code"]);
						$taxmap->setneed_sync('no');
						$taxmap->save();
					}
					else{
						$erp_tax_id = $tax_apply_result1["erp_tax_id"];
					}
					$Order_line_arr['tax_id'] = new odooxmlrpcval($erp_tax_id,"int");
				}
				
				$line_create = new odooxmlrpcmsg('execute');
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$line_create->addParam(new odooxmlrpcval($userId, "int"));
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$line_create->addParam(new odooxmlrpcval("magento.orders", "string"));
				$line_create->addParam(new odooxmlrpcval("create_order_line", "string"));
				$line_create->addParam(new odooxmlrpcval($Order_line_arr, "struct"));
				$line_resp = $client->send($line_create);
				$line_val = $line_resp->value()->me;
				$lineids .= $line_val["int"].",";
			}
			/******************** For Voucher ******************/
			if ($This_order['discount_amount'] < 0){
				$code = $This_order->getCouponCode();
				if(!$code)
					$code = "Discount";
				$code = urlencode($code);
				$Order_line_arr =  array(
							'order_id'=>new odooxmlrpcval($order_id,"int"),
							'name'=>new odooxmlrpcval($code,"string"),
							'product_type'=>new odooxmlrpcval('voucher',"string"),
							'sku'=>new odooxmlrpcval($code,"string"),
							'price_unit'=>new odooxmlrpcval($This_order['discount_amount'],"double"),
							'coupon_code'=>new odooxmlrpcval($code,"string")
					);
				$line_create = new odooxmlrpcmsg('execute');
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$line_create->addParam(new odooxmlrpcval($userId, "int"));
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$line_create->addParam(new odooxmlrpcval("magento.orders", "string"));
				$line_create->addParam(new odooxmlrpcval("extra_order_line", "string"));
				$line_create->addParam(new odooxmlrpcval($Order_line_arr, "struct"));
				$line_resp = $client->send($line_create);
				$line_val = $line_resp->value()->me;
				$lineids .= $line_val["int"].",";
			}

			/******************** Gift Certificate ******************/
			if ($This_order['giftcert_amount'] <> 0){
				$code = $This_order['giftcert_code'];
				if(!$code)
					$code = "No Giftcertifcate Code Defined";
				$code = urlencode($code);
				$Order_line_arr =  array(
					'order_id'=>new odooxmlrpcval($order_id,"int"),
					'name'=>new odooxmlrpcval("Giftcertificates","string"),
					'product_type'=>new odooxmlrpcval('giftcert',"string"),
					'sku'=>new odooxmlrpcval("giftcert","string"),
					'price_unit'=>new odooxmlrpcval($This_order['giftcert_amount'],"double"),
					'giftcert_code'=>new odooxmlrpcval($code,"string")
				);
				$line_create = new odooxmlrpcmsg('execute');
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$line_create->addParam(new odooxmlrpcval($userId, "int"));
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$line_create->addParam(new odooxmlrpcval("magento.orders", "string"));
				$line_create->addParam(new odooxmlrpcval("extra_order_line", "string"));
				$line_create->addParam(new odooxmlrpcval($Order_line_arr, "struct"));
				$line_resp = $client->send($line_create);
				$line_val = $line_resp->value()->me;
				$lineids .= $line_val["int"].",";
			}

			/******************** Bundle Promotions Discount ******************/
			if ($This_order['bundle_discount_amount'] <> 0){
				$code = "Bundle Discount";
				if(!$code)
					$code = "No Giftcertifcate Code Defined";
				$code = urlencode($code);
				$Order_line_arr =  array(
					'order_id'=>new odooxmlrpcval($order_id,"int"),
					'name'=>new odooxmlrpcval("Bundle Discount","string"),
					'product_type'=>new odooxmlrpcval('bundle_discount',"string"),
					'sku'=>new odooxmlrpcval("bundle_discount","string"),
					'price_unit'=>new odooxmlrpcval($This_order['bundle_discount_amount'],"double"),
					'bundle_code'=>new odooxmlrpcval($code,"string")
				);
				$line_create = new odooxmlrpcmsg('execute');
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$line_create->addParam(new odooxmlrpcval($userId, "int"));
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$line_create->addParam(new odooxmlrpcval("magento.orders", "string"));
				$line_create->addParam(new odooxmlrpcval("extra_order_line", "string"));
				$line_create->addParam(new odooxmlrpcval($Order_line_arr, "struct"));
				$line_resp = $client->send($line_create);
				$line_val = $line_resp->value()->me;
				$lineids .= $line_val["int"].",";
			}

		/******************** For Shipping ******************/
			if ($This_order->getShippingDescription()){
				$shipping_description = urlencode($This_order->getShippingDescription());
				$magento_shipping_method = urlencode($This_order->getShippingMethod());
				$Order_line_arr =  array(
						'order_id'=>new odooxmlrpcval($order_id,"int"),
						'name'=>new odooxmlrpcval($shipping_description,"string"),
						'product_type'=>new odooxmlrpcval('shipping',"string"),
						'sku'=>new odooxmlrpcval($magento_shipping_method,"string"),
						'customer_id'=>new odooxmlrpcval(1,"int"),
						'shipping_description'=>new odooxmlrpcval($shipping_description,"string")
					);
				if ($shipping_includes_tax) {
					$Order_line_arr['price_unit'] = new odooxmlrpcval($This_order->getBaseShippingInclTax(),"double");
				}else{
					$Order_line_arr['price_unit'] = new odooxmlrpcval($This_order->getShippingAmount(),"double");
				}
				/* Removed Because the Taxes are handeled within ODOO
				 * if($This_order->getShippingTaxAmount()>0){
					$shippingTaxClass = Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_SHIPPING_TAX_CLASS);
					$erpTaxId = Mage::getModel("odoolink/observer")->getErpTaxIdByTaxClass($shippingTaxClass, $userId, $client);
					if($erpTaxId>0)
						$Order_line_arr['tax_id'] = new odooxmlrpcval($erpTaxId,"int");
				}*/
				$line_create = new odooxmlrpcmsg('execute');
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
				$line_create->addParam(new odooxmlrpcval($userId, "int"));
				$line_create->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
				$line_create->addParam(new odooxmlrpcval("magento.orders", "string"));
				$line_create->addParam(new odooxmlrpcval("extra_order_line", "string"));
				$line_create->addParam(new odooxmlrpcval($Order_line_arr, "struct"));
				$line_resp = $client->send($line_create);
				$line_val = $line_resp->value()->me;
				$lineids .= $line_val["int"].",";
			}
			$id_list1[0]= new odooxmlrpcval($order_id, 'int');
			$key = array(new odooxmlrpcval('name', 'string'));
			$msg = new odooxmlrpcmsg('execute');
			$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$msg->addParam(new odooxmlrpcval($userId, "int"));
			$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$msg->addParam(new odooxmlrpcval("sale.order", "string"));
			$msg->addParam(new odooxmlrpcval("read", "string"));
			$msg->addParam(new odooxmlrpcval($id_list1, "array"));
			$msg->addParam(new odooxmlrpcval($key, "array"));
			$resp = $client->send($msg);
			$val = $resp->value();
			$ids = $val->scalarval();		
			$id = $ids[0]->me;
			$order_name = $id["struct"]["name"]->me["string"];
			$ordermap=Mage::getModel('odoolink/order');
			$ordermap->seterp_order_id($order_id);
			$ordermap->setmage_order_id($mageOrderId);
			$ordermap->seterp_order_line_id(rtrim($lineids,","));
			$ordermap->seterp_cus_id($partner_id);
			$ordermap->seterp_order_name($order_name);
			$ordermap->save();			
			// ENTRY IN MAPPING AT ODOO END
			$Order_map_arr = array(
						'order_ref'=>new odooxmlrpcval($order_id,"int"),
						'oe_order_id'=>new odooxmlrpcval($order_id,"int"),
						'mag_orderIncrement_Id'=>new odooxmlrpcval($This_order->getIncrementId(),"string")
				);
			$order_map = new odooxmlrpcmsg('execute');
			$order_map->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$order_map->addParam(new odooxmlrpcval($userId, "int"));
			$order_map->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$order_map->addParam(new odooxmlrpcval("magento.orders", "string"));
			$order_map->addParam(new odooxmlrpcval("create", "string"));
			$order_map->addParam(new odooxmlrpcval($Order_map_arr, "struct"));
			$order_map_resp = $client->send($order_map);
			return array($order_id, $partner_id);
		}
		return array(0,0,"error occured during creating an ODOO customer.");
	}
}
?>
