<?php

class Erpopen_Odoolink_Model_Odoolink extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('odoolink/odoolink');
    }
	
	public function mappingerp($data){
		$customermap=Mage::getModel('odoolink/odoolink');
		$customermap->seterp_cus_id($data['erp_customer_id']);
		$customermap->setmage_cus_id($data['mage_customer_id']);
		$customermap->setmage_address_id($data['mage_address_id']);
		$customermap->setneed_sync('no');
		$customermap->save();
		return "";
	}
}