<?php
    class Erpopen_Odoolink_Block_Odoolink extends Mage_Core_Block_Template{
        public function _prepareLayout()    {
	       return parent::_prepareLayout();
        }    

        public function getOdoolink()    {
            if (!$this->hasData('odoolink')) {
                $this->setData('odoolink', Mage::registry('odoolink'));
            }
            return $this->getData('odoolink');
        }
    }
?>