<?php

class Erpopen_Odoolink_Block_Adminhtml_Payment_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('payment_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
      $Mage_payment = Mage::getSingleton('odoolink/payment')->getMagePaymentArray();
      $Erp_payment = Mage::getSingleton('odoolink/payment')->getErpPaymentArray();
      
	  $fieldset->addField('erp_payment_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('ODOO Payment'),
          'required'  => true,
          'name'      => 'erp_payment_id',
		  'class'	  =>'required-entry',
		  'values'    => $Erp_payment
	  ));
	  $fieldset->addField('mage_payment_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('Magento Payment'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'mage_payment_id',
		  'values'    => $Mage_payment
      ));

      
     
      if ( Mage::getSingleton('adminhtml/session')->getPaymentData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPaymentData());
          Mage::getSingleton('adminhtml/session')->setPaymentData(null);
      } elseif ( Mage::registry('odoolink_data') ) {
          $form->setValues(Mage::registry('odoolink_data')->getData());
      }
      return parent::_prepareForm();
  }
}