<?php
	class Erpopen_Odoolink_Block_Adminhtml_Odoolink extends Mage_Adminhtml_Block_Widget_Grid_Container	{
		public function __construct()  {
			$this->_controller = 'adminhtml_odoolink';
			$this->_blockGroup = 'odoolink';
			$this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Customer Data');
			$this->_addButtonLabel = Mage::helper('odoolink')->__('Add');
			$this->_addButton('sync_payment', array(
				'label'   => Mage::helper('catalog')->__('Update Customers On ODOO'),
				'onclick' => "setLocation('{$this->getUrl('*/*/updateCustomer')}')",
				'class'   => 'save'
			));
			parent::__construct();
			//$this->removeButton('add');
		}
	}
?>