<?php
class Erpopen_Odoolink_Block_Adminhtml_Currency extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_currency';
    $this->_blockGroup = 'odoolink';
	$this->_addButtonLabel = Mage::helper('odoolink')->__('Manual Currency Mapping');
    $this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Currency Data');
    parent::__construct();
    //$this->removeButton('add');
  }
}