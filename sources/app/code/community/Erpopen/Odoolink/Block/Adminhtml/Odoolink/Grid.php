<?php

class Erpopen_Odoolink_Block_Adminhtml_Odoolink_Grid extends Mage_Adminhtml_Block_Widget_Grid  {

  public function __construct()  {
      parent::__construct();
      $this->setId('odoolinkGrid');
      $this->setDefaultSort('entity_id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()  {
      $collection = Mage::getModel('odoolink/odoolink')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()  {
      $this->addColumn('entity_id', array(
          'header'    => Mage::helper('odoolink')->__('ID'),
          'align'     =>'center',
          'width'     => '200px',
          'index'     => 'entity_id',
      ));
      $this->addColumn('erp_cus_id', array(
          'header'    => Mage::helper('odoolink')->__('ERP Customer Id'),
          'align'     =>'center',
          'index'     => 'erp_cus_id',
      ));     
  	  $this->addColumn('mage_cus_id',array(
      		'header'	=>Mage::helper('odoolink')->__('Mage Customer Id'),
      		'align'		=>'center',
      		'index'		=>'mage_cus_id',
  	  ));
	  $this->addColumn('mage_address_id',array(
      		'header'	=>Mage::helper('odoolink')->__('Mage Address Id'),
      		'align'		=>'center',
      		'index'		=>'mage_address_id',
  	  ));
	  $this->addColumn('need_sync', array(
          'header'    => Mage::helper('odoolink')->__('Need Sync'),
          'align'     =>'center',
          'index'     => 'need_sync',
      ));
  		$this->addExportType('*/*/exportCsv', Mage::helper('odoolink')->__('CSV'));
  		$this->addExportType('*/*/exportXml', Mage::helper('odoolink')->__('XML'));
      return parent::_prepareColumns();
  }
 
  protected function _prepareMassaction()    {
        $this->setMassactionIdField('odoolink_id');
        $this->getMassactionBlock()->setFormFieldName('odoolink');
        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('odoolink')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('odoolink')->__('Are you sure?')
        ));
        $statuses = Mage::getSingleton('odoolink/status')->getOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('odoolink')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
              'visibility' => array(
                   'name' => 'status',
                   'type' => 'select',
                   'class' => 'required-entry',
                   'label' => Mage::helper('odoolink')->__('Status'),
                   'values' => $statuses
               ))
        ));
        return $this;
    }
   /*public function getRowUrl($row){
      return $this->getUrl('*//*/edit', array('id' => $row->getId()));
  }*/
}