<?php
class Erpopen_Odoolink_Block_Adminhtml_Tax extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_tax';
    $this->_blockGroup = 'odoolink';
    $this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Tax Data');
    $this->_addButtonLabel = Mage::helper('odoolink')->__('Manual Tax Mapping');
	$this->_addButton('sync_tax', array(
				'label'   => Mage::helper('catalog')->__('Update Taxes On ODOO'),
				'onclick' => "setLocation('{$this->getUrl('*/*/synctax/q/1')}')",
				'class'   => 'save'
			));
    parent::__construct();
   // $this->removeButton('add');
  }
}