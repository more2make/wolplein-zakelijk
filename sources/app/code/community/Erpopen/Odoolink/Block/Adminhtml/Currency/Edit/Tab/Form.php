<?php

class Erpopen_Odoolink_Block_Adminhtml_Currency_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('currency_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
		$Mage_currency = Mage::getSingleton('odoolink/currency')->getMageCurrencyArray();
		$Erp_currency = Mage::getSingleton('odoolink/currency')->getErpCurrencyArray();
		
		$fieldset->addField('erp_price_list_id', 'select', array(
		  'label'     => Mage::helper('odoolink')->__('ODOO Pricelist Id'),
		  'required'  => true,
		  'name'      => 'erp_price_list_id',
		  'class'	  =>'required-entry',
		  'values'    => $Erp_currency
		));
		$fieldset->addField('mage_currency_id', 'select', array(
			'label'     => Mage::helper('odoolink')->__('Magento Currency'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'mage_currency_id',	
			'values'    => $Mage_currency		  
		));



		if ( Mage::getSingleton('adminhtml/session')->getCurrencyData() )
		{
		  $form->setValues(Mage::getSingleton('adminhtml/session')->getCurrencyData());
		  Mage::getSingleton('adminhtml/session')->setCurrencyData(null);
		} elseif ( Mage::registry('odoolink_data') ) {
		  $form->setValues(Mage::registry('odoolink_data')->getData());
		}
		return parent::_prepareForm();
  }
}