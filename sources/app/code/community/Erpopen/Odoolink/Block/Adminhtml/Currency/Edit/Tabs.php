<?php

class Erpopen_Odoolink_Block_Adminhtml_Currency_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('odoolink_currency_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('odoolink')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('odoolink')->__('Item Information'),
          'title'     => Mage::helper('odoolink')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('odoolink/adminhtml_currency_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}