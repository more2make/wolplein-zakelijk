<?php
class Erpopen_Odoolink_Block_Adminhtml_Products extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_products';
    $this->_blockGroup = 'odoolink';
    $this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Product Data');
    $this->_addButtonLabel = Mage::helper('odoolink')->__('Manual Product Mapping');
	$this->_addButton('sync_product', array(
				'label'   => Mage::helper('catalog')->__('Update Products On ODOO'),
				'onclick' => "setLocation('{$this->getUrl('*/*/updateProduct')}')",
				'class'   => 'save'
			));
    parent::__construct();
    //$this->removeButton('add');
  }
}