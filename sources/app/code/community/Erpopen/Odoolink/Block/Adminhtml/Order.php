<?php
class Erpopen_Odoolink_Block_Adminhtml_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_order';
    $this->_blockGroup = 'odoolink';
    $this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Order Data');
    $this->_addButtonLabel = Mage::helper('odoolink')->__('Add Item');
    parent::__construct();
   // $this->removeButton('add');
  }
}