<?php
class Erpopen_Odoolink_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_category';
    $this->_blockGroup = 'odoolink';
    $this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Category Data');
    $this->_addButtonLabel = Mage::helper('odoolink')->__('Manual Category Mapping');
	$this->_addButton('sync_payment', array(
				'label'   => Mage::helper('catalog')->__('Update Categories On ODOO'),
				'onclick' => "setLocation('{$this->getUrl('*/*/updateCategory')}')",
				'class'   => 'save'
			));
    parent::__construct();
    //$this->removeButton('add');
  }
}