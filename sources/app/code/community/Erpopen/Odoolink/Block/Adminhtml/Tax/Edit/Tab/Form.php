<?php

class Erpopen_Odoolink_Block_Adminhtml_Tax_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('products_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
      $Mage_tax = Mage::getSingleton('odoolink/tax')->getMageTaxArray();
      $Erp_tax = Mage::getSingleton('odoolink/tax')->getErpTaxArray();
	
	  $fieldset->addField('erp_tax_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('ODOO TAX'),
          'class'     => 'required-entry',
		  'name'	  =>'erp_tax_id',
		  'values'    => $Erp_tax
      ));

      $fieldset->addField('mage_tax_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('Magento TAX'),
		  'class'     => 'required-entry',
		   'name'	=>'mage_tax_id',
		  'values'    => $Mage_tax
	  ));	 
	 
      if ( Mage::getSingleton('adminhtml/session')->getTaxData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getTaxData());
          Mage::getSingleton('adminhtml/session')->setTaxData(null);
      } elseif ( Mage::registry('odoolink_data') ) {
          $form->setValues(Mage::registry('odoolink_data')->getData());
      }
      return parent::_prepareForm();
  }
}