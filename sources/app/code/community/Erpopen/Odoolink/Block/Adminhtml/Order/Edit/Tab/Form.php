<?php

class Erpopen_Odoolink_Block_Adminhtml_Order_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('order_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
     
      $fieldset->addField('erp_order_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('ERP Order ID'),
          'class'     => 'validate-number',
          'required'  => true,
          'name'      => 'erp_order_id',
      ));

      $fieldset->addField('mage_order_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('Mage Order ID'),
		  'class'     => 'validate-number',
          'required'  => true,
          'name'      => 'mage_order_id',
	  ));
	   $fieldset->addField('erp_order_line_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('ERP Order Line ID'),
          'required'  => true,
          'name'      => 'erp_order_line_id',
	  ));
	   $fieldset->addField('erp_cus_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('ERP Customer ID'),
		  'class'     => 'validate-number',
          'required'  => true,
          'name'      => 'erp_cus_id',
	  ));
	   $fieldset->addField('erp_order_name', 'text', array(
          'label'     => Mage::helper('odoolink')->__('ERP Order Name'),
		  'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'erp_order_name',
	  ));		
     
      if ( Mage::getSingleton('adminhtml/session')->getPaymentData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPaymentData());
          Mage::getSingleton('adminhtml/session')->setPaymentData(null);
      } elseif ( Mage::registry('odoolink_data') ) {
          $form->setValues(Mage::registry('odoolink_data')->getData());
      }
      return parent::_prepareForm();
  }
}