<?php

class Erpopen_Odoolink_Block_Adminhtml_Order_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'odoolink';
        $this->_controller = 'adminhtml_order';
        
        $this->_updateButton('save', 'label', Mage::helper('odoolink')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('odoolink')->__('Delete Item'));
		
        /*$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);*/

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('payment_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'odoolink_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'odoolink_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('odoolink_data') && Mage::registry('odoolink_data')->getId() ) {
            return Mage::helper('odoolink')->__("Edit Item %s", $this->htmlEscape(Mage::registry('odoolink_data')->getTitle()));
        } else {
            return Mage::helper('odoolink')->__('Add Item');
        }
    }
}