<?php

class Erpopen_Odoolink_Block_Adminhtml_Products_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('products_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
      $Mage_pro = Mage::getSingleton('odoolink/products')->getMageProductArray();
      $Erp_pro = Mage::getSingleton('odoolink/products')->getErpProductArray();
      

		$fieldset->addField('mage_pro_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('Mage Products:'),
		  'class'     => 'required-entry',
		   'name'	=>'mage_pro_id',
		  'values'    => $Mage_pro
		));

		$fieldset->addField('erp_pro_id', 'select', array(
		  'label'     => Mage::helper('odoolink')->__('ODOO Products:'),
		  'class'     => 'required-entry',
		  'name'	  =>'erp_pro_id',
		  'values'    => $Erp_pro
		));		
		
     
      if ( Mage::getSingleton('adminhtml/session')->getPaymentData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPaymentData());
          Mage::getSingleton('adminhtml/session')->setPaymentData(null);
      } elseif ( Mage::registry('odoolink_data') ) {
          $form->setValues(Mage::registry('odoolink_data')->getData());
      }
      return parent::_prepareForm();
  }
}