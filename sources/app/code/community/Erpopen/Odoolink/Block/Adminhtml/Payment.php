<?php
class Erpopen_Odoolink_Block_Adminhtml_Payment extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_payment';
    $this->_blockGroup = 'odoolink';
    $this->_headerText = Mage::helper('odoolink')->__('Magento-ODOO Mapped Payment Data');
    $this->_addButtonLabel = Mage::helper('odoolink')->__('Manual Payment Mapping');
	$this->_addButton('sync_payment', array(
				'label'   => Mage::helper('catalog')->__('Synchronize All Payment Methods'),
				'onclick' => "setLocation('{$this->getUrl('*/*/syncPayment')}')",
				'class'   => 'add'
			));
    parent::__construct();
    //$this->removeButton('add');
  }
}