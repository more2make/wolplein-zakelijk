<?php
	class Erpopen_Odoolink_Block_Adminhtml_Tax_Rule extends Mage_Adminhtml_Block_Widget_Grid_Container{
		public function __construct()	{
		    $this->_controller      = 'tax_rule';
		    $this->_headerText      = Mage::helper('tax')->__('Manage Tax Rules');
		    $this->_addButtonLabel  = Mage::helper('tax')->__('Add New Tax Rule');
		    parent::__construct();			
			if(Mage::getSingleton('admin/session')->isAllowed('admin/erpopen_odoolink')){
				$this->_addButton('sync_tax', array(
					'label'   => Mage::helper('tax')->__('Synchronize All Taxes'),
					'onclick' => "setLocation('{$this->getUrl('odoolink/adminhtml_tax/synctax')}')"
				));
			}
		} 
	}
?>