<?php

class Erpopen_Odoolink_Block_Adminhtml_Odoolink_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('odoolink_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
     
      $fieldset->addField('erp_cus_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('ODOO Customer ID'),
          'class'     => 'validate-number',
          'required'  => true,
          'name'      => 'erp_cus_id',
      ));

      $fieldset->addField('mage_cus_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('Magento Customer Id'),
          'required'  => true,
          'name'      => 'mage_cus_id',
		  'class'     => 'validate-number',
	  ));
	  $fieldset->addField('mage_address_id', 'text', array(
          'label'     => Mage::helper('odoolink')->__('Magento Address Id'),
          'required'  => true,
          'name'      => 'mage_address_id',
		  'class'     => 'validate-number',
	  ));
	  $fieldset->addField('need_sync', 'select', array(
          'label'     => Mage::helper('odoolink')->__('Need Sync'),
          'class'     => 'required-entry',
          'name'      => 'need_sync',          
          'values' => array('no' => 'No','yes' => 'Yes')
      ));   
     
      if ( Mage::getSingleton('adminhtml/session')->getPaymentData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPaymentData());
          Mage::getSingleton('adminhtml/session')->setPaymentData(null);
      } elseif ( Mage::registry('odoolink_data') ) {
          $form->setValues(Mage::registry('odoolink_data')->getData());
      }
      return parent::_prepareForm();
  }
}