<?php
	class Erpopen_Odoolink_Block_Adminhtml_Catalog_Product extends Mage_Adminhtml_Block_Widget_Container	{
        public function __construct()	{
			parent::__construct();
			$this->setTemplate('catalog/product.phtml');
		}
		protected function _prepareLayout()    {
			$this->_addButton('add_new', array(
				'label'   => Mage::helper('catalog')->__('Add Product'),
				'onclick' => "setLocation('{$this->getUrl('*/*/new')}')",
				'class'   => 'add'
			));		
		if(Mage::getSingleton('admin/session')->isAllowed('admin/erpopen_odoolink')){
			$this->_addButton('sync_product', array(
				'label'   => Mage::helper('catalog')->__('Synchronize All Products'),
				'onclick' => "setLocation('{$this->getUrl('*/*/syncpro')}')",
				'class'   => 'add'
			));
			$this->_addButton('sync_category', array(
				'label'   => Mage::helper('catalog')->__('Synchronize All Categories'),
				'onclick' => "setLocation('{$this->getUrl('*/*/synccat')}')",
				'class'   => 'add'
			));
		}
			$this->setChild('grid', $this->getLayout()->createBlock('adminhtml/catalog_product_grid', 'product.grid'));
			return parent::_prepareLayout();
		}
		public function getAddNewButtonHtml()    {
			return $this->getChildHtml('add_new_button');
		}
		public function getGridHtml()    {
			return $this->getChildHtml('grid');
		}
		public function isSingleStoreMode()    {
        if (!Mage::app()->isSingleStoreMode()) {
               return false;
        }
        return true;
    }
}
