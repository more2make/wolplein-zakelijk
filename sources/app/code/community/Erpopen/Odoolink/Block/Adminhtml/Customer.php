<?php
	class Erpopen_Odoolink_Block_Adminhtml_Customer extends Mage_Adminhtml_Block_Widget_Grid_Container	{
		public function __construct()	{				
			$this->_controller = 'customer';
			$this->_headerText = Mage::helper('customer')->__('Manage Customers');
			$this->_addButtonLabel = Mage::helper('customer')->__('Add New Customer');
			parent::__construct();			 
			if(Mage::getSingleton('admin/session')->isAllowed('admin/erpopen_odoolink')){
				$this->_addButton('sync_customer', array(
					'label'   => Mage::helper('customer')->__('Synchronise All Customers'),
					'onclick' => "setLocation('{$this->getUrl('odoolink/adminhtml_odoolink/synccust')}')"
				));			
			}
		}
	}
?>