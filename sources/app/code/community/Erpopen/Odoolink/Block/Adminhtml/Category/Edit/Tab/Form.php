<?php

class Erpopen_Odoolink_Block_Adminhtml_Category_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('category_form', array('legend'=>Mage::helper('odoolink')->__('Item information')));
     
	$Mage_cat = Mage::getSingleton('odoolink/category')->getMageCategoryArray();
    $Erp_cat = Mage::getSingleton('odoolink/category')->getErpCategoryArray();

	 $fieldset->addField('mage_cat_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('Magento Category'),
		  'class'     => 'required-entry',
		   'name'	=>'mage_cat_id',
		  'values'    => $Mage_cat
	  ));
	  
	 $fieldset->addField('erp_cat_id', 'select', array(
          'label'     => Mage::helper('odoolink')->__('ODOO Category'),
          'class'     => 'required-entry',
		  'name'	  =>'erp_cat_id',
		  'values'    => $Erp_cat
      )); 
      if ( Mage::getSingleton('adminhtml/session')->getPaymentData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPaymentData());
          Mage::getSingleton('adminhtml/session')->setPaymentData(null);
      } elseif ( Mage::registry('odoolink_data') ) {
          $form->setValues(Mage::registry('odoolink_data')->getData());
      }
      return parent::_prepareForm();
  }
}