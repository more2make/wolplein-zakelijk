<?php

class Erpopen_Odoolink_Adminhtml_PaymentController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return true;
    }
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('odoolink/odoolink')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Payment Manager'), Mage::helper('adminhtml')->__('Payment Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}
	
	public function massDeleteAction() {
        $OdoolinkIds = $this->getRequest()->getParam('odoolink');
        if(!is_array($OdoolinkIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($OdoolinkIds as $OdoolinkId) {
                    $Odoolink = Mage::getModel('odoolink/payment')->load($OdoolinkId);
                    $Odoolink->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($OdoolinkIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
	public function syncPaymentAction() {
		$payments = Mage::getSingleton('payment/config')->getActiveMethods();		
       $methods = array();
	   $payment_array = array();
       foreach ($payments as $paymentCode=>$paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = array(
                'label'   => $paymentTitle,
                'value' => $paymentCode,
            );
			array_push($payment_array,$paymentTitle);
        }		
		$userId = -1;			
		if($userId<=0)
				$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId != "error"){
			$i=0;
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$pay_error = '';
			$pay_success = '';
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$result_check = $write->query("SELECT * FROM ".$prefix."erp_mage_payment_mapping");
			$checkList = $result_check->fetchAll();
			foreach($checkList as $label){
				$payment_array = array_diff($payment_array, array($label['mage_payment_id']));
			}
			if (count($payment_array)>0){
				foreach($payment_array as $method){
					$arrayVal = array(  'name'=>new odooxmlrpcval($method, "string"),
										'type'=>new odooxmlrpcval('cash', "string")
								);
					$erp_payment_id = Mage::getModel("odoolink/commonsocket")->Erp_Journal_create($arrayVal, $userId, $client);
					if ($erp_payment_id == 0){
						$pay_error.= $method.', ';
					}else{						
						$write->query("INSERT INTO ".$prefix."erp_mage_payment_mapping VALUES('','".$method."','$erp_payment_id')");
						$pay_success.= $method.', ';
					}
				}
			}
			else
				$this->_getSession()->addSuccess("All Payment methods has been already Synchronized in ODOO.");
		}
		else
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		if ($pay_error!='')
			$this->_getSession()->addNotice("Payment methods ".rtrim($pay_error,',')." already exist in ODOO!!!");
		if ($pay_success!='')
			$this->_getSession()->addSuccess("Payment methods ".rtrim($pay_success,',')." has been Synchronized sucessfully in ODOO.");
		
		$this->_redirect('*/*/index');
	}
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('odoolink/payment')->load($id);
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('odoolink_data', $model);

			
			
			$this->loadLayout();
			$this->_setActiveMenu('odoolink/items');

			
			
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('odoolink/adminhtml_payment_edit'))
				->_addLeft($this->getLayout()->createBlock('odoolink/adminhtml_payment_edit_tabs'));
				
				

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {		
		if ($data = $this->getRequest()->getPost()) {
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  			
			$model = Mage::getModel('odoolink/payment');
			$done = count($model->getCollection()->addFieldToFilter('mage_payment_id',$data['mage_payment_id']));
	  		if ($done > 0){
				$this->_getSession()->addError($data['mage_payment_id']." Payment Method is already mapped.");
				$this->_redirect('*/*/');
				return;
			}
			else{
				$model->setData($data)
					->setId($this->getRequest()->getParam('id'));
				
				try {
					if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
						$model->setCreatedTime(now())
							->setUpdateTime(now());
					} else {
						$model->setUpdateTime(now());
					}	
					
					$model->save();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__('Item was successfully saved'));
					Mage::getSingleton('adminhtml/session')->setFormData(false);

					if ($this->getRequest()->getParam('back')) {
						$this->_redirect('*/*/edit', array('id' => $model->getId()));
						return;
					}
					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	
	/**
     * Export category grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'Payment_Mapping.csv';       
		$content = $this->getLayout()->createBlock('odoolink/adminhtml_payment_grid')
                        ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName = 'Payment_Mapping.xml';
        $content = $this->getLayout()->createBlock('odoolink/adminhtml_payment_grid')
                        ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
?>