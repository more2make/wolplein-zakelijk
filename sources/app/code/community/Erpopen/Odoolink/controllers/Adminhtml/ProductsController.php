<?php

class Erpopen_Odoolink_Adminhtml_ProductsController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return true;
    }
	
	protected function _initAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: initAction()", null, "odoolink.log"); 
		$this->loadLayout()
			->_setActiveMenu('odoolink/odoolink')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Products Manager'), Mage::helper('adminhtml')->__('Products Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: indexAction()", null, "odoolink.log"); 
		$this->_initAction()->renderLayout();
	}
	
	public function massDeleteAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: massDeleteAction()", null, "odoolink.log"); 
        $OdoolinkIds = $this->getRequest()->getParam('odoolink');
        if(!is_array($OdoolinkIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($OdoolinkIds as $OdoolinkId) {
                    $Odoolink = Mage::getModel('odoolink/products')->load($OdoolinkId);
                    $Odoolink->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($OdoolinkIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
	public function massStatusAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: massStatusAction()", null, "odoolink.log"); 
		$Ids = $this->getRequest()->getParam('odoolink');
		if (!is_array($Ids)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
			foreach ($Ids as $Id) {
				$banner = Mage::getModel('odoolink/products')->load($Id)
				->setNeedSync($this->getRequest()->getParam('status'))
				->save();
			}
			$this->_getSession()->addSuccess(
			$this->__('Total of %d record(s) were successfully updated', count($Ids))
			);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function editAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: editAction()", null, "odoolink.log"); 
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('odoolink/products')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('odoolink_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('odoolink/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('odoolink/adminhtml_products_edit'))
				->_addLeft($this->getLayout()->createBlock('odoolink/adminhtml_products_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('products')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: newAction()", null, "odoolink.log"); 
		$this->_forward('edit');
	}
 
	public function saveAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: saveAction()", null, "odoolink.log"); 
		if ($data = $this->getRequest()->getPost()) {
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}	  			
			$model = Mage::getModel('odoolink/products');
			$done = count($model->getCollection()->addFieldToFilter('mage_pro_id',$data['mage_pro_id']));
			if ($done > 0){
				$this->_getSession()->addError("ODOO Product Id ".$data['mage_pro_id']." is already mapped.");
				$this->_redirect('*/*/');
				return;
			}else{
				$model->setData($data)
					->setId($this->getRequest()->getParam('id'));
					
				// adding map record to ODOO
				$status = $this->_mapOnErp($data['mage_pro_id'], $data['erp_pro_id']);
				if(!$status){
					$this->_getSession()->addError("Sorry, Product Id ".$data['erp_pro_id']." does not exist on ODOO.");
					$this->_redirect('*/*/');
					return;	
				}
				try {
					if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
						$model->setCreatedTime(now())
							->setUpdateTime(now());
					} else {
						$model->setUpdateTime(now());
					}
					
					$model->save();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__('Item was successfully saved'));
					Mage::getSingleton('adminhtml/session')->setFormData(false);

					if ($this->getRequest()->getParam('back')) {
						$this->_redirect('*/*/edit', array('id' => $model->getId()));
						return;
					}
					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('payment')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	
	protected function _mapOnErp($mage_product_id, $erp_product_id){
		Mage::log("controllers\Adminhtml\ProductsController.php: _mapOnErp()", null, "odoolink.log"); 
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
		if ($userId != "error"){
			$map_arr = array(	
								'pro_name'=>new odooxmlrpcval($erp_product_id,"int"),
								'oe_product_id'=>new odooxmlrpcval($erp_product_id,"int"),
								'mag_product_id'=>new odooxmlrpcval($mage_product_id,"int")
							);
			$map_data = new odooxmlrpcmsg('execute');
			$map_data->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$map_data->addParam(new odooxmlrpcval($userId, "int"));
			$map_data->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$map_data->addParam(new odooxmlrpcval("magento.product", "string"));
			$map_data->addParam(new odooxmlrpcval("create", "string"));
			$map_data->addParam(new odooxmlrpcval($map_arr, "struct"));
			$resp = $client->send($map_data);
			if ($resp->errno != 0){
				return false;
			}else{
				return true;
			}
		}
	}
	
	public function syncMassProductAction(){
		Mage::log("controllers\Adminhtml\ProductsController.php: syncMassProductAction()", null, "odoolink.log"); 
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		}
		else{
			$inventory = Mage::getStoreConfig('odoolink/Odoolink2/inventory');
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");			
			$productIds = $this->getRequest()->getParam('product');
			if (!is_array($productIds)) {
				$this->_getSession()->addError($this->__('Please select product(s).'));
			} else {
				$fail_ids = ''; $count = 0;
				if (!empty($productIds)) {
					try {
						foreach ($productIds as $productId) {
							$pro_id = 0; $image = false; $erp_categ_ids = array();
							$product = Mage::getModel('catalog/product')->load($productId);
							$categories = $product->getCategoryIds();
							if(count($categories) > 0){
								foreach($categories as $cat_id){
									$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='".$cat_id."'");
									$catg_one=$catg_check->fetch();
									if($catg_one["erp_cat_id"] > 0 ){
										$categ_id = $catg_one["erp_cat_id"];
										array_push($erp_categ_ids, new odooxmlrpcval($categ_id, 'int'));
									}
								}
							}else{
								$catg_check = $write->query("SELECT * FROM ".$prefix."erp_mage_category_mapping WHERE mage_cat_id='2'");
								$catg = $catg_check->fetch();
								array_push($erp_categ_ids, new odooxmlrpcval($catg["erp_cat_id"], 'int'));
							}
							$result_check=$write->query("SELECT * FROM ".$prefix."erp_mage_product_mapping WHERE mage_pro_id='".$product->getId()."'");
							$check_one = $result_check->fetch();
							try{
								$image = file_get_contents($product->getImageUrl());
							}catch (Exception $e){
								$image = false;
							}
							if($image) {
								$pro_image = base64_encode($image);
							}
							$ean = $product->getEan();
							if(!$check_one)	{
								Mage::log("Product does noet exist");
								$type = $product->getTypeID();
								$name = urlencode($product->getName());
								$sku = urlencode($product->getSku());
								$desc = urlencode(strip_tags($product->getDescription()));
								$srt_desc = urlencode(strip_tags($product->getShortDescription()));
								$pro_array = array(
											'name'=>new odooxmlrpcval($name, "string"),
											'description'=>new odooxmlrpcval($desc, "string"),
											'description_sale'=>new odooxmlrpcval($srt_desc, "string"),
											'list_price'=>new odooxmlrpcval($product->getPrice(), "string"),
											'type'=>new odooxmlrpcval($type, "string"),
											'categ_ids'=>new odooxmlrpcval($erp_categ_ids, "array"),
											'default_code'=>new odooxmlrpcval($sku, "string"),
											'weight_net'=>new odooxmlrpcval($product->getWeight(), "double"),
											'mage_id'=>new odooxmlrpcval($product->getId(), "int"),
											'method'=>new odooxmlrpcval('create', "string")
										);
								if($pro_image){
									$pro_array['image'] = new odooxmlrpcval($pro_image, "string");
								}
								if($ean){
									$check_ean = Mage::getModel("odoolink/commonsocket")->validate_ean13($ean);
									if($check_ean){
										$pro_array['ean13'] = new odooxmlrpcval($ean, "string");
									}
								}
								$pro_id = Mage::getModel("odoolink/commonsocket")->Erp_product_create($pro_array, $userId, $client);
								if($pro_id>0){
									$write->query("INSERT INTO ".$prefix."erp_mage_product_mapping VALUES('','".$pro_id."','".$product->getId()."','no')");
									if($inventory == 1){
										Mage::getModel("odoolink/commonsocket")->ErpCreateInventory($product, $pro_id, 0, $userId, $client);
									}
									$count ++;
								}
							}
							elseif($check_one["need_sync"] == 'yes'){
								Mage::log("Sync need=yes update ofzo?");
								$pro_id = $check_one["erp_pro_id"];
								$type = urlencode($product->getTypeID());
								$name = urlencode($product->getName());
								$sku = urlencode($product->getSku());
								$desc = urlencode(strip_tags($product->getDescription()));
								$srt_desc = urlencode(strip_tags($product->getShortDescription()));
								$pro_array = array(
											'name'=>new odooxmlrpcval($name, "string"),
											'description'=>new odooxmlrpcval($desc, "string"),
											'description_sale'=>new odooxmlrpcval($srt_desc, "string"),
											'list_price'=>new odooxmlrpcval($product->getPrice(), "string"),
											'type'=>new odooxmlrpcval($type, "string"),
											'categ_ids'=>new odooxmlrpcval($erp_categ_ids, "array"),
											'default_code'=>new odooxmlrpcval($sku, "string"),
											'weight_net'=>new odooxmlrpcval($product->getWeight(), "double"),
											'product_id'=>new odooxmlrpcval($pro_id, "int"),
											'method'=>new odooxmlrpcval('write', "string")
										);
								if($pro_image){
									$pro_array['image'] = new odooxmlrpcval($pro_image, "string");
								}
								if($ean){
									$check_ean = Mage::getModel("odoolink/commonsocket")->validate_ean13($ean);
									if($check_ean){
										$pro_array['ean13'] = new odooxmlrpcval($ean, "string");
									}
								}
								Mage::getModel("odoolink/commonsocket")->Erp_product_update($pro_array, $userId, $client);
								$write->query("UPDATE ".$prefix."erp_mage_product_mapping SET need_sync='no' WHERE erp_pro_id='$pro_id'");
								if($inventory == 1){
									Mage::getModel("odoolink/commonsocket")->ErpCreateInventory($product, $pro_id, 0, $userId, $client);
								}
								$count ++;
							}
							else{
								$fail_ids .= $productId.',';								
							}
						}
						if($count){
							$this->_getSession()->addSuccess(
								$this->__('Total of %d product(s) have been synchronized.', count($productIds))
							);
						}
						if($fail_ids){
							$this->_getSession()->addNotice("Product ids ".rtrim($fail_ids,',')." have been already synchronized!!!");
						}
					} catch (Exception $e) {
						$this->_getSession()->addError($e->getMessage());
					}
				}
			}
		}
		$this->_redirect('adminhtml/catalog_product');
	}
	
	public function updateProductAction(){
		Mage::log("controllers\Adminhtml\ProductsController.php: updateProductAction()", null, "odoolink.log", null, "odoolink.log");
		$signal = Mage::getModel("odoolink/commonsocket")->CreateProduct();
		if($signal==1)
				$this->_getSession()->addNotice("All Products has been Updated Successfully.");
		else
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		$this->_redirect("*/*/");	
	}
	
	/**
     * Export category grid to CSV format
     */
    public function exportCsvAction()
    {
		Mage::log("controllers\Adminhtml\ProductsController.php: exportCsvAction()", null, "odoolink.log", null, "odoolink.log"); 
        $fileName   = 'Product_Mapping.csv';       
		$content = $this->getLayout()->createBlock('odoolink/adminhtml_products_grid')
                        ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
		Mage::log("controllers\Adminhtml\ProductsController.php: exportXmlAction()", null, "odoolink.log", null, "odoolink.log");
        $fileName = 'Product_Mapping.xml';
        $content = $this->getLayout()->createBlock('odoolink/adminhtml_products_grid')
                        ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
		Mage::log("controllers\Adminhtml\ProductsController.php: _sendUploadResponse()", null, "odoolink.log", null, "odoolink.log");
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
?>