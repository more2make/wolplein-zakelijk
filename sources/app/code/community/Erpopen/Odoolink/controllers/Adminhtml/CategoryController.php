<?php

class Erpopen_Odoolink_Adminhtml_CategoryController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return true;
    }
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('odoolink/odoolink')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Category Manager'), Mage::helper('adminhtml')->__('Category Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		//echo "hello";
		$this->_initAction()->renderLayout();
	}

	 public function massDeleteAction() {
        $OdoolinkIds = $this->getRequest()->getParam('odoolink');
        if(!is_array($OdoolinkIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($OdoolinkIds as $OdoolinkId) {
                    $Odoolink = Mage::getModel('odoolink/category')->load($OdoolinkId);
                    $Odoolink->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($OdoolinkIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	public function massStatusAction() {
		$Ids = $this->getRequest()->getParam('odoolink');
		if (!is_array($Ids)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
			foreach ($Ids as $Id) {
				$banner = Mage::getModel('odoolink/category')->load($Id)
				->setNeedSync($this->getRequest()->getParam('status'))
				->save();
			}
			$this->_getSession()->addSuccess(
			$this->__('Total of %d record(s) were successfully updated', count($Ids))
			);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}	
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('odoolink/category')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('odoolink_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('odoolink/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('odoolink/adminhtml_category_edit'))
				->_addLeft($this->getLayout()->createBlock('odoolink/adminhtml_category_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Item does not exist'));
			$this->_redirect('*');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  			
			$model = Mage::getModel('odoolink/category');
			$done = count($model->getCollection()->addFieldToFilter('mage_cat_id',$data['mage_cat_id']));
			if ($done > 0){
				$this->_getSession()->addError("Mage Category Id ".$data['mage_cat_id']." is already mapped.");
				$this->_redirect('*/*/');
				return;
			}else{
				$model->setData($data)
					->setId($this->getRequest()->getParam('id'));
				
				try {
					if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
						$model->setCreatedTime(now())
							->setUpdateTime(now());
					} else {
						$model->setUpdateTime(now());
					}	
					
					$model->save();
					// adding map record to ODOO
					$this->_mapOnErp($data['mage_cat_id'],$data['erp_cat_id']);
					
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__('Item was successfully saved'));
					Mage::getSingleton('adminhtml/session')->setFormData(false);

					if ($this->getRequest()->getParam('back')) {
						$this->_redirect('*/*/edit', array('id' => $model->getId()));
						return;
					}
					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}					
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Unable to find item to save'));
		$this->_redirect('*/*/');
	}
	protected function _mapOnErp($mage_id, $erp_id){
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
		if ($userId != "error"){
			$catg_map_arr = array(
								'cat_name'=>new odooxmlrpcval($erp_id,"int"),
								'oe_category_id'=>new odooxmlrpcval($erp_id,"int"),
								'mag_category_id'=>new odooxmlrpcval($mage_id,"int")								
							);
			$catg_map = new odooxmlrpcmsg('execute');
			$catg_map->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$catg_map->addParam(new odooxmlrpcval($userId, "int"));
			$catg_map->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$catg_map->addParam(new odooxmlrpcval("magento.category", "string"));
			$catg_map->addParam(new odooxmlrpcval("create", "string"));
			$catg_map->addParam(new odooxmlrpcval($catg_map_arr, "struct"));
			$catg_map_resp = $client->send($catg_map);		
		}
	}
	
	/**
     * Export category grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'Category_Mapping.csv';       
		$content = $this->getLayout()->createBlock('odoolink/adminhtml_category_grid')
                        ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName = 'Category_Mapping.xml';
        $content = $this->getLayout()->createBlock('odoolink/adminhtml_category_grid')
                        ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
	
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	public function updateCategoryAction(){		
		$signal = Mage::getModel("odoolink/commonsocket")->CreateCategory();
		if($signal==1)
				$this->_getSession()->addNotice("All Categories has been Updated Successfully.");
		else
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		$this->_redirect("*/*/");
	
	}
}