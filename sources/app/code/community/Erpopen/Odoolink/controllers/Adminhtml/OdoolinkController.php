<?php

class Erpopen_Odoolink_Adminhtml_OdoolinkController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return true;
    }
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('odoolink/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	
	public function connectionAction(){
		$status = false;
		$error_message = '';
		$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
		$sock = new odooxmlrpc_client(Mage::getStoreConfig('odoolink/odoolink/url').":".Mage::getStoreConfig('odoolink/odoolink/port')."/xmlrpc/common");
		$msg = new odooxmlrpcmsg('login');
		$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
		$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/uname'), "string"));
		$msg->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
		$resp =  $sock->send($msg);
		if($resp->faultcode()){
			$error_message = $resp->faultString();
		}
		else{
			$val = $resp->value();
			$id = $val->scalarval();
			if($id>0)
				$status = true;				
		}
		if($status == true){
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__("Congratulation, It's sucessfully connected with ODOO through XML-RPC."));
			Mage::getModel('core/config')->saveConfig('odoolink/odoolink/status', "Successfully Connected");
		}
		else{
			if($error_message){			
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__("Error In ODOO Connection , due to ( ".$error_message." ), Please re-check the ODOO details!!!"));
			}else{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__("Error In ODOO Connection,  please check the login credentials!!!"));
			}
			Mage::getModel('core/config')->saveConfig('odoolink/odoolink/status', "Not Connected");
		}
		Mage::getConfig()->cleanCache();		
		$this->_redirect('adminhtml/system_config/index');
	}
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('odoolink/odoolink')->load($id);
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			
			Mage::register('odoolink_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('odoolink/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('odoolink/adminhtml_Odoolink_edit'))
				->_addLeft($this->getLayout()->createBlock('odoolink/adminhtml_Odoolink_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  			
			$model = Mage::getModel('odoolink/odoolink');
			$done = count($model->getCollection()->addFieldToFilter('erp_cus_id',$data['erp_cus_id']));
			if ($done > 0){
				$this->_getSession()->addError("ODOO Customer Id ".$data['erp_cus_id']." is already mapped.");
				$this->_redirect('*/*/');
				return;
			}else{
				$model->setData($data)
					->setId($this->getRequest()->getParam('id'));
				// adding map record to ODOO
				$status = $this->_mapOnErp($data['mage_cus_id'], $data['mage_address_id'], $data['erp_cus_id']);
				if(!$status){
					$this->_getSession()->addError("Sorry, Customer Id ".$data['erp_cus_id']." does not exist on ODOO.");
					$this->_redirect('*/*/');
					return;	
				}
				try {
					if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
						$model->setCreatedTime(now())
							->setUpdateTime(now());
					} else {
						$model->setUpdateTime(now());
					}	
					
					$model->save();					
					
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__('Item was successfully saved'));
					Mage::getSingleton('adminhtml/session')->setFormData(false);

					if ($this->getRequest()->getParam('back')) {
						$this->_redirect('*/*/edit', array('id' => $model->getId()));
						return;
					}
					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	
	protected function _mapOnErp($mage_customer_id, $mage_address_id, $erp_customer_id){
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
		if ($userId != "error"){
			$map_arr = array(	'cus_name'=>new odooxmlrpcval($erp_customer_id,"int"),
								'oe_customer_id'=>new odooxmlrpcval($erp_customer_id,"int"),
								'mag_customer_id'=>new odooxmlrpcval($mage_customer_id,"string"),
								'mag_address_id'=>new odooxmlrpcval($mage_address_id,"string")
							);
			$map_data = new odooxmlrpcmsg('execute');
			$map_data->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
			$map_data->addParam(new odooxmlrpcval($userId, "int"));
			$map_data->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
			$map_data->addParam(new odooxmlrpcval("magento.customers", "string"));
			$map_data->addParam(new odooxmlrpcval("create", "string"));
			$map_data->addParam(new odooxmlrpcval($map_arr, "struct"));
			$resp = $client->send($map_data);
			if ($resp->errno != 0){
				return false;
			}else{
				return true;
			}
		}
	}
	
	
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('odoolink/odoolink');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $OdoolinkIds = $this->getRequest()->getParam('odoolink');
        if(!is_array($OdoolinkIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($OdoolinkIds as $OdoolinkId) {
                    $Odoolink = Mage::getModel('odoolink/odoolink')->load($OdoolinkId);
                    $Odoolink->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($OdoolinkIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction() {
		$Ids = $this->getRequest()->getParam('odoolink');
		if (!is_array($Ids)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
			foreach ($Ids as $Id) {
				$banner = Mage::getModel('odoolink/odoolink')->load($Id)
				->setNeedSync($this->getRequest()->getParam('status'))
				->save();
			}
			$this->_getSession()->addSuccess(
			$this->__('Total of %d record(s) were successfully updated', count($Ids))
			);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
  
    public function exportCsvAction()
    {
        $fileName   = 'Customer_Mapping.csv';
        $content    = $this->getLayout()->createBlock('odoolink/adminhtml_Odoolink_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'Customer_Mapping.xml';
        $content    = $this->getLayout()->createBlock('odoolink/adminhtml_Odoolink_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
	
	public function synccustAction(){	
		$customer_collection = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*');
		$signal=0;
		$code = '';
		$model = Mage::getModel('odoolink/odoolink');
		$CommonSocket = Mage::getModel("odoolink/commonsocket");
		$client = $CommonSocket->getClientConnect();
		$userId = $CommonSocket->getSocketConnect();
		if ($userId == "error"){
			$signal = 2;
		}else{
			foreach ($customer_collection as $customer) {
				if(!$customer->getName())
					continue;
				$mappingcollection =  $model->getCollection();
				$mappingcollection->addFieldToFilter('mage_cus_id',array('eq'=>$customer->getId()));
				if(count($mappingcollection) > 0){								
					$update_data = $this->updateSingleCustomer($customer, $userId, $client);
					$code .= $update_data[0];
					$signal = $update_data[1];
				}
				else{
					$sync_data = $this->syncSingleCustomer($customer, $userId, $client);
					$code .= $sync_data[0];
					$signal .= $sync_data[0];
				}
			}
		}
		if ($code)
			$this->_getSession()->addSuccess("Customer Ids ".rtrim($code,',')." have been Synchronized Successfully in ODOO.");
		if($signal==0)
			$this->_getSession()->addSuccess("All Customers had been Synchronized Successfully on ODOO.");
		if($signal==2)
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		
		$this->_redirect("adminhtml/customer");	
	}

	public function syncSingleCustomer($customer, $userId, $client){
		$signal=0;		
		$code = '';
		$CommonSocket = Mage::getModel("odoolink/commonsocket");
		$company_id = 0;
		$parent_id = 0;
		$customer_arr =  array(
					'is_user'=>new odooxmlrpcval(true,"boolean"),
					'name'=>new odooxmlrpcval(urlencode($customer->getName()),"string"),
					'email'=>new odooxmlrpcval(urlencode($customer->getEmail()),"string"),
					'method'=>new odooxmlrpcval('create',"string"),
			        'store_id'=>new odooxmlrpcval($customer->getStoreId(),"string")
				);
		$company_id = $CommonSocket->Erp_customer_create($customer_arr, $customer->getId(), 'customer', $userId, $client);
		if($company_id>0){							
			foreach ($customer->getAddresses() as $address)	 {
				$bool = false;
				$type = $this->getAddressType($customer, $address);
				$name = urlencode($address->getName());
				$email = urlencode($customer->getEmail());
				$city = urlencode($address->getCity());
				$region = urlencode($address->getRegion());
				$company = urlencode($address->getCompany());
				$streets = $address->getStreet();
				if(count($streets)>1){
					$street = urlencode($streets[0]);
					$street2 = urlencode($streets[1]);
				}else{
					$street = urlencode($streets[0]);
					$street2 = urlencode('');
				}
				Mage::log ('Housenumber additions');
				$housenumber = urlencode($address->getHousenumber());
				Mage::log ('Housenumber: '.$housenumber);
				$housenumberadd = urlencode($address->getHousenumberadd());
				Mage::log ('HousenumberAdd: '.$housenumberadd);

				if (!($address->getVatId())) {
					Mage::log ('VAT ID'.urlencode($address->getVatId()));
					$vatid = urlencode($address->getVatId());
				}else{
					Mage::log ('Empty VAT ID');
					$vatid = urlencode('');
				}

				$customer_arr =  array(
							'magento_user_id'=>new odooxmlrpcval($company_id,"int"),
							'name'=>new odooxmlrpcval($name,"string"),
							'company'=>new odooxmlrpcval($company,"string"),
							'email'=>new odooxmlrpcval($email,"string"),
							'street'=>new odooxmlrpcval($street,"string"),
							'street2'=>new odooxmlrpcval($street2,"string"),
							'city'=>new odooxmlrpcval($city,"string"),
							'zip'=>new odooxmlrpcval($address->getPostcode(),"string"),
							'phone'=>new odooxmlrpcval($address->getTelephone(),"string"),
							'fax'=>new odooxmlrpcval($address->getFax(),"string"),
							'country_code'=>new odooxmlrpcval($address->getCountryId(),"string"),
							'region'=>new odooxmlrpcval($region,"string"),
							'customer'=>new odooxmlrpcval(true,"boolean"),
							'method'=>new odooxmlrpcval('create',"string"),
							'type'=>new odooxmlrpcval($type,"string"),
							'vat_id'=>new odooxmlrpcval($vatid,"string"),
							'housenumber'=>new odooxmlrpcval($housenumber,"string"),
							'housenumberadd'=>new odooxmlrpcval($housenumberadd,"string"),
                            'store_id'=>new odooxmlrpcval($customer->getStoreId(),"string")
						);
				$CommonSocket->Erp_customer_create($customer_arr, $customer->getId(), $address->getId(), $userId, $client);

			}
			$code .= $customer->getId().',';
		}
		$signal=1;
		return array($code, $signal);
	}

	public function updateSingleCustomer($customer, $userId, $client){		
		$signal=0;		
		$code = '';
		$CommonSocket = Mage::getModel("odoolink/commonsocket");
		$model = Mage::getModel('odoolink/odoolink');
		$mappingcollection = $model->getCollection()
								->addFieldToFilter('mage_cus_id',array('eq'=>$customer->getId()))
								->addFieldToFilter('mage_address_id',array('eq'=>'customer'))
								->addFieldToFilter('need_sync',array('eq'=>'yes'));
		if(count($mappingcollection) > 0){

			foreach($mappingcollection as $map){
				$erpcompanyId = $map->getErpCusId();
				$entityId = $map->getEntityId();					
			}
			$customer_arr =  array(
					'is_user'=>new odooxmlrpcval(true,"boolean"),
					'name'=>new odooxmlrpcval(urlencode($customer->getName()),"string"),
					'email'=>new odooxmlrpcval(urlencode($customer->getEmail()),"string"),
					'method'=>new odooxmlrpcval('write',"string"),
					'partner_id'=>new odooxmlrpcval($erpcompanyId,"int")
				);

			$CommonSocket->Erp_customer_update($customer_arr, $entityId, $userId, $client);
			$code .= $customer->getId().',';
			foreach ($customer->getAddresses() as $address)	 {
				$flag = false; $bool = false; $erp_cus_id = 0;
				$type = $this->getAddressType($customer, $address);

				$name = urlencode($address->getName());
				//$email = urlencode($customer->getEmail());
				$city = urlencode($address->getCity());
				$region = urlencode($address->getRegion());
				$company = urlencode($address->getCompany());

				$streets = $address->getStreet();
				if(count($streets)>1){
					$street = urlencode($streets[0]);
					$street2 = urlencode($streets[1]);
				}else{
					$street = urlencode($streets[0]);
					$street2 = urlencode('');
				}
				Mage::log ('Housenumber additions');
				$housenumber = urlencode($address->getHousenumber());
				Mage::log ('Housenumber: '.$housenumber);
				$housenumberadd = urlencode($address->getHousenumberadd());
				Mage::log ('HousenumberAdd: '.$housenumberadd);

				if (!($address->getVatId())) {
					Mage::log ('VAT ID'.urlencode($address->getVatId()));
					$vatid = urlencode($address->getVatId());
				}else{
					Mage::log ('Empty VAT ID');
					$vatid = urlencode('');
				}

				$addresscollection = $model->getCollection()
									->addFieldToFilter('mage_cus_id',array('eq'=>$customer->getId()))
									->addFieldToFilter('mage_address_id',array('eq'=>$address->getId()));
				foreach($addresscollection as $add){
					$mapId = $add->getEntityId();
					if($add->getNeedSync() == 'yes'){
						$flag = true;
						$erp_cus_id = $add->getErpCusId();
					}
				}
				if ($flag){
					$customer_arr =  array(
							'magento_user_id'=>new odooxmlrpcval($erpcompanyId,"int"),
							'name'=>new odooxmlrpcval($name,"string"),
							//'email'=>new odooxmlrpcval($email,"string"),
							'street'=>new odooxmlrpcval($street,"string"),
							'street2'=>new odooxmlrpcval($street2,"string"),
							'city'=>new odooxmlrpcval($city,"string"),
							'zip'=>new odooxmlrpcval($address->getPostcode(),"string"),
							'phone'=>new odooxmlrpcval($address->getTelephone(),"string"),
							'fax'=>new odooxmlrpcval($address->getFax(),"string"),
							'country_code'=>new odooxmlrpcval($address->getCountryId(),"string"),	
							'region'=>new odooxmlrpcval($region,"string"),
							'type'=>new odooxmlrpcval($type,"string"),
							'company'=>new odooxmlrpcval($company,"string"),
							'method'=>new odooxmlrpcval('write',"string"),
							'partner_id'=>new odooxmlrpcval($erp_cus_id,"int"),
							'vat_id'=>new odooxmlrpcval($vatid,"string"),
							'housenumber'=>new odooxmlrpcval($housenumber,"string"),
							'housenumberadd'=>new odooxmlrpcval($housenumberadd,"string")
						);
					$CommonSocket->Erp_customer_update($customer_arr, $mapId, $userId, $client);
					$signal = 1;					
				}
				elseif(count($addresscollection) == 0){
					$customer_arr =  array(
							'magento_user_id'=>new odooxmlrpcval($erpcompanyId,"int"),
							'name'=>new odooxmlrpcval($name,"string"),
							//'email'=>new odooxmlrpcval($email,"string"),
							'street'=>new odooxmlrpcval($street,"string"),
							'street2'=>new odooxmlrpcval($street2,"string"),
							'city'=>new odooxmlrpcval($city,"string"),
							'zip'=>new odooxmlrpcval($address->getPostcode(),"string"),
							'phone'=>new odooxmlrpcval($address->getTelephone(),"string"),
							'fax'=>new odooxmlrpcval($address->getFax(),"string"),
							'country_code'=>new odooxmlrpcval($address->getCountryId(),"string"),
							'region'=>new odooxmlrpcval($region,"string"),
							'type'=>new odooxmlrpcval($type,"string"),
							'tag'=>new odooxmlrpcval($company,"string"),
							'customer'=>new odooxmlrpcval(true,"boolean"),
							'company'=>new odooxmlrpcval($company,"string"),
							'use_parent_address'=>new odooxmlrpcval($bool,"boolean"),
							'method'=>new odooxmlrpcval('create',"string"),
							'vat_id'=>new odooxmlrpcval($vatid,"string"),
							'housenumber'=>new odooxmlrpcval($housenumber,"string"),
							'housenumberadd'=>new odooxmlrpcval($housenumberadd,"string")
					);
					$CommonSocket->Erp_customer_create($customer_arr, $customer->getId(), $address->getId(), $userId, $client);
				}
			}
		}		
		return array($code, $signal);
	}
	
	public function updateCustomerAction(){
		$model = Mage::getModel('odoolink/odoolink');
		$mappingcollection = $model->getCollection()
								->addFieldToFilter('mage_address_id',array('eq'=>'customer'))
								->addFieldToFilter('need_sync',array('eq'=>'yes'));
		$signal=0;		
		$code = '';
		$CommonSocket = Mage::getModel("odoolink/commonsocket");
		$client = $CommonSocket->getClientConnect();		
		$userId = $CommonSocket->getSocketConnect();
		if ($userId == "error"){
			$signal = 2;
		}else{			
			foreach($mappingcollection as $map){				
				$customer = Mage::getModel('customer/customer')->load($map->getMageCusId());	
				$update_data = $this->updateSingleCustomer($customer, $userId, $client);
				$code .= $update_data[0];
				$signal = $update_data[1];

			}
		}
		if ($code)
			$this->_getSession()->addNotice("Magento Customer Ids ".rtrim($code,',')." Successfully updated to ODOO");
		
		if($signal==0)
			$this->_getSession()->addSuccess("All Customers are already Updated on ODOO.");
		if($signal==2)
			$this->_getSession()->addError("Connection Error!! Invalid ODOO Configuration.");
		
		$this->_redirect("*/*/");
	
	}

	public function syncMassCustomerAction(){
		$signal=0;
		$code = '';$update_data = array();$sync_data = array();
		$model = Mage::getModel('odoolink/odoolink');
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		}
		else{
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");			
			$customerIds = $this->getRequest()->getParam('customer');
			if(empty($customerIds)) {
				$this->_getSession()->addError($this->__('Please select customer(s).'));
			}else{
				foreach($customerIds as $customerId) {		
					$customer = Mage::getModel('customer/customer')->load($customerId);					
					if(!$customer->getName())
						continue;	
					$mappingcollection =  $model->getCollection()->addFieldToFilter('mage_cus_id',array('eq'=>$customer->getId()));
					if(count($mappingcollection)>0){
						Mage::log ('Update Single Customer');
						$update_data = $this->updateSingleCustomer($customer, $userId, $client);
						$code .= $update_data[0];
						$signal = $update_data[1];
					}
					else{
						Mage::log ('Sync Single Customer');
						$sync_data = $this->syncSingleCustomer($customer, $userId, $client);
						$code .= $sync_data[0];
						$signal = $sync_data[0];
					}
				}
			}
		}
		if ($code)
			$this->_getSession()->addSuccess("Customer Ids ".rtrim($code,',')." have been Synchronized Successfully in ODOO.");
		if($signal==0)
			$this->_getSession()->addSuccess("All Customers had been Synchronized Successfully on ODOO.");
		$this->_redirect("adminhtml/customer");	
	}
	
	
	public function getAddressType($customer, $address){
		$type = '';
		if($customer->getDefaultShippingAddress() && $customer->getDefaultBillingAddress()){
			if ($customer->getDefaultShippingAddress()->getId() == $customer->getDefaultBillingAddress()->getId())
				$type = 'default';			
			elseif ($customer->getDefaultShippingAddress()->getId() == $address->getId())
				$type = 'delivery';
			elseif($customer->getDefaultBillingAddress()->getId() == $address->getId())
				$type = 'invoice';
			else
				$type = 'contact';
		}
		elseif($customer->getDefaultShippingAddress()){							
			if ($customer->getDefaultShippingAddress()->getId() == $address->getId())
				$type = 'delivery';
			else
				$type = 'contact';
		}
		elseif($customer->getDefaultBillingAddress()){
		
			if($customer->getDefaultBillingAddress()->getId() == $address->getId())
				$type = 'invoice';
			else
				$type = 'contact';
		}
		return $type;
	}
}