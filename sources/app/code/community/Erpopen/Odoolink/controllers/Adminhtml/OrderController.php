<?php

class Erpopen_Odoolink_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return true;
    }
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('odoolink/odoolink')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Order Manager'), Mage::helper('adminhtml')->__('Order Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}
	
	public function massDeleteAction() {
        $OdoolinkIds = $this->getRequest()->getParam('odoolink');
        if(!is_array($OdoolinkIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($OdoolinkIds as $OdoolinkId) {
                    $Odoolink = Mage::getModel('odoolink/order')->load($OdoolinkId);
                    $Odoolink->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($OdoolinkIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
	public function syncorderAction(){
		$magerporderIds = $this->getRequest()->getPost('order_ids',array());
		if(!is_array($magerporderIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        }
		else {
            try {
				$prefix=Mage::getConfig()->getTablePrefix();
				$order_model = Mage::getModel('odoolink/order');
				$write = Mage::getSingleton("core/resource")->getConnection("core_write");
				$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
				$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
				if ($userId != "error"){
					$count = 0;
					$fail_ids = '';
					$error_message = '';
					sort($magerporderIds);
 					foreach ($magerporderIds as $lastOrderId) {
						$carrier_id = 0;
						$OrderArray = array();
						$This_order = Mage::getModel('sales/order')->load($lastOrderId);						
						if($This_order->getStatus()=='canceled'){
							continue;
						}
						$ordercollection =  $order_model->getCollection()
												->addFieldToFilter('mage_order_id',array('eq'=>$lastOrderId));
						if (count($ordercollection)==0){
							$erpOrderDetails = Mage::getModel("odoolink/order")->_syncSpecificOrder($lastOrderId, $userId, $client);
							$erpOrderId = $erpOrderDetails[0];
							$partner_id = $erpOrderDetails[1];
							
/*** Checking for draftstate from setting if yes then order would not be Confirmed, shipped, invoiced   ***/
							$draft_state = Mage::getStoreConfig('odoolink/Odoolink2/draftstate');
							$auto_shipment = Mage::getStoreConfig('odoolink/Odoolink2/auto_shipment');
							$auto_invoice = Mage::getStoreConfig('odoolink/Odoolink2/auto_invoice');
							//if($erpOrderId>0 && $partner_id>0){
							//	if($draft_state!=1){
							//		$method = new odooxmlrpcmsg('exec_workflow');
							//		$method->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/dbname'), "string"));
							//		$method->addParam(new odooxmlrpcval($userId, "int"));
							//		$method->addParam(new odooxmlrpcval(Mage::getStoreConfig('odoolink/odoolink/pwd'), "string"));
							//		$method->addParam(new odooxmlrpcval("sale.order", "string"));
							//		$method->addParam(new odooxmlrpcval("order_confirm", "string"));
							//		$method->addParam(new odooxmlrpcval($erpOrderId, "int"));
							//		$method_resp = $client->send($method);
							//	// invoice and shipping sync codes...
							//		if($This_order->hasInvoices() && $auto_invoice==1){
							//			Mage::getModel("odoolink/commonsocket")->doErpOrderInvoice($This_order, $erpOrderId, $partner_id, $userId, $client);
							//		}
							//		if($This_order->hasShipments() && $auto_shipment == 1){
							//			Mage::getModel("odoolink/commonsocket")->doErpOrderShipment($This_order, $erpOrderId, $userId, $client);
							//		}
							//	}
							//	$count = $count+1;
							//}else{
							//	$error_message .= $erpOrderDetails[2].',';
							//	$fail_ids .= $lastOrderId.',';
							//}
						}else{
							$this->_getSession()->addError("Mage Order Id ".$lastOrderId." are already synced.");
						}
					}
					if($count){
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('adminhtml')->__(
								'Total of %d record(s) were successfully updated To ODOO', $count)
							);
					}
					if($fail_ids){
						$this->_getSession()->addError("Mage Order Ids ".rtrim($fail_ids,',')." could not be synced. Cause ".rtrim($error_message,','));
					}
				}else{
					$this->_getSession()->addError("Error in ODOO Configuration!!! ");
				}
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
		$this->_redirect('adminhtml/sales_order/index');
	}
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('odoolink/order')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('odoolink_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('odoolink/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('odoolink/adminhtml_order_edit'))
				->_addLeft($this->getLayout()->createBlock('odoolink/adminhtml_order_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Item does not exist'));
			$this->_redirect('*');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  			
			$model = Mage::getModel('odoolink/order');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}	
	
	/**
     * Export category grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'Order_Mapping.csv';       
		$content = $this->getLayout()->createBlock('odoolink/adminhtml_order_grid')
                        ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName = 'Order_Mapping.xml';
        $content = $this->getLayout()->createBlock('odoolink/adminhtml_order_grid')
                        ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
	
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
?>