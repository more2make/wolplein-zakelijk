<?php

class Erpopen_Odoolink_Adminhtml_TaxController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return true;
    }
	
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('odoolink/odoolink')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Payment Manager'), Mage::helper('adminhtml')->__('Payment Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}
	
	public function massDeleteAction() {
        $OdoolinkIds = $this->getRequest()->getParam('odoolink');
        if(!is_array($OdoolinkIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($OdoolinkIds as $OdoolinkId) {
                    $Odoolink = Mage::getModel('odoolink/tax')->load($OdoolinkId);
                    $Odoolink->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($OdoolinkIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
	public function massStatusAction() {
		$Ids = $this->getRequest()->getParam('odoolink');
		if (!is_array($Ids)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
			foreach ($Ids as $Id) {
				$banner = Mage::getModel('odoolink/tax')->load($Id)
				->setNeedSync($this->getRequest()->getParam('status'))
				->save();
			}
			$this->_getSession()->addSuccess(
			$this->__('Total of %d record(s) were successfully updated', count($Ids))
			);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('odoolink/tax')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('odoolink_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('odoolink/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('odoolink/adminhtml_tax_edit'))
				->_addLeft($this->getLayout()->createBlock('odoolink/adminhtml_tax_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Item does not exist'));
			$this->_redirect('*');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
	
	public function saveAction() {
		$prefix=Mage::getConfig()->getTablePrefix();
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		if ($data = $this->getRequest()->getPost()) {			
			$result = $write->query("SELECT * FROM ".$prefix."tax_calculation_rate WHERE tax_calculation_rate_id = '".$data['mage_tax_id']."'");
			$tax = $result->fetch();
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS ;
					$uploader->save($path, $_FILES['filename']['name'] );
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
			
			$model = Mage::getModel('odoolink/tax');
			$done = count($model->getCollection()->addFieldToFilter('mage_tax_id',array('eq'=>$data['mage_tax_id'])));		
	  		if ($done > 0){
				$this->_getSession()->addError($tax['code']." Tax is already mapped.");
				$this->_redirect('*/*/');
				return;
			}else{
				$model->setData($data)->setId($this->getRequest()->getParam('id'));				
				try {
					if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
						$model->setCreatedTime(now())
							->setUpdateTime(now());
					} else {
						$model->setUpdateTime(now());
					}					
					$id = $model->save();
					// own query added to update tax code..					
					$write->query("UPDATE ".$prefix."erp_mage_tax_mapping SET mage_tax_code='".$tax['code']."' WHERE entity_id='".$id['entity_id']."'");
					
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('odoolink')->__('Item was successfully saved'));
					Mage::getSingleton('adminhtml/session')->setFormData(false);

					if ($this->getRequest()->getParam('back')) {
						$this->_redirect('*/*/edit', array('id' => $model->getId()));
						return;
					}
					$this->_redirect('*/*/');
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::getSingleton('adminhtml/session')->setFormData($data);
					$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
					return;
				}
			}
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('odoolink')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
	public function synctaxAction() {	
		$map = '';
		$userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
		if ($userId == "error"){
			$this->_getSession()->addError("Error in ODOO Configuration!!! ");
		}			
		else{
			$prefix=Mage::getConfig()->getTablePrefix();
			$write = Mage::getSingleton("core/resource")->getConnection("core_write");
			$result = $write->query("SELECT * FROM ".$prefix."tax_calculation_rate");
			$code = '';
			$code1 = '';
			$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
			foreach($result->fetchAll() as $one_row)	{					
				$mage_tax_id = $one_row['tax_calculation_rate_id'];
				$mage_tax_code = $one_row["code"];
				$rate = $one_row["rate"]/100;
				$result_check = $write->query("SELECT * FROM ".$prefix."erp_mage_tax_mapping WHERE mage_tax_id='$mage_tax_id'");
				$check_one=$result_check->fetch();
				if (!$check_one){
					$tax_array =  array('name'=>new odooxmlrpcval($one_row["code"],"string"),
										'description'=>new odooxmlrpcval($one_row["code"],"string"),
										'type'=>new odooxmlrpcval('percent',"string"),
										'amount'=>new odooxmlrpcval($rate,"string")                                       
								);
					$erp_tax_id = Mage::getModel("odoolink/commonsocket")->Erp_Tax_create($tax_array, $userId, $client);
					if($erp_tax_id>0){
						$write->query("INSERT INTO ".$prefix."erp_mage_tax_mapping VALUES('','$erp_tax_id','$mage_tax_id','$mage_tax_code','no')");
						$code .= $mage_tax_code.',';
					}
					else
						$map .= $mage_tax_code.',';
				}
				elseif($check_one["need_sync"] == 'yes'){
					$tax_array =  array('name'=>new odooxmlrpcval($one_row["code"],"string"),
										'description'=>new odooxmlrpcval($one_row["code"],"string"),
										'type'=>new odooxmlrpcval('percent',"string"),
										'amount'=>new odooxmlrpcval($rate,"string")
								);
					$erp_tax_id = $check_one["erp_tax_id"];
					Mage::getModel("odoolink/commonsocket")->Erp_Tax_update($tax_array, $erp_tax_id, $userId, $client);
					$write->query("UPDATE ".$prefix."erp_mage_tax_mapping SET need_sync='no', mage_tax_code='$mage_tax_code' WHERE erp_tax_id='$erp_tax_id'");
					$code1 .= $mage_tax_code.',';	 
				}
			}
			if ($map)
				$this->_getSession()->addError("Tax codes ".rtrim($map,',')." already exist on ODOO, Please map it manually.");
			if ($code)
				$this->_getSession()->addSuccess("Tax codes ".rtrim($code,',')." have been Synchronized Successfully.");
			if ($code1)
				$this->_getSession()->addSuccess("Tax code ".rtrim($code1,',')." Successfully Updated on ODOO!!!");
			if (!$code && !$code1 && !$map)
				$this->_getSession()->addNotice("All Taxes are already Synchronized on ODOO!!!");
		}
		
		$temp=$this->getRequest()->getParam('q');
		if($temp==1){
			$this->_redirect("*/*/");
		}else{
			$this->_redirect("adminhtml/tax_rule");
		}
	}
	
	/**
     * Export category grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'Tax_Mapping.csv';       
		$content = $this->getLayout()->createBlock('odoolink/adminhtml_tax_grid')
                        ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }
	
	public function exportXmlAction() {
        $fileName = 'Tax_Mapping.xml';
        $content = $this->getLayout()->createBlock('odoolink/adminhtml_tax_grid')
                        ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }
	
    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
?>