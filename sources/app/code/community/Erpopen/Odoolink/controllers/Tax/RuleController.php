<?php
    include_once("Mage/Adminhtml/Controllers/Tax/RuleController.php");
    class Erpopen_Odoolink_Tax_RuleController extends Mage_Adminhtml_Controller_Action	{
        public function synctaxAction() {
            if($userId<=0)
                $userId = Mage::getModel("odoolink/commonsocket")->getSocketConnect();
			if ($userId == "error"){
				$this->_getSession()->addError("Error in ODOO Configuration!!! ");
			}			
			else{
				$prefix=Mage::getConfig()->getTablePrefix();
				$write = Mage::getSingleton("core/resource")->getConnection("core_write");
				$result = $write->query("SELECT * FROM ".$prefix."tax_calculation_rate");
				$code = '';
				$code1 = '';
				$client = Mage::getModel("odoolink/commonsocket")->getClientConnect();
				foreach($result->fetchAll() as $one_row)	{					
					$mage_tax_id = $one_row['tax_calculation_rate_id'];
					$mage_tax_code = $one_row["code"];
					$rate = $one_row["rate"]/100;
					$result_check = $write->query("SELECT * FROM ".$prefix."erp_mage_tax_mapping WHERE mage_tax_id='$mage_tax_id'");
					$check_one=$result_check->fetch();
					if (!$check_one){
						$tax_array =  array('name'=>new odooxmlrpcval($one_row["code"],"string"),
											'description'=>new odooxmlrpcval($one_row["code"],"string"),
											'type'=>new odooxmlrpcval('percent',"string"),
											'amount'=>new odooxmlrpcval($rate,"string")                                       
									);
						$erp_tax_id = Mage::getModel("odoolink/commonsocket")->Erp_Tax_create($tax_array, $userId, $client);
						$write->query("INSERT INTO ".$prefix."erp_mage_tax_mapping VALUES('','$erp_tax_id','$mage_tax_id','$mage_tax_code','no')");
						$code .= $mage_tax_code.',';
					}
					elseif($check_one["need_sync"] == 'yes'){
						$tax_array =  array('name'=>new odooxmlrpcval($one_row["code"],"string"),
											'description'=>new odooxmlrpcval($one_row["code"],"string"),
											'type'=>new odooxmlrpcval('percent',"string"),
											'amount'=>new odooxmlrpcval($rate,"string")                                       
									);
						$erp_tax_id = $check_one["erp_tax_id"];
						Mage::getModel("odoolink/commonsocket")->Erp_Tax_update($tax_array, $erp_tax_id, $userId, $client);
						$write->query("UPDATE ".$prefix."erp_mage_tax_mapping SET need_sync='no', mage_tax_code='$mage_tax_code' WHERE erp_tax_id='$erp_tax_id'");
						$code1 .= $mage_tax_code.',';	 
					}
				}
				if ($code)
					$this->_getSession()->addSuccess("Tax codes ".rtrim($code,',')." have been Synchronized Successfully.");
				if ($code1)
					$this->_getSession()->addSuccess("Tax code ".rtrim($code1,',')." Successfully Updated on ODOO!!!");
				if (!$code && !$code1)
					$this->_getSession()->addNotice("All Taxes are already Synchronized on ODOO!!!");
			}
			$this->_redirect("*/*/");
        }		
		
    }
?>