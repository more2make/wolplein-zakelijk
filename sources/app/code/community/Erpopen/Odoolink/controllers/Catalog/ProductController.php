<?php
	include_once("Mage/Adminhtml/controllers/Catalog/ProductController.php");
	class Erpopen_Odoolink_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController	{
		public function synccatAction()	{
			$signal = Mage::getModel("odoolink/commonsocket")->CreateCategory();
			if($signal==1)
				$this->_getSession()->addSuccess("All Categories have been Syncronised Successfully");
			else
				$this->_getSession()->addError("Error in ODOO Configuration!!! ");
			$this->_redirect("*/*/");		
		}	
		public function syncproAction()	{
			$signal = Mage::getModel("odoolink/commonsocket")->CreateProduct();
			if($signal==1)
				$this->_getSession()->addSuccess("All Products have been Syncronised Successfully");
			else
				$this->_getSession()->addError("Error in ODOO Configuration!!! ");
			$this->_redirect("*/*/");
		}
	}
?>