var flightPath;
	var directionDisplay;
	var directionsService = new google.maps.DirectionsService();
	var map
  
function initMap() {
  usRoadMapType = new google.maps.StyledMapType([
      {
        stylers: [
        { hue: "#fff" },
        { saturation: -100},
        { lightness: 35 }
      ]
    },{
  featureType: 'poi',
  elementType: 'geometry',
  stylers: [
    { visibility: 'on' }
  ]
},{
      featureType: "road",
      elementType: "geometry",
      stylers: [
		{ color: '#ffb8d9' },
        { visibility: "simplified" }
      ]
    },{
      featureType: "poi",
      elementType: "labels.text",
      stylers: [
        { visibility: "off" }
      ]
	},{
      featureType: "poi",
      elementType: "labels",
      stylers: [
        { visibility: "off" }
      ]
    },{
      featureType: "road",
      elementType: "labels",
      stylers: [
        { visibility: "on" }
        ]
      }
    ]);

var latlng = new google.maps.LatLng(51.800630, 5.271507);
  directionsDisplay = new google.maps.DirectionsRenderer();
  
var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeControlOptions: {
	  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'usroadatlas']}
    };

var lineSymbol = {
    path: google.maps.SymbolPath.CIRCLE,
    strokeOpacity: 1,
    fillOpacity: 1,
	strokeColor: '#fe7628',
	fillColor: '#fe7628',
    scale: 2
};

var looproute = [
	{lat: 51.807793, lng: 5.262939},
	{lat: 51.807718, lng: 5.262944},
	{lat: 51.807628, lng: 5.262889},
	{lat: 51.807580, lng: 5.262843},
	{lat: 51.807525, lng: 5.262776},
	{lat: 51.807397, lng: 5.262806},
	{lat: 51.807490, lng: 5.264200},
	{lat: 51.807389, lng: 5.264860},
	{lat: 51.806290, lng: 5.267839},
	{lat: 51.806234, lng: 5.267804},
	{lat: 51.806199, lng: 5.267813},
	{lat: 51.806175, lng: 5.267835},
	{lat: 51.806143, lng: 5.267881},
    {lat: 51.803639, lng: 5.265563},
	{lat: 51.803234, lng: 5.265391},
	{lat: 51.802932, lng: 5.265379},
	{lat: 51.802952, lng: 5.265631},
	{lat: 51.802889, lng: 5.266608},
	{lat: 51.802181, lng: 5.268040},
	{lat: 51.801863, lng: 5.269032},
    {lat: 51.801146, lng: 5.268712},
    {lat: 51.800630, lng: 5.271507}
];

flightPath = new google.maps.Polyline({
    path: looproute,
    geodesic: true,
    strokeOpacity: 0,
    icons: [{
      icon: lineSymbol,
      offset: '0',
      repeat: '15px'
    }],
 });

var image = 'http://www.wolplein.nl/media/sliders/wolplein.png';
    markerCenter = new google.maps.Marker({
    icon: image,
	draggable: false,
  });


map = new google.maps.Map(document.getElementById("map"),myOptions);
    directionsDisplay.setMap(map);
	directionsDisplay.setOptions( { suppressMarkers: true } );
	map.mapTypes.set('usroadatlas', usRoadMapType);
	map.setMapTypeId('usroadatlas');
	
var marker = new google.maps.Marker({
      position: latlng, 
      map: map, 
      title:'Valeton 9c',
	  icon: image
    }); 
}

function calcRoute() {
    var start = document.getElementById("routeStart").value;
    var end = "51.800630, 5.271507";
    var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
	
flightPath.setMap(null);

directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
		directionsDisplay.setMap(map);
        directionsDisplay.setDirections(response);
      }
    });
document.getElementById("toon1").style.display = "block";
  }

function addCircle() {
  flightPath.setMap(map);
  document.getElementById("toon").style.visibility = "hidden";
  document.getElementById("verberg").style.visibility = "visible";
  document.getElementById("toon1").style.display = "none";
  document.getElementById("verberg1").style.display = "none";
  document.getElementById("directionsPanel").style.display = "none";
	map.setCenter({
        lat : 51.804444,
        lng : 5.266341
});
	map.setZoom(15);
	directionsDisplay.setMap(null);
	directionsDisplay.setPanel(null);
}


function removeCircle() {
  flightPath.setMap(null);
  document.getElementById("verberg").style.visibility = "hidden";
  document.getElementById("toon").style.visibility = "visible";
  document.getElementById("toon1").style.display = "none";
  document.getElementById("verberg1").style.display = "none";
  document.getElementById("directionsPanel").style.display = "none";
	map.setCenter({
        lat : 51.800630,
        lng : 5.271507
});
directionsDisplay.setMap(null);
directionsDisplay.setPanel(null);
}

function addNavigatie() {
  document.getElementById("directionsPanel").style.display = "inline-block";
  directionsDisplay.setPanel(document.getElementById("directionsPanel"));
  document.getElementById("toon1").style.display = "none";
  document.getElementById("verberg1").style.display = "inline-block";
}
function removeNavigatie() {
	document.getElementById("directionsPanel").style.display = "none";
  document.getElementById("verberg1").style.display = "none";
  document.getElementById("toon1").style.display = "inline-block";
}

