var $j = jQuery.noConflict();

// Make footerblocks the same height

equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $j(container).each(function() {

        $el = $j(this);
        $j($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$j(window).load(function() {
    equalheight('.footer-container .footer-block');
});


$j(window).resize(function(){
    equalheight('.footer-container .footer-block');
});

jQuery(function($) {
    
    $(document).on('click', '.execute-swatch', function(e) {
	
        var $this = $(this),
            sid = $this.data('color'),
            target = $this.data('target'),
            $targetSelect = $('#'+target);
            
        //triger change normally
        $targetSelect.val(sid).trigger('change');
        
        //trigger change via js since normal change event does not seem to trigger all of magento events
        var element = document.getElementById(target),
        evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		element.dispatchEvent(evt);
		
		if($( "div" ).hasClass( "select" ))
		{
		$( "div" ).removeClass( "select" );
		}
		
$this.addClass( "select" );
		
    });
});

jQuery(function($) {
    
    $(document).on('change', '.product-options select', function(e) {
        //check if their is a promo-form, if not no need to proceed
        var $promoCartButtons = $('.promo-addtocart');
        
        if ($promoCartButtons.length > 0) {
            //check if all productoptions are filled
            var $productOptions = $('.product-options select'),
                allOptionsSet = true;
            
            $productOptions.each(function () {
                if ($(this).val() == '') {
                    allOptionsSet = false;
                    return;//return is the break of jquery
                }
            });
            
            //iterate through the buttons and set them in the correct state
            $promoCartButtons.each(function() {
                var $this = $(this);
                
                if (allOptionsSet) {
                    //all options are set, shown cart option and hide configure button
                     $this.removeClass('no-display');
                     $this.siblings('.promo-config').addClass('no-display');
                     
                     //update price display using Magento product config (with overrides from SCP module)
                     var childProductId = spConfig.getMatchingSimpleProduct();
                     var childProducts = spConfig.config.childProducts;
                     
                     if(childProductId) {
                        //child product found, update price
                        var $promoDiscount = $this.closest('.bundlediscount-lists'),
                            $discountContainer = $this.closest('.bundle-discount-container'),
                            discountAmount = parseFloat($promoDiscount.data('discount-amount')),
                            discountIsPercentage = parseFloat($promoDiscount.data('is-percentage')),
                            $priceContainer = $('.price-container', $discountContainer),
                            $oldPriceContainer = $('.old-price', $priceContainer),
                            $newPriceContainer = $('.new-price', $priceContainer),
                            totalPrice = parseFloat(childProducts[childProductId]["finalPrice"]),
                            finalPrice = 0;
                        
                        $('.bundlediscount-selections .bundlediscount-selections-img', $discountContainer).each(function() {
                            //first element is main product, already added
                            if ($(this).index() !== 0) {
                                totalPrice += parseFloat($(this).data('price'));
                            }
                        });
                        
                        //remove discount from totalprice to make finalPrice
                        if (discountIsPercentage == 1) {
                            finalPrice = totalPrice - ((totalPrice / 100) * discountAmount);
                        } else {
                            finalPrice = totalPrice - discountAmount;
                        }
                        
                        //set new total and final price
                        $oldPriceContainer.html(spConfig.formatPrice(totalPrice, false));
                        $newPriceContainer.html(spConfig.formatPrice(finalPrice, false));
                        $priceContainer.removeClass('no-display');
                     }
                } else {
                    //in case cart button is shown, hide it and replace with configure
                    $this.addClass('no-display');
                    $this.siblings('.promo-config').removeClass('no-display');
                }
            });
        }
        
    });
    
    $(document).on('click', '.promo-addtocart', function(e) {
        //build form and submit it
        var $this = $(this),
            target = $this.data('target'),
            productId = $this.data('product-id'),
            $productOptions = $('.product-options select');
            
        var $newForm = jQuery('<form>', {
            'action': target,
            'method': 'post'
        })    
        
        $productOptions.each(function () {
            var $option = $(this),
                name = $option.attr('name');
            
            //the target expects the productId to be used in the super_attribute name
            name = name.replace('super_attribute', 'super_attribute['+productId+']');
            
            $newForm.append(jQuery('<input>', {
                'name': name,
                'value': $option.val(),
                'type': 'hidden'
            }));
        });
        
        $newForm.submit();
    });
});