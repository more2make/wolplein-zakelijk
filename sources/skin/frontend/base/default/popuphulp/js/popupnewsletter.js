/* 
	author: istockphp.com
*/

jQuery(function($) {
	
$( window ).resize(function() {
	if($("#toPopupNewsletter").css("display") == "block"){
		if($( document ).width()>751){var height = "330px";}else{var height = "150px";}
		$("#toPopupNewsletter" ).css("height", height);
	}
});

$(document).ready(function(){
	
	if(readCookie('popup-newsletter')==null || readCookie('popup-newsletter')!='closed')
	{
	var time_in = $('input[name=time_in]').val();
		setTimeout(function(){helpFunc();}, time_in);
	}
	if(readCookie('popup-newsletter')=='submited')
	{
		helpFunc();
	}

});

function helpFunc() {
	
		loading(); // loading	
		loadPopup(); // function show popup 
		return false;
	
}
	
	$("button.home_newspaper_submit").click(function() {
		writeCookie('popup-newsletter','submited',30);
	});

	/* event for close the popup */
	$("div.close").hover(
					function() {
						$('span.ecs_tooltip').show();
					},
					function () {
    					$('span.ecs_tooltip').hide();
  					}
				);
	
	$("div.close").click(function() {
		disablePopup();  // function close pop up
		writeCookie('popup-newsletter','closed',30);
	});
	
	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePopup();  // function close pop up
			writeCookie('popup-newsletter','closed',30);
		}  	
	});
	
	$("div#backgroundPopup").click(function() {
		disablePopup();  // function close pop up
		writeCookie('popup-newsletter','closed',30);
	});
	

	 /************** start: functions. **************/
	function loading() {
		$("div.loader").show();  
	}
	function closeloading() {
		$("div.loader").fadeOut('normal');
	}
	
	var popupStatus = 0; // set value
	
	function loadPopup() {

		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			$( "#toPopupNewsletter" ).css("display", "block");
			
			if($( window ).width()>768){var height = "330px";}else{var height = "150px";}
				
			$( "#toPopupNewsletter" ).animate({ height: height}, 0900);

			$('input[name=curl]').val($(location).attr('href'));
			
			popupStatus = 1; // and set value to 1
		}
		
		if(readCookie('popup-newsletter')=='submited'){
			$("#title").fadeOut(0500);
			$("#thanks").fadeIn(0500);
			writeCookie('popup-newsletter','closed',30);
			
			function c(){
				var c=9;
				$('.c').text(c);
				setInterval(function(){
					c--;
					if(c>=0){
						$('.c').text(c);
					}
					if(c==0){
						disablePopup();
					}
				},1000);
			}
			// Start
			c();
			
			//Tracking Analytics
			ga('send', {
			  hitType: 'event',
			  eventCategory: 'Nieuwsbrief',
			  eventAction: 'Aanmelding',
			  eventLabel: 'Popup'
			});
		}
	}
		
	function disablePopup() {
		if(popupStatus == 1) { // if value is 1, close popup
			

			$("#toPopupNewsletter").fadeOut("normal");
		
			
			$("#backgroundPopup").fadeOut("normal");  
			popupStatus = 0;  // and set value to 0
		}
	}
	/************** end: functions. **************/
	
}); // jQuery End