/* 
	author: istockphp.com
*/

jQuery(function($) {

$(document).ready(function(){


		helpFunc();
  
});

function helpFunc() {

var currentdate = new Date(); 
var hour = currentdate.getHours();

var minutes = currentdate.getMinutes();

var day  = currentdate.getDay();

var hourminutes	= hour+''+minutes;

if (hourminutes<930 || hour>=17 || day == 6 || day == 0)//buiten kaNtoor tijden en geen za(6) of zo(0)
 {
	var time='out';
 }
else
{
	var time='in';
}

	if(readCookie('popup-back')=='true')//lang opc geweest en geen mobiel
	{
	 
	loading(); // loading
			setTimeout(function(){ // then show popup, deley in .5 second
				loadPopup(time); // function show popup 
			}, 500); // .5 second
	return false;
	
	}
}
	/* event for close the popup */
	$("div.close").hover(
					function() {
						$('span.ecs_tooltip').show();
					},
					function () {
    					$('span.ecs_tooltip').hide();
  					}
				);
	
	$("div.close").click(function() {
		disablePopup();  // function close pop up
	});
	
	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePopup();  // function close pop up
		}  	
	});
	
	$("div#backgroundPopup").click(function() {
		disablePopup();  // function close pop up
	});
	

	 /************** start: functions. **************/
	function loading() {
		$("div.loader").show();  
	}
	function closeloading() {
		$("div.loader").fadeOut('normal');  
	}
	
	var popupStatus = 0; // set value
	
	function loadPopup(time) { 
		if(popupStatus == 0) { // if value is 0, show popup
			closeloading(); // fadeout loading
			
			if(time=='out')
			{
			$("#toPopupmail").fadeIn(0500); // fadein popup div
			}
			else
			{
			
			$("#toPopupcall").fadeIn(0500); // fadein popup div
			}
			
			$("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
			$("#backgroundPopup").fadeIn(0001); 
			popupStatus = 1; // and set value to 1
		}
		writeCookie('popup-back','false',0.08);
	}
		
	function disablePopup() {
		if(popupStatus == 1) { // if value is 1, close popup
			

			$("#toPopupmail").fadeOut("normal");

			$("#toPopupcall").fadeOut("normal");
		
			
			$("#backgroundPopup").fadeOut("normal");  
			popupStatus = 0;  // and set value to 0
		}
	}
	/************** end: functions. **************/
	
}); // jQuery End