jQuery(function($) {



    $("div.item div.link").click(function() {
	
        loadPopup($(this).attr('id'));  // function close pop up
    });


    /* event for close the popup */
    $("div.close").hover(
                    function() {
                        $('span.ecs_tooltip').show();
                    },
                    function () {
                        $('span.ecs_tooltip').hide();
                      }
                );
    
    $("div.close").click(function() {
        disablePopup();  // function close pop up
    });
    
    $(this).keyup(function(event) {
        if (event.which == 27) { // 27 is 'Ecs' in the keyboard
            disablePopup();  // function close pop up
        }      
    });
    
    $("div#backgroundPopup").click(function() {
        disablePopup();  // function close pop up
    });
    
    
    

     /************** start: functions. **************/
    function loading() {
        $("div.loader").show();  
    }
    function closeloading() {
        $("div.loader").fadeOut('normal');  
    }
    
    function loadPopup(src) { 
        closeloading(); // fadeout loading
            $(".popup-lightbox").fadeIn(0500);
			$(".popup-lightbox").css('display', 'block');
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001); 
            $("#video").attr("src", "https://www.youtube.com/embed/"+src);
		
        writeCookie('popup-back','false',0.08);
    }
        
    function disablePopup() {
            $(".popup-lightbox").fadeOut("normal");
            $("#backgroundPopup").fadeOut("normal");  

            $("#video").attr("src","");

            
    }
    /************** end: functions. **************/
    
}); // jQuery End

